function [Lrgb,Datos]  = algoritmoJDM(L)

Lrgb = zeros(size(L,1),size(L,2),3);
Lrgb(:,:,1) = (L*255);
Lrgb(:,:,2) = (L*255);
Lrgb(:,:,3) = (L*255);
Lrgb = uint8(Lrgb);

DIM = DimImagen();
Pr = int16(DIM./2);

E = [0,0];
estadoLogico = L(Pr(1),Pr(2));

if(~estadoLogico)
    [Lrgb,PrN] = calcularPrAfuera(L,Lrgb,Pr,DIM);
    E = double(PrN-Pr);
    E = E/norm(E);
    Pr = PrN;
end

[A,B,C,D,Lrgb] = calcularVectores(L,Lrgb,Pr,DIM);

E = [E(2), E(1)];

Lrgb = PintarPunto(Lrgb,Pr,[255,0,0]);

TAB = angulo(A,B);
TBC = angulo(B,C);
TCD = angulo(C,D);
TDA = angulo(D,A);
nA = norm(A);
nB = norm(B);
nC = norm(C);
nD = norm(D);
Datos = [A B C D nA nB nC nD TAB TBC TCD TDA double(estadoLogico) E];

end



