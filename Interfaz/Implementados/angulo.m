function T = angulo(u,v)
nu = norm(u);
nv = norm(v);

if(nu ~= 0 && nv ~= 0)
T = real(acos(dot(u,v)/(nu*nv)));
else
    T = 0;
end

if(isnan(T))
    T = 0;
end
end