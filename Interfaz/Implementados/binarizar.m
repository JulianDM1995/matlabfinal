function BW  = binarizar(RGB)

I = rgb2hsv(RGB);

channel1Min = 0.942;
channel1Max = 0.057;

channel2Min = 0.101;
channel2Max = 1.000;

channel3Min = 0.321;
channel3Max = 1.000;

sliderBW = ( (I(:,:,1) >= channel1Min) | (I(:,:,1) <= channel1Max) ) & ...
    (I(:,:,2) >= channel2Min ) & (I(:,:,2) <= channel2Max) & ...
    (I(:,:,3) >= channel3Min ) & (I(:,:,3) <= channel3Max);
BW = sliderBW;

end
