function varargout = Entrenador(varargin)

gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @Entrenador_OpeningFcn, ...
    'gui_OutputFcn',  @Entrenador_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end

function botonProbar_Callback(hObject, eventdata, handles)

global DatosTotales
n = str2num(get(handles.edit1,'String'));
A = [DatosTotales(n,1),DatosTotales(n,2)];
B = [DatosTotales(n,3),DatosTotales(n,4)];
C = [DatosTotales(n,5),DatosTotales(n,6)];
D = [DatosTotales(n,7),DatosTotales(n,8)];
des = DatosTotales(n,size(DatosTotales,2));
axes(handles.vistaPrueba);
ESTADO = DatosTotales(n,17);
pintarAngulosYTitulo(A,B,C,D,ESTADO,des);

function botonEliminarElemento_Callback(hObject, eventdata, handles)
dato = str2double(get(handles.edit1,'String'));
eliminarDato(handles,dato);

function edit1_Callback(hObject, eventdata, handles)
function edit1_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit3_Callback(hObject, eventdata, handles)
function edit3_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit4_Callback(hObject, eventdata, handles)
function edit4_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit5_Callback(hObject, eventdata, handles)
function edit5_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit6_Callback(hObject, eventdata, handles)
function edit6_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%-------------------------------- INICIAL ---------------------------------

function Entrenador_OpeningFcn(hObject, eventdata, handles, varargin)

handles.output = hObject;
guidata(hObject, handles);

jFrame=get(handle(handles.figure1), 'javaframe');
jicon=javax.swing.ImageIcon('./otros/icon.png');
jFrame.setFigureIcon(jicon);
handles.output = hObject;
guidata(hObject, handles);

global imgO dimO

addpath('./Otros');

[file,path] = uigetfile( ...
    {'*.png;*.jpeg;*.jpg;*.bmp',...
    'Archivos de imagen (*.png,*.jpeg,*.jpg,*.bmp)';
    }, ...
    'Selecciona una imagen');
link = strcat(path,file);

imgO = imread(link);
dimO = [size(imgO,1),size(imgO,2)];

addpath('./Implementados');
addpath('./funcionesDeImagen');
addpath('./ImagenesEntrenamiento');
clc;

iniciarDatos(handles);
iniciarConstantes();
mostrarimgF(handles);

function varargout = Entrenador_OutputFcn(hObject, eventdata, handles)
varargout{1} = handles.output;

function figure1_WindowKeyReleaseFcn(hObject, eventdata, handles)
global DatosTotales
letra = get(gcf,'currentKey');

switch(letra)
    case 'a'
        MoverP('Izq');
    case 's'
        MoverP('Abj');
    case 'd'
        MoverP('Der');
    case 'w'
        MoverP('Arr');
    case 'r'
        Zoom('Mas');
    case 'f'
        Zoom('Min');
    case 'q'
        Rotar('Izq');
    case 'e'
        Rotar('Der');
    case 'l'
        indexarDato("Der");
        guardarDato(handles);
    case 'j'
        indexarDato("Izq");
        guardarDato(handles);
    case 'i'
        indexarDato("Arr");
        guardarDato(handles);
    case 'k'
        indexarDato("Abj");
        guardarDato(handles);
    case 'x'
        eliminarDato(handles,size(DatosTotales,1));
    case 'n'
        reiniciarDatos();
end

mostrarimgF(handles);

%----------------------------COSAS INCIALES--------------------------------

function iniciarConstantes()
global dimC P r kZ kR vP vZ vR

dimC=DimImagen();
r = ceil(norm(DimImagen()))/2;

[P kZ kR] = constantesIniciales();

vP = 5;
vZ = 0.1;
vR = 5;

Zoom('Zero');

function reiniciarDatos()
global dimC P kZ kR
[P kZ kR] = constantesIniciales();
Zoom('Zero');

function iniciarDatos(handles)
global DatosTotales;
if(exist('DatosRedNeuronal.mat', 'file') == 2)
    load('DatosRedNeuronal.mat','DatosTotales');
    mostrarDatosTotalesEnTabla(handles);
    s = strcat('Datos totales: [',num2str(size(DatosTotales,1)),',',num2str(size(DatosTotales,2)),']');
    set(handles.panelTodosLosDatos,'Title',s);
else
    DatosTotales = zeros(1,18);
end

axes(handles.vistaVectores);
axis off
axes(handles.vistaPrueba);
axis off

axes(handles.vistaPrueba);
pintarAngulosYTitulo([0,0],[0,0],[0,0],[0,0],false,'---');

%----------------------------CAMBIOS--------------------------------------

function MoverP(comando)
global  P vP dimZ kR

dP = [0,0];
switch comando
    case 'Izq'
        dP(2) = -vP;
    case 'Arr'
        dP(1) = -vP;
    case 'Der'
        dP(2) = vP;
    case 'Abj'
        dP(1) = vP;
end
M = double([cosd(kR) -sind(kR); sind(kR) cosd(kR)]);
P = P+dP*M;

P = max([1,1],P);
P = min(dimZ,P);

Aplicar_PreCrop();

function Rotar(comando)
global kR vR
dkR = 0;
switch comando
    case 'Izq'
        dkR = vR;
    case 'Der'
        dkR = -vR;
    case 'Zero'
        dkR = 0;
end

kR = kR + dkR;
Aplicar_Rot();

function Zoom(comando)
global imgO imgZ dimZ kZ dimC vZ

dK = 0;
switch comando
    case 'Mas'
        dK = vZ;
    case 'Min'
        dK = -vZ;
    case 'Zero'
        dK = 0;
end

imgZ_ = imresize(imgO,kZ+dK);
size(imgZ_);
if true([size(imgZ_,1),size(imgZ_,2)]>dimC)
    imgZ = imgZ_;
    kZ = kZ+dK;
end

dimZ = [size(imgZ,1),size(imgZ,2)];
Aplicar_PreCrop();

%-----------------------------Aplicaciones---------------------------------

function Aplicar_PreCrop()
global imgZ imgPC P r dimZ kZ

imgPC = uint8(zeros(2*r,2*r,3));

Ptemp = ceil(P);

ini = Ptemp-r;
fin = Ptemp+r;

X = ini(1):fin(1)-1;
Y = ini(2):fin(2)-1;

for x = 1:2*r
    for y = 1:2*r
        if(X(x)>0 && Y(y) > 0 && X(x)<= dimZ(1) && Y(y) <= dimZ(2))
            imgPC(x,y,:)=imgZ(X(x),Y(y),:);
        end
    end
end

Aplicar_Rot();

function Aplicar_Rot()
global imgPC imgR kR dimR
imgR = imrotate(imgPC,kR,'bilinear','crop');
dimR = [size(imgR,1),size(imgR,2)];
Aplicar_Crop();

function Aplicar_Crop()

global imgC imgR dimC

Pr = [size(imgR,1),size(imgR,2)];

ini = (Pr - dimC)/2;
fin = (Pr + dimC)/2;

imgC = imgR(ini(1):fin(1),ini(2):fin(2),:);
BinarizarYCalcularDatos();

function BinarizarYCalcularDatos()
global imgC imgB imgBC DatosActuales

imgB = binarizar(imgC);
[imgBC,DatosActuales] = algoritmoJDM(imgB);

%--------------------------Mostrar imagenes--------------------------------

function mostrarimgF(handles)
global imgC imgBC DatosActuales

A = [DatosActuales(1),DatosActuales(2)];
B = [DatosActuales(3),DatosActuales(4)];
C = [DatosActuales(5),DatosActuales(6)];
D = [DatosActuales(7),DatosActuales(8)];
ESTADO = DatosActuales(17);
mostrarConstantesEnTexto(handles)

axes(handles.imgFAxis);
imshow(imgC+imgBC*0.4);

axes(handles.imgBAxis);
imshow(imgBC);

axes(handles.vistaVectores);
pintarAngulos(A,B,C,D,ESTADO);

mostrarDatosActualEnTabla(handles);

%--------------------------Mostrar Tablas----------------------------------

function mostrarDatosActualEnTabla(handles)
global DatosActuales
Datos = DatosActuales;
Datos(13:16) = Datos(13:16)*180/pi;
set(handles.datosActuales,'data',Datos);

function mostrarDatosTotalesEnTabla(handles)
global DatosTotales
Datos = DatosTotales;
Datos(:,13:16) = Datos(:,13:16)*180/pi;

set(handles.datosTotales,'data',Datos);

function mostrarConstantesEnTexto(handles)
global P kZ kR

s = strcat('P = [',num2str(uint16(P(1))),',',num2str(uint16(P(2))),']   F = ',num2str(kZ),'x   Ang = ',num2str(-kR),'�');
set(handles.text5,'String',s);

%-----------------------Tratamiento de datos-------------------------------

function indexarDato(comando)
global DatosTotales DatosActuales
decision = 0;
switch comando
    case 'Izq'
        decision = 1;
    case 'Arr'
        decision = 2;
    case 'Der'
        decision = 3;
    case 'Abj'
        decision = 4;
end

Temp = DatosActuales;

for i = 1:2
    
    if(i==2)
        Temp(1)=-DatosActuales(5);
        Temp(2)=DatosActuales(6);
        Temp(3)=-DatosActuales(3);
        Temp(5)=-DatosActuales(1);
        Temp(6)=DatosActuales(2);
        Temp(7)=-DatosActuales(7);
        Temp(18)=-DatosActuales(18);
        
        switch comando
            case 'Izq'
                comando = 'Der';
                decision = 3;
            case 'Der'
                comando = 'Izq';
                decision = 1;
        end
        
    end
    Col = [Temp decision];
    if(any(any(DatosTotales)))
        DatosTotales = [DatosTotales;Col];
    else
        DatosTotales = Col;
    end
       
end

function guardarDato(handles)
global DatosTotales
mostrarDatosTotalesEnTabla(handles);

s = strcat('Datos totales: [',num2str(size(DatosTotales,1)),',',num2str(size(DatosTotales,2)),']');
set(handles.panelTodosLosDatos,'Title',s);

Datos = DatosTotales';
inputs = Datos(1:end-1,:);
targets = Datos(end,:);
save('inputs.mat','inputs');
save('targets.mat','targets');
save('DatosRedNeuronal.mat','DatosTotales');

function eliminarDato(handles,col)
global DatosTotales
DatosTotales(col,:) = [];
guardarDato(handles);
