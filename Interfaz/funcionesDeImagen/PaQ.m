function RGB = PaQ(RGB,P,Q,color)
M = double(max(abs(P(1)-Q(1)),abs(P(2)-Q(2))));

dx = double(Q(1)-P(1));
dy = double(Q(2)-P(2));

px = dx/M;
py = dy/M;

for i = 0:M-1
    x= int16(P(1)+i*px);
    y= int16(P(2)+i*py);
    
    RGB = PintarPunto(RGB,[x,y],color);
end

end

