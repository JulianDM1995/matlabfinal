function [] = pintarAngulosYTitulo(A,B,C,D,ESTADO,des)
TAA0 = angulo(A,[-1,0])*sign(A(2));
TAB = angulo(B,A) + TAA0;
TBC = angulo(B,C)+TAB;
TCD = angulo(C,D)+TBC;

thetaAB = linspace(TAA0,TAB, 36)-pi/2;
thetaBC = linspace(TAB, TBC, 36)-pi/2;
thetaCD = linspace(TBC,TCD, 36)-pi/2;
thetaDA = linspace(TAA0,-(2*pi-TCD), 36)-pi/2;

[xAB1, yAB1] = pol2cart(thetaAB, 1);
[xAB2, yAB2] = pol2cart(thetaAB, 0);

[xBC1, yBC1] = pol2cart(thetaBC, 1);
[xBC2, yBC2] = pol2cart(thetaBC, 0);

[xCD1, yCD1] = pol2cart(thetaCD, 1);
[xCD2, yCD2] = pol2cart(thetaCD, 0);

[xDA1, yDA1] = pol2cart(thetaDA, 1);
[xDA2, yDA2] = pol2cart(thetaDA, 0);

cero = zeros(1,2);
MA = [cero;A];
MB = [cero;B];
MC = [cero;C];
MD = [cero;D];

hold off
cla
hold on

patch([yAB1 fliplr(yAB2)],[xAB1 fliplr(xAB2)],[.7,.7,0], 'EdgeColor',[.7,.7,0])
patch([yBC1 fliplr(yBC2)],[xBC1 fliplr(xBC2)],[0,.7,0], 'EdgeColor',[0,.7,0])
patch([yCD1 fliplr(yCD2)],[xCD1 fliplr(xCD2)],[0,.7,.7], 'EdgeColor',[0,.7,.7])
patch([yDA1 fliplr(yDA2)],[xDA1 fliplr(xDA2)],[.7,0,.7], 'EdgeColor',[.7,0,.7])

plot(MA(:,1),MA(:,2),'Color',[1,1,0],...
    'Marker','o','MarkerFaceColor',[1,1,0],...
    'LineWidth',3);

plot(MB(:,1),MB(:,2),'Color',[0,1,0],...
    'Marker','o','MarkerFaceColor',[0,1,0],...
    'LineWidth',3);

plot(MC(:,1),MC(:,2),'Color',[0,1,1],...
    'Marker','o','MarkerFaceColor',[0,1,1],...
    'LineWidth',3);

plot(MD(:,1),MD(:,2),'Color',[1,0,1],...
    'Marker','o','MarkerFaceColor',[1,0,1],...
    'LineWidth',3);

switch des
    
    case 1
        Des = 'Rot Izq';
    case 2
        Des = 'Avanzar';
    case 3
        Des = 'Rot Der';
    case 4
        Des = 'Retroceder';
    case 5
        Des = 'Aterrizar';
    otherwise
        Des = '------';
end

text(0,-1.25,Des,'HorizontalAlignment','center')

if(ESTADO)
   COLOR = [144,185,27]; 
else
  COLOR = [212,26,27]; 
end
COLOR = COLOR/255;
plot(0,0,'o','MarkerSize',15,'MarkerEdgeColor','k','MarkerFaceColor',COLOR);

axis equal
axis([-1.1 1.1 -1.1 1.1])
end
