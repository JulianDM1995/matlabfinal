function [A,B,C,D] = calcularVectores_2(L,Pr,Dim)
%---------------------------------------------------------
PrD = double(Pr);

Am = max(1,PrD(1)-1);
AM = min(Dim(1),PrD(1)+1);
Bm = max(1,PrD(2)-1);
BM = min(Dim(2),PrD(2)+1);
Cm = max(1,PrD(1)-1);
CM = min(Dim(1),PrD(1)+1);
Dm = max(1,PrD(2)-1);
DM = min(Dim(2),PrD(2)+1);

AR = max(1,PrD(2)-1);
BR = max(1,PrD(1)-1);
CR = min(Dim(2),PrD(2)+1);
DR = min(Dim(1),PrD(1)+1);

AK = true;
BK = true;
CK = true;
DK = true;

tamBuff = 200;
AMedC = 1;
BMedC = 1;
CMedC = 1;
DMedC = 1;

AVef = zeros(tamBuff,1);
BVef =zeros(tamBuff,1);
CVef =zeros(tamBuff,1);
DVef =zeros(tamBuff,1);

Ap = [((Am+AM)/2),AR];
Bp = [BR,((Bm+BM)/2)];
Cp = [((Cm+CM)/2),CR];
Dp = [DR,((Dm+DM)/2)];

while(AK || BK || CK || DK)
    
    if(AK)
        AC = 1;
        for y = Am:AM
            %Lr = PintarPunto(Lr,[y,AR],[130,130,0]);
            if(L(y,AR))
                AVef(AC) = y;
                AC = AC+1;
            end
        end
        if(AC>1 && AR ~= 1)
            
            AMedC = AMedC + 1;
            AV_ = AVef(1:AC-1);
            Am = min(AV_)-1;
            Am = max(Am,1);
            AM = max(AV_)+1;
            AM = min(AM,Dim(1));
            AR = max(1,AR-1);
            AK = ~(AR==1);
        else
            Am = Am+1;
            AM = AM-1;
            if(~any(AVef))
                AR = AR+1;
            else
                AMedC = AMedC + 1;
            end
            AK = false;
        end
    end
    
    BC = 1;
    if(BK)
        for x = Bm:BM
            %Lr = PintarPunto(Lr,[BR,x],[0,130,0]);
            if(L(BR,x))
                BVef(BC) = x;
                BC = BC+1;
            end
        end
        
        if(BC>1 && BR ~= 1)
            
            BMedC = BMedC + 1;
            BV_ = BVef(1:BC-1);
            Bm = min(BV_)-1;
            Bm = max(1,Bm);
            BM = max(BV_)+1;
            BM = min(BM,Dim(2));
            BK = ~(BR==1);
            BR = max(1,BR-1);
        else
            Bm = Bm+1;
            BM = BM-1;
            if(~any(BVef))
                BR = BR+1;
            else
                BMedC = BMedC + 1;
            end
            BK = false;
        end
    end
    
    
    CC = 1;
    if(CK)
        for y = Cm:CM
            %Lr = PintarPunto(Lr,[y,CR],[0,130,130]);
            if(L(y,CR))
                CVef(CC) = y;
                CC = CC+1;
            end
        end
        
        if(CC>1 && CR ~= Dim(2))
            CMedC = CMedC + 1;
            CV_ = CVef(1:CC-1);
            Cm = min(CV_)-1;
            Cm = max(Cm,1);
            CM = max(CV_)+1;
            CM = min(CM,Dim(1));
            CK = ~(CR==Dim(2));
            CR = min(Dim(2),CR+1);
        else
            Cm = Cm+1;
            CM = CM-1;
            if(~any(CVef))
                CR = CR-1;
            else
                CMedC = CMedC + 1;
            end
            CK = false;
        end
    end
    
    
    DC = 1;
    if(DK)
        for x = Dm:DM
            %Lr = PintarPunto(Lr,[DR,x],[130,0,130]);
            if(L(DR,x))
                DVef(DC) = x;
                DC = DC+1;
            end
        end
        if(DC > 1 && DR ~= Dim(1))
            DMedC = DMedC + 1;
            DV_ = DVef(1:DC-1);
            Dm = min(DV_)-1;
            Dm = max(1,Dm);
            DM = max(DV_)+1;
            DM = min(DM,Dim(2));
            DR = max(1,DR+1);
        else
            Dm = Dm+1;
            DM = DM-1;
            if(~any(DVef))
                DR = DR-1;
            else
                DMedC = DMedC + 1;
            end
            DK = false;
        end
    end
    
end

Aq = [((Am+AM)/2),AR];
Bq = [BR,((Bm+BM)/2)];
Cq = [((Cm+CM)/2),CR];
Dq = [DR,((Dm+DM)/2)];

%Lr = PaQ(Lr,Ap,Aq,[255,255,0]);
%Lr = PaQ(Lr,Bp,Bq,[0,255,0]);
%Lr = PaQ(Lr,Cp,Cq,[0,255,255]);
%Lr = PaQ(Lr,Dp,Dq,[255,0,255]);

A = Aq-Ap ;
B = Bq-Bp ;
C = Cq-Cp ;
D = Dq-Dp ;

A = [-abs(A(2)),-A(1)];
B = [B(2),abs(B(1))];
C = [abs(C(2)),-C(1)];
D = [D(2),-abs(D(1))];

M = max([norm(A),norm(B),norm(C),norm(D)]);

A = A/norm(M);
B = B/norm(M);
C = C/norm(M);
D = D/norm(M);
end
