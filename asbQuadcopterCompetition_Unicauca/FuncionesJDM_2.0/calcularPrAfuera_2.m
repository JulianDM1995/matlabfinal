function E = calcularPrAfuera_2(L,Pr,Dim)
%---------------------------------------------------------

Rad = 1;                               %Radio inicial
encontrar = 0;                       %Variable l�gica, si ha encontrado lineas blancas o no
tamBuff = 200;                           %Cuantas posiciones almacenar� E
ET_ = zeros(tamBuff,2);           %Encuentra todos los E Posibles
m = 1;                               %Variable contadora de cuantos pixeles sobre linea blanca encuentra
Lim = max(Dim);                      %Limite hasta donde R puede crecer

%Mientras que no haya encontrado donde tocar, y el radio sea menor al
%l�mite, entonces el radio va a ir aumentando, lo cual a su vez va
%modificando las coordenadas de A,B,C,D

while(encontrar == 0 && Rad < Lim)
    
    A = ([max(1,Pr(1)-Rad),max(1,Pr(2)-Rad)]);
    B = ([min(Dim(1),Pr(1)+Rad),max(1,Pr(2)-Rad)]);
    C = ([max(1,Pr(1)-Rad),min(Dim(2),Pr(2)+Rad)]);
    D = ([min(Dim(1),Pr(1)+Rad),min(Dim(2),Pr(2)+Rad)]);
    
    for y = A(1):B(1)
        %Lr = PintarPunto(Lr,[y,A(2)],[130,130,0]);
        if(L(y,A(2)))
            ET_(m,:) = double([y,A(2)]);
            m = m+1;
            encontrar = 1;
        end
    end
    
    for x = A(2):C(2)                                          %Va desde el punto A al B, con altura constante.
        %Lr = PintarPunto(Lr,[A(1),x],[0,130,0]);
        if(L(A(1),x))                                          %Si encuentra linea blanca...
            ET_(m,:) = double([A(1),x]);                               %Almacena las coordenadas del pixel donde est� la linea blanca
            m = m+1;                                           %El contador va aumentando.
            encontrar = 1;                                     %Como encuentra linea blanca, puede terminar el ciclo.
        end
    end
    
    for y = C(1):D(1)
        %Lr = PintarPunto(Lr,[y,C(2)],[0,130,130]);
        if(L(y,C(2)))
            ET_(m,:) = double([y,C(2)]);
            m = m+1;
            encontrar = 1;
        end
    end
    
    for x = B(2):D(2)
        %Lr = PintarPunto(Lr,[B(1),x],[130,0,130]);
        if(L(B(1),x))
            ET_(m,:) = double([B(1),x]);
            m = m+1;
            encontrar = 1;
        end
    end
    
    %El radio va aumentando al final de cada ciclo. En caso de que no se haya encontrado nada
    %entonces se repite el ciclo.
    Rad = Rad+1;
    
end

if(m>1) %Si encontr� alg�n pixel sobre linea blanca:
    
    ET = ET_(1:m-1,:);         %Guarda solo las posiciones efectivas
    
    %    for i = 1:size(ET,1)
    %        Lr = PintarPunto(Lr,ET(i,:),[0,0,255]);
    %    end
    
    %Encuentra las distancias desde el punto Pr hasta cada uno
    %de los pixeles efectivos
    EM = sqrt(double((ET(:,1))-double(Pr(1))).^2+(double(ET(:,2))-double(Pr(2))).^2);
    
    %Encuentra el �ndice de cual tiene la menor distancia
    ind = find(EM == min(EM));
    %Almacena en E el pixel efectivo que est� a la menor distancia de Pr
    E = int16(ET(ind(1),:));
    %    Lr = PaQ(Lr,Pr,E,[150,150,150]);
else %Si no encontr� nada es por que toda la imagen est� en negro.
    E = int16(Pr);
end

end
