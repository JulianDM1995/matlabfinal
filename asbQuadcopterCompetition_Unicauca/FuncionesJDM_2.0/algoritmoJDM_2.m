function [Datos]  = algoritmoJDM_2(L)

DIM = DimImagen();
Pr = int16(DIM./2);

E = [0,0];
estadoLogico = L(Pr(1),Pr(2));

if(~estadoLogico)
    [PrN] = calcularPrAfuera_2(L,Pr,DIM);
    E = double(PrN-Pr);
    E = E/norm(E);
    Pr = PrN;
end

[A,B,C,D] = calcularVectores_2(L,Pr,DIM);

E = [E(2), E(1)];

TAB = angulo(A,B);
TBC = angulo(B,C);
TCD = angulo(C,D);
TDA = angulo(D,A);
nA = norm(A);
nB = norm(B);
nC = norm(C);
nD = norm(D);
Datos = [A B C D nA nB nC nD TAB TBC TCD TDA double(estadoLogico) E];

end

function T = angulo(u,v)
nu = norm(u);
nv = norm(v);

if(nu ~= 0 && nv ~= 0)
T = real(acos(dot(u,v)/(nu*nv)));
else
    T = 0;
end

if(isnan(T))
    T = 0;
end
end


