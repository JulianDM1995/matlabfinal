#ifndef __c6_flightControlSystem_h__
#define __c6_flightControlSystem_h__

/* Type Definitions */
#ifndef typedef_SFc6_flightControlSystemInstanceStruct
#define typedef_SFc6_flightControlSystemInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c6_sfEvent;
  boolean_T c6_doneDoubleBufferReInit;
  uint8_T c6_is_active_c6_flightControlSystem;
  real_T c6_I[57600];
  real_T c6_outputImage[57600];
  real_T c6_y[57600];
  real_T c6_dv0[57600];
  real_T c6_u[57600];
  void *c6_fEmlrtCtx;
  uint8_T (*c6_RGB)[57600];
  boolean_T (*c6_BW)[19200];
} SFc6_flightControlSystemInstanceStruct;

#endif                                 /*typedef_SFc6_flightControlSystemInstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c6_flightControlSystem_get_eml_resolved_functions_info
  (void);

/* Function Definitions */
extern void sf_c6_flightControlSystem_get_check_sum(mxArray *plhs[]);
extern void c6_flightControlSystem_method_dispatcher(SimStruct *S, int_T method,
  void *data);

#endif
