/*
 * stateEstimator_data.c
 *
 * Sponsored License - for use in support of a program or activity
 * sponsored by MathWorks.  Not for government, commercial or other
 * non-sponsored organizational use.
 *
 * Code generation for model "stateEstimator".
 *
 * Model version              : 1.45
 * Simulink Coder version : 8.14 (R2018a) 06-Feb-2018
 * C source code generated on : Wed Oct 17 00:28:34 2018
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: ARM Compatible->ARM 9
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "stateEstimator.h"
#include "stateEstimator_private.h"

/* Invariant block signals (default storage) */
const ConstB_stateEstimator_h_T stateEstimator_ConstB = {
  { 0.026241420641871196, 0.069776736071495662 },/* '<S57>/Conversion' */

  { -0.046, 0.0 },                     /* '<S14>/ReshapeX0' */

  { 0.026590304322228676, 0.069776736071495662 },/* '<S56>/Conversion' */

  { 0.00262414206418712, 0.0069776736071495669, 0.0069776736071495669,
    0.03760769293505653 },             /* '<S59>/Conversion' */
  0.0,                                 /* '<S58>/Conversion' */

  { 0.00575686F, 0.0F, 0.0F, 0.00575686F },/* '<S175>/Conversion' */

  { 0.125465602F, 0.0F, 0.0F, 0.125465602F },/* '<S128>/Conversion' */

  { 0.125465602F, 0.0F, 0.0F, 0.125465602F },/* '<S127>/Conversion' */

  { 0.00575686F, 0.0F, 0.0F, 0.00575686F },/* '<S174>/Conversion' */

  { 0.0F, 0.0F },                      /* '<S71>/ReshapeX0' */

  { 0.1F, 0.0F },                      /* '<S133>/ReshapeX0' */

  { 0.00269485894F, 0.0071657123F, 0.0071657123F, 0.0381076932F },/* '<S27>/Conversion' */

  { 0.627328038F, 0.0F, 0.0F, 0.627328038F },/* '<S130>/Conversion' */

  { 0.717328072F, 0.0F, 0.0F, 0.717328072F },/* '<S98>/Conversion' */

  { 0.00172705797F, 0.0F, 0.0F, 0.00172705797F },/* '<S177>/Conversion' */

  { 0.00173705805F, 0.0F, 0.0F, 0.00173705805F },/* '<S145>/Conversion' */
  0.0F,                                /* '<S129>/Conversion' */
  0.0F,                                /* '<S176>/Conversion' */
  0,                                   /* '<S71>/DataTypeConversionReset' */
  0                                    /* '<S133>/DataTypeConversionReset' */
};
