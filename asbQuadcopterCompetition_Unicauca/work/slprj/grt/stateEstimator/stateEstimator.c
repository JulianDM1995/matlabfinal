/*
 * Code generation for system model 'stateEstimator'
 *
 * Model                      : stateEstimator
 * Model version              : 1.45
 * Simulink Coder version : 8.14 (R2018a) 06-Feb-2018
 * C source code generated on : Wed Oct 17 00:28:34 2018
 *
 * Note that the functions contained in this file are part of a Simulink
 * model, and are not self-contained algorithms.
 */

#include "stateEstimator.h"
#include "stateEstimator_private.h"
#include "rt_powf_snf.h"

MdlrefDW_stateEstimator_T stateEstimator_MdlrefDW;

/* Block signals (default storage) */
B_stateEstimator_c_T stateEstimator_B;

/* Block states (default storage) */
DW_stateEstimator_f_T stateEstimator_DW;

/*
 * System initialize for enable system:
 *    '<S106>/MeasurementUpdate'
 *    '<S153>/MeasurementUpdate'
 */
void stateEstimator_MeasurementUpdate_Init(B_MeasurementUpdate_stateEstimator_T *
  localB)
{
  /* SystemInitialize for Outport: '<S131>/L*(y[k]-yhat[k|k-1])' */
  localB->Product3[0] = 0.0F;
  localB->Product3[1] = 0.0F;
}

/*
 * Disable for enable system:
 *    '<S106>/MeasurementUpdate'
 *    '<S153>/MeasurementUpdate'
 */
void stateEstimator_MeasurementUpdate_Disable
  (B_MeasurementUpdate_stateEstimator_T *localB,
   DW_MeasurementUpdate_stateEstimator_T *localDW)
{
  /* Disable for Outport: '<S131>/L*(y[k]-yhat[k|k-1])' */
  localB->Product3[0] = 0.0F;
  localB->Product3[1] = 0.0F;
  localDW->MeasurementUpdate_MODE = false;
}

/*
 * Output and update for enable system:
 *    '<S106>/MeasurementUpdate'
 *    '<S153>/MeasurementUpdate'
 */
void stateEstimator_MeasurementUpdate(boolean_T rtu_Enable, const real32_T
  rtu_Lk[4], const real32_T rtu_yk[2], const real32_T rtu_Ck[4], const real32_T
  rtu_xhatkk1[2], const real32_T rtu_Dk[4], const real32_T rtu_uk[2],
  B_MeasurementUpdate_stateEstimator_T *localB,
  DW_MeasurementUpdate_stateEstimator_T *localDW)
{
  real32_T rtu_yk_idx_0;
  real32_T rtu_yk_idx_1;

  /* Outputs for Enabled SubSystem: '<S106>/MeasurementUpdate' incorporates:
   *  EnablePort: '<S131>/Enable'
   */
  if (rtu_Enable) {
    if (!localDW->MeasurementUpdate_MODE) {
      localDW->MeasurementUpdate_MODE = true;
    }

    /* Sum: '<S131>/Sum' incorporates:
     *  Product: '<S131>/C[k]*xhat[k|k-1]'
     *  Product: '<S131>/D[k]*u[k]'
     *  Product: '<S131>/Product3'
     *  Sum: '<S131>/Add1'
     */
    rtu_yk_idx_0 = rtu_yk[0] - ((rtu_Ck[0] * rtu_xhatkk1[0] + rtu_Ck[2] *
      rtu_xhatkk1[1]) + (rtu_Dk[0] * rtu_uk[0] + rtu_Dk[2] * rtu_uk[1]));
    rtu_yk_idx_1 = rtu_yk[1] - ((rtu_Ck[1] * rtu_xhatkk1[0] + rtu_Ck[3] *
      rtu_xhatkk1[1]) + (rtu_Dk[1] * rtu_uk[0] + rtu_Dk[3] * rtu_uk[1]));

    /* Product: '<S131>/Product3' */
    localB->Product3[0] = 0.0F;
    localB->Product3[0] += rtu_Lk[0] * rtu_yk_idx_0;
    localB->Product3[0] += rtu_Lk[2] * rtu_yk_idx_1;
    localB->Product3[1] = 0.0F;
    localB->Product3[1] += rtu_Lk[1] * rtu_yk_idx_0;
    localB->Product3[1] += rtu_Lk[3] * rtu_yk_idx_1;
  } else {
    if (localDW->MeasurementUpdate_MODE) {
      stateEstimator_MeasurementUpdate_Disable(localB, localDW);
    }
  }

  /* End of Outputs for SubSystem: '<S106>/MeasurementUpdate' */
}

/*
 * System initialize for atomic system:
 *    '<S71>/UseCurrentEstimator'
 *    '<S133>/UseCurrentEstimator'
 */
void stateEstimator_UseCurrentEstimator_Init
  (B_UseCurrentEstimator_stateEstimator_T *localB)
{
  /* SystemInitialize for Enabled SubSystem: '<S111>/Enabled Subsystem' */
  /* SystemInitialize for Outport: '<S132>/deltax' */
  localB->Product2[0] = 0.0F;
  localB->Product2[1] = 0.0F;

  /* End of SystemInitialize for SubSystem: '<S111>/Enabled Subsystem' */
}

/*
 * Disable for atomic system:
 *    '<S71>/UseCurrentEstimator'
 *    '<S133>/UseCurrentEstimator'
 */
void stateEstimator_UseCurrentEstimator_Disable
  (B_UseCurrentEstimator_stateEstimator_T *localB,
   DW_UseCurrentEstimator_stateEstimator_T *localDW)
{
  /* Disable for Enabled SubSystem: '<S111>/Enabled Subsystem' */
  if (localDW->EnabledSubsystem_MODE) {
    /* Disable for Outport: '<S132>/deltax' */
    localB->Product2[0] = 0.0F;
    localB->Product2[1] = 0.0F;
    localDW->EnabledSubsystem_MODE = false;
  }

  /* End of Disable for SubSystem: '<S111>/Enabled Subsystem' */
}

/*
 * Output and update for atomic system:
 *    '<S71>/UseCurrentEstimator'
 *    '<S133>/UseCurrentEstimator'
 */
void stateEstimator_UseCurrentEstimator(boolean_T rtu_Enablek, const real32_T
  rtu_Mk[4], const real32_T rtu_uk[2], const real32_T rtu_yk[2], const real32_T
  rtu_Ck[4], const real32_T rtu_Dk[4], const real32_T rtu_xhatkk1[2],
  B_UseCurrentEstimator_stateEstimator_T *localB,
  DW_UseCurrentEstimator_stateEstimator_T *localDW)
{
  real32_T rtu_yk_idx_0;
  real32_T rtu_yk_idx_1;

  /* Outputs for Enabled SubSystem: '<S111>/Enabled Subsystem' incorporates:
   *  EnablePort: '<S132>/Enable'
   */
  if (rtu_Enablek) {
    if (!localDW->EnabledSubsystem_MODE) {
      localDW->EnabledSubsystem_MODE = true;
    }

    /* Sum: '<S132>/Add1' incorporates:
     *  Product: '<S132>/Product'
     *  Product: '<S132>/Product1'
     *  Product: '<S132>/Product2'
     */
    rtu_yk_idx_0 = (rtu_yk[0] - (rtu_Ck[0] * rtu_xhatkk1[0] + rtu_Ck[2] *
      rtu_xhatkk1[1])) - (rtu_Dk[0] * rtu_uk[0] + rtu_Dk[2] * rtu_uk[1]);
    rtu_yk_idx_1 = (rtu_yk[1] - (rtu_Ck[1] * rtu_xhatkk1[0] + rtu_Ck[3] *
      rtu_xhatkk1[1])) - (rtu_Dk[1] * rtu_uk[0] + rtu_Dk[3] * rtu_uk[1]);

    /* Product: '<S132>/Product2' */
    localB->Product2[0] = 0.0F;
    localB->Product2[0] += rtu_Mk[0] * rtu_yk_idx_0;
    localB->Product2[0] += rtu_Mk[2] * rtu_yk_idx_1;
    localB->Product2[1] = 0.0F;
    localB->Product2[1] += rtu_Mk[1] * rtu_yk_idx_0;
    localB->Product2[1] += rtu_Mk[3] * rtu_yk_idx_1;
  } else {
    if (localDW->EnabledSubsystem_MODE) {
      /* Disable for Outport: '<S132>/deltax' */
      localB->Product2[0] = 0.0F;
      localB->Product2[1] = 0.0F;
      localDW->EnabledSubsystem_MODE = false;
    }
  }

  /* End of Outputs for SubSystem: '<S111>/Enabled Subsystem' */

  /* Sum: '<S111>/Add' */
  localB->Add[0] = localB->Product2[0] + rtu_xhatkk1[0];
  localB->Add[1] = localB->Product2[1] + rtu_xhatkk1[1];
}

/* System initialize for referenced model: 'stateEstimator' */
void stateEstimator_Init(void)
{
  int32_T i;

  /* InitializeConditions for Memory: '<S2>/Memory' */
  stateEstimator_DW.Memory_PreviousInput[0] = 0.0F;
  stateEstimator_DW.Memory_PreviousInput[1] = 0.0F;
  stateEstimator_DW.Memory_PreviousInput[2] = 0.0F;

  /* InitializeConditions for DiscreteFilter: '<S5>/IIR_IMUgyro_r' */
  for (i = 0; i < 5; i++) {
    stateEstimator_DW.IIR_IMUgyro_r_states[i] = 0.0F;
  }

  /* End of InitializeConditions for DiscreteFilter: '<S5>/IIR_IMUgyro_r' */

  /* InitializeConditions for DiscreteFir: '<S5>/FIR_IMUaccel' */
  stateEstimator_DW.FIR_IMUaccel_circBuf = 0;
  for (i = 0; i < 15; i++) {
    stateEstimator_DW.FIR_IMUaccel_states[i] = 0.0F;
  }

  /* End of InitializeConditions for DiscreteFir: '<S5>/FIR_IMUaccel' */

  /* InitializeConditions for Delay: '<S3>/Delay2' */
  stateEstimator_DW.Delay2_DSTATE = 0.0;

  /* InitializeConditions for DiscreteFilter: '<S15>/pressureFilter_IIR' */
  stateEstimator_DW.pressureFilter_IIR_states[0] = 0.0F;

  /* InitializeConditions for DiscreteFilter: '<S15>/soonarFilter_IIR' */
  stateEstimator_DW.soonarFilter_IIR_states[0] = 0.0;

  /* InitializeConditions for DiscreteFilter: '<S15>/pressureFilter_IIR' */
  stateEstimator_DW.pressureFilter_IIR_states[1] = 0.0F;

  /* InitializeConditions for DiscreteFilter: '<S15>/soonarFilter_IIR' */
  stateEstimator_DW.soonarFilter_IIR_states[1] = 0.0;

  /* InitializeConditions for DiscreteFilter: '<S15>/pressureFilter_IIR' */
  stateEstimator_DW.pressureFilter_IIR_states[2] = 0.0F;

  /* InitializeConditions for DiscreteFilter: '<S15>/soonarFilter_IIR' */
  stateEstimator_DW.soonarFilter_IIR_states[2] = 0.0;

  /* InitializeConditions for Delay: '<S14>/MemoryX' */
  stateEstimator_DW.icLoad = 1U;

  /* InitializeConditions for Delay: '<S1>/Delay1' */
  stateEstimator_DW.Delay1_DSTATE[0] = 0.0F;
  stateEstimator_DW.Delay1_DSTATE[1] = 0.0F;

  /* InitializeConditions for DiscreteFilter: '<S70>/IIRgyroz' */
  for (i = 0; i < 10; i++) {
    stateEstimator_DW.IIRgyroz_states[i] = 0.0F;
  }

  /* End of InitializeConditions for DiscreteFilter: '<S70>/IIRgyroz' */

  /* InitializeConditions for Delay: '<S71>/MemoryX' */
  stateEstimator_DW.icLoad_i = 1U;

  /* InitializeConditions for Delay: '<S133>/MemoryX' */
  stateEstimator_DW.icLoad_g = 1U;

  /* InitializeConditions for DiscreteIntegrator: '<S68>/SimplyIntegrateVelocity' */
  stateEstimator_DW.SimplyIntegrateVelocity_PrevResetState = 2;

  /* InitializeConditions for UnitDelay: '<S77>/UD' */
  stateEstimator_DW.UD_DSTATE[0] = 0.0F;

  /* InitializeConditions for Delay: '<S67>/Delay' */
  stateEstimator_DW.Delay_DSTATE[0] = 0.0F;

  /* InitializeConditions for Delay: '<S68>/Delay' */
  stateEstimator_DW.Delay_DSTATE_l[0] = 0.0F;

  /* InitializeConditions for DiscreteIntegrator: '<S68>/SimplyIntegrateVelocity' */
  stateEstimator_DW.SimplyIntegrateVelocity_DSTATE[0] = 0.0F;

  /* SystemInitialize for Merge: '<S2>/Merge' */
  stateEstimator_B.Merge[0] = 0.0F;

  /* SystemInitialize for Atomic SubSystem: '<S14>/UseCurrentEstimator' */
  /* SystemInitialize for Enabled SubSystem: '<S40>/Enabled Subsystem' */
  /* SystemInitialize for Outport: '<S61>/deltax' */
  stateEstimator_B.Product2[0] = 0.0;

  /* End of SystemInitialize for SubSystem: '<S40>/Enabled Subsystem' */
  /* End of SystemInitialize for SubSystem: '<S14>/UseCurrentEstimator' */

  /* InitializeConditions for UnitDelay: '<S77>/UD' */
  stateEstimator_DW.UD_DSTATE[1] = 0.0F;

  /* InitializeConditions for Delay: '<S67>/Delay' */
  stateEstimator_DW.Delay_DSTATE[1] = 0.0F;

  /* InitializeConditions for Delay: '<S68>/Delay' */
  stateEstimator_DW.Delay_DSTATE_l[1] = 0.0F;

  /* InitializeConditions for DiscreteIntegrator: '<S68>/SimplyIntegrateVelocity' */
  stateEstimator_DW.SimplyIntegrateVelocity_DSTATE[1] = 0.0F;

  /* SystemInitialize for Merge: '<S2>/Merge' */
  stateEstimator_B.Merge[1] = 0.0F;

  /* SystemInitialize for Atomic SubSystem: '<S14>/UseCurrentEstimator' */
  /* SystemInitialize for Enabled SubSystem: '<S40>/Enabled Subsystem' */
  /* SystemInitialize for Outport: '<S61>/deltax' */
  stateEstimator_B.Product2[1] = 0.0;

  /* End of SystemInitialize for SubSystem: '<S40>/Enabled Subsystem' */
  /* End of SystemInitialize for SubSystem: '<S14>/UseCurrentEstimator' */

  /* SystemInitialize for Atomic SubSystem: '<S71>/UseCurrentEstimator' */
  stateEstimator_UseCurrentEstimator_Init
    (&stateEstimator_B.UseCurrentEstimator_l);

  /* End of SystemInitialize for SubSystem: '<S71>/UseCurrentEstimator' */

  /* SystemInitialize for Atomic SubSystem: '<S133>/UseCurrentEstimator' */
  stateEstimator_UseCurrentEstimator_Init
    (&stateEstimator_B.UseCurrentEstimator_j);

  /* End of SystemInitialize for SubSystem: '<S133>/UseCurrentEstimator' */

  /* SystemInitialize for Enabled SubSystem: '<S153>/MeasurementUpdate' */
  stateEstimator_MeasurementUpdate_Init(&stateEstimator_B.MeasurementUpdate_b);

  /* End of SystemInitialize for SubSystem: '<S153>/MeasurementUpdate' */

  /* SystemInitialize for Enabled SubSystem: '<S106>/MeasurementUpdate' */
  stateEstimator_MeasurementUpdate_Init(&stateEstimator_B.MeasurementUpdate_e);

  /* End of SystemInitialize for SubSystem: '<S106>/MeasurementUpdate' */

  /* SystemInitialize for Enabled SubSystem: '<S35>/MeasurementUpdate' */
  /* SystemInitialize for Outport: '<S60>/L*(y[k]-yhat[k|k-1])' */
  stateEstimator_B.Product3[0] = 0.0;
  stateEstimator_B.Product3[1] = 0.0;

  /* End of SystemInitialize for SubSystem: '<S35>/MeasurementUpdate' */
}

/* System reset for referenced model: 'stateEstimator' */
void stateEstimator_Reset(void)
{
  int32_T i;

  /* InitializeConditions for Memory: '<S2>/Memory' */
  stateEstimator_DW.Memory_PreviousInput[0] = 0.0F;
  stateEstimator_DW.Memory_PreviousInput[1] = 0.0F;
  stateEstimator_DW.Memory_PreviousInput[2] = 0.0F;

  /* InitializeConditions for DiscreteFilter: '<S5>/IIR_IMUgyro_r' */
  for (i = 0; i < 5; i++) {
    stateEstimator_DW.IIR_IMUgyro_r_states[i] = 0.0F;
  }

  /* End of InitializeConditions for DiscreteFilter: '<S5>/IIR_IMUgyro_r' */

  /* InitializeConditions for DiscreteFir: '<S5>/FIR_IMUaccel' */
  stateEstimator_DW.FIR_IMUaccel_circBuf = 0;
  for (i = 0; i < 15; i++) {
    stateEstimator_DW.FIR_IMUaccel_states[i] = 0.0F;
  }

  /* End of InitializeConditions for DiscreteFir: '<S5>/FIR_IMUaccel' */

  /* InitializeConditions for Delay: '<S3>/Delay2' */
  stateEstimator_DW.Delay2_DSTATE = 0.0;

  /* InitializeConditions for DiscreteFilter: '<S15>/pressureFilter_IIR' */
  stateEstimator_DW.pressureFilter_IIR_states[0] = 0.0F;

  /* InitializeConditions for DiscreteFilter: '<S15>/soonarFilter_IIR' */
  stateEstimator_DW.soonarFilter_IIR_states[0] = 0.0;

  /* InitializeConditions for DiscreteFilter: '<S15>/pressureFilter_IIR' */
  stateEstimator_DW.pressureFilter_IIR_states[1] = 0.0F;

  /* InitializeConditions for DiscreteFilter: '<S15>/soonarFilter_IIR' */
  stateEstimator_DW.soonarFilter_IIR_states[1] = 0.0;

  /* InitializeConditions for DiscreteFilter: '<S15>/pressureFilter_IIR' */
  stateEstimator_DW.pressureFilter_IIR_states[2] = 0.0F;

  /* InitializeConditions for DiscreteFilter: '<S15>/soonarFilter_IIR' */
  stateEstimator_DW.soonarFilter_IIR_states[2] = 0.0;

  /* InitializeConditions for Delay: '<S14>/MemoryX' */
  stateEstimator_DW.icLoad = 1U;

  /* InitializeConditions for Delay: '<S1>/Delay1' */
  stateEstimator_DW.Delay1_DSTATE[0] = 0.0F;
  stateEstimator_DW.Delay1_DSTATE[1] = 0.0F;

  /* InitializeConditions for DiscreteFilter: '<S70>/IIRgyroz' */
  for (i = 0; i < 10; i++) {
    stateEstimator_DW.IIRgyroz_states[i] = 0.0F;
  }

  /* End of InitializeConditions for DiscreteFilter: '<S70>/IIRgyroz' */

  /* InitializeConditions for Delay: '<S71>/MemoryX' */
  stateEstimator_DW.icLoad_i = 1U;

  /* InitializeConditions for Delay: '<S133>/MemoryX' */
  stateEstimator_DW.icLoad_g = 1U;

  /* InitializeConditions for UnitDelay: '<S77>/UD' */
  stateEstimator_DW.UD_DSTATE[0] = 0.0F;

  /* InitializeConditions for Delay: '<S67>/Delay' */
  stateEstimator_DW.Delay_DSTATE[0] = 0.0F;

  /* InitializeConditions for Delay: '<S68>/Delay' */
  stateEstimator_DW.Delay_DSTATE_l[0] = 0.0F;

  /* InitializeConditions for DiscreteIntegrator: '<S68>/SimplyIntegrateVelocity' */
  stateEstimator_DW.SimplyIntegrateVelocity_DSTATE[0] = 0.0F;

  /* InitializeConditions for UnitDelay: '<S77>/UD' */
  stateEstimator_DW.UD_DSTATE[1] = 0.0F;

  /* InitializeConditions for Delay: '<S67>/Delay' */
  stateEstimator_DW.Delay_DSTATE[1] = 0.0F;

  /* InitializeConditions for Delay: '<S68>/Delay' */
  stateEstimator_DW.Delay_DSTATE_l[1] = 0.0F;

  /* InitializeConditions for DiscreteIntegrator: '<S68>/SimplyIntegrateVelocity' */
  stateEstimator_DW.SimplyIntegrateVelocity_DSTATE[1] = 0.0F;
  stateEstimator_DW.SimplyIntegrateVelocity_PrevResetState = 2;
}

/* Disable for referenced model: 'stateEstimator' */
void stateEstimator_Disable(void)
{
  /* Disable for Atomic SubSystem: '<S14>/UseCurrentEstimator' */
  /* Disable for Enabled SubSystem: '<S40>/Enabled Subsystem' */
  if (stateEstimator_DW.EnabledSubsystem_MODE) {
    /* Disable for Outport: '<S61>/deltax' */
    stateEstimator_B.Product2[0] = 0.0;
    stateEstimator_B.Product2[1] = 0.0;
    stateEstimator_DW.EnabledSubsystem_MODE = false;
  }

  /* End of Disable for SubSystem: '<S40>/Enabled Subsystem' */
  /* End of Disable for SubSystem: '<S14>/UseCurrentEstimator' */

  /* Disable for Atomic SubSystem: '<S71>/UseCurrentEstimator' */
  stateEstimator_UseCurrentEstimator_Disable
    (&stateEstimator_B.UseCurrentEstimator_l,
     &stateEstimator_DW.UseCurrentEstimator_l);

  /* End of Disable for SubSystem: '<S71>/UseCurrentEstimator' */

  /* Disable for Atomic SubSystem: '<S133>/UseCurrentEstimator' */
  stateEstimator_UseCurrentEstimator_Disable
    (&stateEstimator_B.UseCurrentEstimator_j,
     &stateEstimator_DW.UseCurrentEstimator_j);

  /* End of Disable for SubSystem: '<S133>/UseCurrentEstimator' */

  /* Disable for Enabled SubSystem: '<S153>/MeasurementUpdate' */
  if (stateEstimator_DW.MeasurementUpdate_b.MeasurementUpdate_MODE) {
    stateEstimator_MeasurementUpdate_Disable
      (&stateEstimator_B.MeasurementUpdate_b,
       &stateEstimator_DW.MeasurementUpdate_b);
  }

  /* End of Disable for SubSystem: '<S153>/MeasurementUpdate' */

  /* Disable for Enabled SubSystem: '<S106>/MeasurementUpdate' */
  if (stateEstimator_DW.MeasurementUpdate_e.MeasurementUpdate_MODE) {
    stateEstimator_MeasurementUpdate_Disable
      (&stateEstimator_B.MeasurementUpdate_e,
       &stateEstimator_DW.MeasurementUpdate_e);
  }

  /* End of Disable for SubSystem: '<S106>/MeasurementUpdate' */

  /* Disable for Enabled SubSystem: '<S35>/MeasurementUpdate' */
  if (stateEstimator_DW.MeasurementUpdate_MODE) {
    /* Disable for Outport: '<S60>/L*(y[k]-yhat[k|k-1])' */
    stateEstimator_B.Product3[0] = 0.0;
    stateEstimator_B.Product3[1] = 0.0;
    stateEstimator_DW.MeasurementUpdate_MODE = false;
  }

  /* End of Disable for SubSystem: '<S35>/MeasurementUpdate' */
}

/* Output and update for referenced model: 'stateEstimator' */
void stateEstimator(const boolean_T *rtu_controlModePosVSOrient_flagin, const
                    sensordata_t *rtu_sensordata_datin, const real32_T
                    *rtu_usePosVIS_flagin, const real32_T rtu_opticalFlow_datin
                    [3], const real32_T rtu_posVIS_datin[4], const real32_T
                    rtu_sensorCalibration_datin[8], statesEstim_t
                    *rty_states_estimout)
{
  /* local block i/o variables */
  boolean_T rtb_DataTypeConversionEnable_i;
  boolean_T rtb_DataTypeConversionEnable_h;
  int32_T j;
  int32_T denIdx;
  int32_T cff;
  boolean_T rtb_LogicalOperator_cg;
  real32_T rtb_DataTypeConversion2;
  boolean_T rtb_Compare_i;
  int32_T i;
  real32_T rtb_DataTypeConversion1_o_idx_1;
  real32_T rtb_Sum_idx_2;
  real32_T y;
  real32_T rtb_DataTypeConversion1_o_idx_0_tmp;
  real32_T rtb_VectorConcatenate_tmp;
  real32_T rtb_VectorConcatenate_tmp_0;

  /* SignalConversion: '<S13>/ConcatBufferAtVector ConcatenateIn1' incorporates:
   *  Constant: '<S12>/Constant'
   */
  stateEstimator_B.VectorConcatenate[0] = 0.0F;

  /* SignalConversion: '<S13>/ConcatBufferAtVector ConcatenateIn2' incorporates:
   *  Constant: '<S12>/Constant'
   */
  stateEstimator_B.VectorConcatenate[1] = 0.0F;

  /* SignalConversion: '<S12>/TmpSignal ConversionAtTrigonometric FunctionInport1' incorporates:
   *  Memory: '<S2>/Memory'
   */
  stateEstimator_B.sincos_o1[0] = stateEstimator_DW.Memory_PreviousInput[2];
  stateEstimator_B.sincos_o1[1] = stateEstimator_DW.Memory_PreviousInput[1];
  stateEstimator_B.sincos_o1[2] = stateEstimator_DW.Memory_PreviousInput[0];

  /* Trigonometry: '<S12>/Trigonometric Function' incorporates:
   *  Memory: '<S2>/Memory'
   *  SignalConversion: '<S12>/TmpSignal ConversionAtTrigonometric FunctionInport1'
   */
  stateEstimator_DW.Memory_PreviousInput[0] = (real32_T)sin
    (stateEstimator_DW.Memory_PreviousInput[2]);
  rtb_DataTypeConversion1_o_idx_0_tmp = (real32_T)cos
    (stateEstimator_DW.Memory_PreviousInput[2]);
  stateEstimator_DW.Memory_PreviousInput[1] = (real32_T)sin
    (stateEstimator_DW.Memory_PreviousInput[1]);
  rtb_DataTypeConversion1_o_idx_1 = (real32_T)cos(stateEstimator_B.sincos_o1[1]);
  stateEstimator_DW.Memory_PreviousInput[2] = (real32_T)sin
    (stateEstimator_B.sincos_o1[2]);

  /* SignalConversion: '<S13>/ConcatBufferAtVector ConcatenateIn3' */
  stateEstimator_B.VectorConcatenate[2] = rtb_DataTypeConversion1_o_idx_1;

  /* SignalConversion: '<S13>/ConcatBufferAtVector ConcatenateIn4' */
  stateEstimator_B.VectorConcatenate[3] =
    stateEstimator_DW.Memory_PreviousInput[0];

  /* Product: '<S12>/Product1' incorporates:
   *  Trigonometry: '<S12>/Trigonometric Function'
   */
  stateEstimator_B.VectorConcatenate[4] = rtb_DataTypeConversion1_o_idx_0_tmp *
    rtb_DataTypeConversion1_o_idx_1;

  /* Product: '<S12>/Product3' */
  stateEstimator_B.VectorConcatenate[5] =
    stateEstimator_DW.Memory_PreviousInput[0] *
    stateEstimator_DW.Memory_PreviousInput[1];

  /* SignalConversion: '<S13>/ConcatBufferAtVector ConcatenateIn7' incorporates:
   *  Trigonometry: '<S12>/Trigonometric Function'
   */
  stateEstimator_B.VectorConcatenate[6] = rtb_DataTypeConversion1_o_idx_0_tmp;

  /* Product: '<S12>/Product2' incorporates:
   *  Gain: '<S12>/Gain'
   */
  stateEstimator_B.VectorConcatenate[7] = (-1.0F) *
    stateEstimator_DW.Memory_PreviousInput[0] * rtb_DataTypeConversion1_o_idx_1;

  /* Product: '<S12>/Product4' incorporates:
   *  Trigonometry: '<S12>/Trigonometric Function'
   */
  stateEstimator_B.VectorConcatenate[8] = rtb_DataTypeConversion1_o_idx_0_tmp *
    stateEstimator_DW.Memory_PreviousInput[1];

  /* Product: '<S12>/Divide' incorporates:
   *  Reshape: '<S13>/Reshape (9) to [3x3] column-major'
   */
  for (j = 0; j < 9; j++) {
    stateEstimator_B.VectorConcatenate[j] /= rtb_DataTypeConversion1_o_idx_1;
  }

  /* End of Product: '<S12>/Divide' */

  /* Gain: '<S5>/inverseIMU_gain' incorporates:
   *  Bias: '<S5>/Assuming that calib was done level!'
   *  DataTypeConversion: '<S5>/Data Type Conversion'
   *  Sum: '<S5>/Sum1'
   */
  stateEstimator_B.inverseIMU_gain[0] = (rtu_sensordata_datin->ddx -
    (rtu_sensorCalibration_datin[0] + 0.0F)) * 0.994075298F;
  stateEstimator_B.inverseIMU_gain[1] = (rtu_sensordata_datin->ddy -
    (rtu_sensorCalibration_datin[1] + 0.0F)) * 0.996184587F;
  stateEstimator_B.inverseIMU_gain[2] = (rtu_sensordata_datin->ddz -
    (rtu_sensorCalibration_datin[2] + 9.81F)) * 1.00549F;
  stateEstimator_B.inverseIMU_gain[3] = (rtu_sensordata_datin->p -
    (rtu_sensorCalibration_datin[3] + 0.0F)) * 1.00139189F;
  stateEstimator_B.inverseIMU_gain[4] = (rtu_sensordata_datin->q -
    (rtu_sensorCalibration_datin[4] + 0.0F)) * 0.993601203F;
  stateEstimator_B.inverseIMU_gain[5] = (rtu_sensordata_datin->r -
    (rtu_sensorCalibration_datin[5] + 0.0F)) * 1.00003F;

  /* DiscreteFilter: '<S5>/IIR_IMUgyro_r' */
  stateEstimator_DW.IIR_IMUgyro_r_tmp = 0.0F;
  rtb_DataTypeConversion1_o_idx_1 = stateEstimator_B.inverseIMU_gain[5];
  denIdx = 1;
  for (j = 0; j < 5; j++) {
    rtb_DataTypeConversion1_o_idx_1 -= rtCP_IIR_IMUgyro_r_DenCoef[denIdx] *
      stateEstimator_DW.IIR_IMUgyro_r_states[j];
    denIdx++;
  }

  stateEstimator_DW.IIR_IMUgyro_r_tmp = rtb_DataTypeConversion1_o_idx_1 / 1.0F;
  rtb_DataTypeConversion1_o_idx_0_tmp = 0.282124132F *
    stateEstimator_DW.IIR_IMUgyro_r_tmp;
  denIdx = 1;
  for (j = 0; j < 5; j++) {
    rtb_DataTypeConversion1_o_idx_0_tmp += rtCP_IIR_IMUgyro_r_NumCoef[denIdx] *
      stateEstimator_DW.IIR_IMUgyro_r_states[j];
    denIdx++;
  }

  /* Product: '<S2>/Product' incorporates:
   *  DiscreteFilter: '<S5>/IIR_IMUgyro_r'
   *  SignalConversion: '<S2>/TmpSignal ConversionAtProductInport2'
   */
  for (j = 0; j < 3; j++) {
    stateEstimator_B.sincos_o1_h[j] = stateEstimator_B.VectorConcatenate[j + 6] *
      rtb_DataTypeConversion1_o_idx_0_tmp +
      (stateEstimator_B.VectorConcatenate[j + 3] *
       stateEstimator_B.inverseIMU_gain[4] +
       stateEstimator_B.VectorConcatenate[j] * stateEstimator_B.inverseIMU_gain
       [3]);
  }

  /* End of Product: '<S2>/Product' */

  /* Sum: '<S2>/Sum' incorporates:
   *  Gain: '<S2>/Gain'
   */
  rtb_Sum_idx_2 = 0.005F * stateEstimator_B.sincos_o1_h[0] +
    stateEstimator_B.sincos_o1[2];

  /* DiscreteFir: '<S5>/FIR_IMUaccel' */
  rtb_DataTypeConversion1_o_idx_1 = stateEstimator_B.inverseIMU_gain[0] *
    0.0264077242F;
  cff = 1;
  for (j = stateEstimator_DW.FIR_IMUaccel_circBuf; j < 5; j++) {
    rtb_DataTypeConversion1_o_idx_1 += stateEstimator_DW.FIR_IMUaccel_states[j] *
      rtCP_FIR_IMUaccel_Coefficients[cff];
    cff++;
  }

  for (j = 0; j < stateEstimator_DW.FIR_IMUaccel_circBuf; j++) {
    rtb_DataTypeConversion1_o_idx_1 += stateEstimator_DW.FIR_IMUaccel_states[j] *
      rtCP_FIR_IMUaccel_Coefficients[cff];
    cff++;
  }

  stateEstimator_B.FIR_IMUaccel[0] = rtb_DataTypeConversion1_o_idx_1;

  /* Math: '<S2>/Math Function' incorporates:
   *  DiscreteFir: '<S5>/FIR_IMUaccel'
   */
  rtb_DataTypeConversion2 = (real32_T)floor(2.0F);
  if ((rtb_DataTypeConversion1_o_idx_1 < 0.0F) && (2.0F >
       rtb_DataTypeConversion2)) {
    stateEstimator_B.sincos_o1_h[0] = -rt_powf_snf
      (-rtb_DataTypeConversion1_o_idx_1, 2.0F);
  } else {
    stateEstimator_B.sincos_o1_h[0] = rt_powf_snf
      (rtb_DataTypeConversion1_o_idx_1, 2.0F);
  }

  /* DiscreteFir: '<S5>/FIR_IMUaccel' */
  rtb_DataTypeConversion1_o_idx_1 = stateEstimator_B.inverseIMU_gain[1] *
    0.0264077242F;
  cff = 1;
  for (j = stateEstimator_DW.FIR_IMUaccel_circBuf; j < 5; j++) {
    rtb_DataTypeConversion1_o_idx_1 += stateEstimator_DW.FIR_IMUaccel_states[5 +
      j] * rtCP_FIR_IMUaccel_Coefficients[cff];
    cff++;
  }

  for (j = 0; j < stateEstimator_DW.FIR_IMUaccel_circBuf; j++) {
    rtb_DataTypeConversion1_o_idx_1 += stateEstimator_DW.FIR_IMUaccel_states[5 +
      j] * rtCP_FIR_IMUaccel_Coefficients[cff];
    cff++;
  }

  stateEstimator_B.FIR_IMUaccel[1] = rtb_DataTypeConversion1_o_idx_1;

  /* Math: '<S2>/Math Function' incorporates:
   *  DiscreteFir: '<S5>/FIR_IMUaccel'
   */
  if ((rtb_DataTypeConversion1_o_idx_1 < 0.0F) && (2.0F >
       rtb_DataTypeConversion2)) {
    y = -rt_powf_snf(-rtb_DataTypeConversion1_o_idx_1, 2.0F);
  } else {
    y = rt_powf_snf(rtb_DataTypeConversion1_o_idx_1, 2.0F);
  }

  /* DiscreteFir: '<S5>/FIR_IMUaccel' */
  rtb_DataTypeConversion1_o_idx_1 = stateEstimator_B.inverseIMU_gain[2] *
    0.0264077242F;
  cff = 1;
  for (j = stateEstimator_DW.FIR_IMUaccel_circBuf; j < 5; j++) {
    rtb_DataTypeConversion1_o_idx_1 += stateEstimator_DW.FIR_IMUaccel_states[10
      + j] * rtCP_FIR_IMUaccel_Coefficients[cff];
    cff++;
  }

  for (j = 0; j < stateEstimator_DW.FIR_IMUaccel_circBuf; j++) {
    rtb_DataTypeConversion1_o_idx_1 += stateEstimator_DW.FIR_IMUaccel_states[10
      + j] * rtCP_FIR_IMUaccel_Coefficients[cff];
    cff++;
  }

  stateEstimator_B.FIR_IMUaccel[2] = rtb_DataTypeConversion1_o_idx_1;

  /* Math: '<S2>/Math Function' incorporates:
   *  DiscreteFir: '<S5>/FIR_IMUaccel'
   */
  if ((rtb_DataTypeConversion1_o_idx_1 < 0.0F) && (2.0F >
       rtb_DataTypeConversion2)) {
    rtb_DataTypeConversion2 = -rt_powf_snf(-rtb_DataTypeConversion1_o_idx_1,
      2.0F);
  } else {
    rtb_DataTypeConversion2 = rt_powf_snf(rtb_DataTypeConversion1_o_idx_1, 2.0F);
  }

  /* Sqrt: '<S2>/Sqrt' incorporates:
   *  Math: '<S2>/Math Function'
   *  Sum: '<S2>/Sum of Elements'
   */
  rtb_DataTypeConversion2 = (real32_T)sqrt((stateEstimator_B.sincos_o1_h[0] + y)
    + rtb_DataTypeConversion2);

  /* If: '<S2>/If' incorporates:
   *  Constant: '<S6>/Constant'
   *  Constant: '<S7>/Constant'
   *  Gain: '<S2>/Gain'
   *  Inport: '<S10>/prin'
   *  Logic: '<S2>/Logical Operator'
   *  RelationalOperator: '<S6>/Compare'
   *  RelationalOperator: '<S7>/Compare'
   *  Sum: '<S2>/Sum'
   */
  if ((rtb_DataTypeConversion2 > 9.79038F) && (rtb_DataTypeConversion2 <
       9.82962F)) {
    /* Outputs for IfAction SubSystem: '<S2>/If Action Subsystem' incorporates:
     *  ActionPort: '<S8>/Action Port'
     */
    /* Sum: '<S8>/Sum' incorporates:
     *  DiscreteFir: '<S5>/FIR_IMUaccel'
     *  Gain: '<S2>/Gain'
     *  Gain: '<S8>/Gain'
     *  Gain: '<S8>/Gain1'
     *  Product: '<S8>/Divide'
     *  Sum: '<S2>/Sum'
     *  Trigonometry: '<S8>/Trigonometric Function'
     */
    stateEstimator_B.Merge[1] = (0.005F * stateEstimator_B.sincos_o1_h[2] +
      stateEstimator_B.sincos_o1[0]) * 0.999F + (real32_T)atan
      (stateEstimator_B.FIR_IMUaccel[1] / rtb_DataTypeConversion1_o_idx_1) *
      0.001F;

    /* Gain: '<S8>/Gain2' */
    y = 0.101936802F * stateEstimator_B.FIR_IMUaccel[0];

    /* Trigonometry: '<S8>/Trigonometric Function1' */
    if (y > 1.0F) {
      y = 1.0F;
    } else {
      if (y < -1.0F) {
        y = -1.0F;
      }
    }

    /* Sum: '<S8>/Sum1' incorporates:
     *  Gain: '<S2>/Gain'
     *  Gain: '<S8>/Gain3'
     *  Gain: '<S8>/Gain4'
     *  Sum: '<S2>/Sum'
     *  Trigonometry: '<S8>/Trigonometric Function1'
     */
    stateEstimator_B.Merge[0] = (0.005F * stateEstimator_B.sincos_o1_h[1] +
      stateEstimator_B.sincos_o1[1]) * 0.999F + 0.001F * (real32_T)asin(y);

    /* End of Outputs for SubSystem: '<S2>/If Action Subsystem' */
  } else {
    /* Outputs for IfAction SubSystem: '<S2>/If Action Subsystem2' incorporates:
     *  ActionPort: '<S10>/Action Port'
     */
    stateEstimator_B.Merge[0] = 0.005F * stateEstimator_B.sincos_o1_h[1] +
      stateEstimator_B.sincos_o1[1];
    stateEstimator_B.Merge[1] = 0.005F * stateEstimator_B.sincos_o1_h[2] +
      stateEstimator_B.sincos_o1[0];

    /* End of Outputs for SubSystem: '<S2>/If Action Subsystem2' */
  }

  /* End of If: '<S2>/If' */

  /* If: '<S2>/If1' incorporates:
   *  Constant: '<S185>/Constant'
   *  Logic: '<S5>/Logical Operator'
   *  RelationalOperator: '<S185>/Compare'
   */
  if ((rtu_posVIS_datin[0] != (-99.0F)) && (*rtu_usePosVIS_flagin != 0.0F)) {
    /* Outputs for IfAction SubSystem: '<S2>/If Action Subsystem1' incorporates:
     *  ActionPort: '<S9>/Action Port'
     */
    /* DataTypeConversion: '<S2>/Data Type Conversion3' incorporates:
     *  Gain: '<S9>/Gain'
     *  Gain: '<S9>/Gain1'
     *  Sum: '<S9>/Sum'
     */
    stateEstimator_DW.Memory_PreviousInput[0] = 0.8F * rtb_Sum_idx_2 + 0.2F *
      rtu_posVIS_datin[3];

    /* End of Outputs for SubSystem: '<S2>/If Action Subsystem1' */
  } else {
    /* Outputs for IfAction SubSystem: '<S2>/If Action Subsystem3' incorporates:
     *  ActionPort: '<S11>/Action Port'
     */
    /* DataTypeConversion: '<S2>/Data Type Conversion3' incorporates:
     *  Inport: '<S11>/yawin'
     */
    stateEstimator_DW.Memory_PreviousInput[0] = rtb_Sum_idx_2;

    /* End of Outputs for SubSystem: '<S2>/If Action Subsystem3' */
  }

  /* End of If: '<S2>/If1' */

  /* DataTypeConversion: '<S2>/Data Type Conversion3' */
  stateEstimator_DW.Memory_PreviousInput[1] = stateEstimator_B.Merge[0];
  stateEstimator_DW.Memory_PreviousInput[2] = stateEstimator_B.Merge[1];

  /* Trigonometry: '<S135>/sincos' */
  stateEstimator_B.sincos_o1_h[0] = (real32_T)sin
    (stateEstimator_DW.Memory_PreviousInput[0]);
  stateEstimator_B.sincos_o1[0] = (real32_T)cos
    (stateEstimator_DW.Memory_PreviousInput[0]);
  rtb_DataTypeConversion2 = (real32_T)sin
    (stateEstimator_DW.Memory_PreviousInput[1]);
  stateEstimator_B.sincos_o1[1] = (real32_T)cos
    (stateEstimator_DW.Memory_PreviousInput[1]);
  y = (real32_T)sin(stateEstimator_DW.Memory_PreviousInput[2]);
  rtb_Sum_idx_2 = (real32_T)cos(stateEstimator_DW.Memory_PreviousInput[2]);

  /* Fcn: '<S135>/Fcn11' */
  stateEstimator_B.VectorConcatenate[0] = stateEstimator_B.sincos_o1[1] *
    stateEstimator_B.sincos_o1[0];

  /* Fcn: '<S135>/Fcn21' incorporates:
   *  Fcn: '<S135>/Fcn22'
   *  Trigonometry: '<S135>/sincos'
   */
  rtb_VectorConcatenate_tmp = y * rtb_DataTypeConversion2;
  stateEstimator_B.VectorConcatenate[1] = rtb_VectorConcatenate_tmp *
    stateEstimator_B.sincos_o1[0] - rtb_Sum_idx_2 *
    stateEstimator_B.sincos_o1_h[0];

  /* Fcn: '<S135>/Fcn31' incorporates:
   *  Fcn: '<S135>/Fcn32'
   *  Trigonometry: '<S135>/sincos'
   */
  rtb_VectorConcatenate_tmp_0 = rtb_Sum_idx_2 * rtb_DataTypeConversion2;
  stateEstimator_B.VectorConcatenate[2] = rtb_VectorConcatenate_tmp_0 *
    stateEstimator_B.sincos_o1[0] + y * stateEstimator_B.sincos_o1_h[0];

  /* Fcn: '<S135>/Fcn12' */
  stateEstimator_B.VectorConcatenate[3] = stateEstimator_B.sincos_o1[1] *
    stateEstimator_B.sincos_o1_h[0];

  /* Fcn: '<S135>/Fcn22' incorporates:
   *  Trigonometry: '<S135>/sincos'
   */
  stateEstimator_B.VectorConcatenate[4] = rtb_VectorConcatenate_tmp *
    stateEstimator_B.sincos_o1_h[0] + rtb_Sum_idx_2 *
    stateEstimator_B.sincos_o1[0];

  /* Fcn: '<S135>/Fcn32' incorporates:
   *  Trigonometry: '<S135>/sincos'
   */
  stateEstimator_B.VectorConcatenate[5] = rtb_VectorConcatenate_tmp_0 *
    stateEstimator_B.sincos_o1_h[0] - y * stateEstimator_B.sincos_o1[0];

  /* Fcn: '<S135>/Fcn13' incorporates:
   *  Trigonometry: '<S135>/sincos'
   */
  stateEstimator_B.VectorConcatenate[6] = -rtb_DataTypeConversion2;

  /* Fcn: '<S135>/Fcn23' incorporates:
   *  Trigonometry: '<S135>/sincos'
   */
  stateEstimator_B.VectorConcatenate[7] = y * stateEstimator_B.sincos_o1[1];

  /* Fcn: '<S135>/Fcn33' incorporates:
   *  Trigonometry: '<S135>/sincos'
   */
  stateEstimator_B.VectorConcatenate[8] = rtb_Sum_idx_2 *
    stateEstimator_B.sincos_o1[1];

  /* Math: '<S68>/Math Function' */
  for (j = 0; j < 3; j++) {
    stateEstimator_B.rtb_VectorConcatenate_m[3 * j] =
      stateEstimator_B.VectorConcatenate[j];
    stateEstimator_B.rtb_VectorConcatenate_m[1 + 3 * j] =
      stateEstimator_B.VectorConcatenate[j + 3];
    stateEstimator_B.rtb_VectorConcatenate_m[2 + 3 * j] =
      stateEstimator_B.VectorConcatenate[j + 6];
  }

  for (j = 0; j < 3; j++) {
    stateEstimator_B.VectorConcatenate[3 * j] =
      stateEstimator_B.rtb_VectorConcatenate_m[3 * j];
    stateEstimator_B.VectorConcatenate[1 + 3 * j] =
      stateEstimator_B.rtb_VectorConcatenate_m[3 * j + 1];
    stateEstimator_B.VectorConcatenate[2 + 3 * j] =
      stateEstimator_B.rtb_VectorConcatenate_m[3 * j + 2];

    /* Trigonometry: '<S16>/sincos' */
    stateEstimator_B.sincos_o1_h[j] = (real32_T)sin
      (stateEstimator_DW.Memory_PreviousInput[j]);
    stateEstimator_B.sincos_o1[j] = (real32_T)cos
      (stateEstimator_DW.Memory_PreviousInput[j]);
  }

  /* End of Math: '<S68>/Math Function' */

  /* Fcn: '<S16>/Fcn11' */
  stateEstimator_B.Reshape9to3x3columnmajor[0] = stateEstimator_B.sincos_o1[1] *
    stateEstimator_B.sincos_o1[0];

  /* Fcn: '<S16>/Fcn21' incorporates:
   *  Fcn: '<S16>/Fcn22'
   */
  rtb_DataTypeConversion2 = stateEstimator_B.sincos_o1_h[2] *
    stateEstimator_B.sincos_o1_h[1];
  stateEstimator_B.Reshape9to3x3columnmajor[1] = rtb_DataTypeConversion2 *
    stateEstimator_B.sincos_o1[0] - stateEstimator_B.sincos_o1[2] *
    stateEstimator_B.sincos_o1_h[0];

  /* Fcn: '<S16>/Fcn31' incorporates:
   *  Fcn: '<S16>/Fcn32'
   */
  y = stateEstimator_B.sincos_o1[2] * stateEstimator_B.sincos_o1_h[1];
  stateEstimator_B.Reshape9to3x3columnmajor[2] = y * stateEstimator_B.sincos_o1
    [0] + stateEstimator_B.sincos_o1_h[2] * stateEstimator_B.sincos_o1_h[0];

  /* Fcn: '<S16>/Fcn12' */
  stateEstimator_B.Reshape9to3x3columnmajor[3] = stateEstimator_B.sincos_o1[1] *
    stateEstimator_B.sincos_o1_h[0];

  /* Fcn: '<S16>/Fcn22' */
  stateEstimator_B.Reshape9to3x3columnmajor[4] = rtb_DataTypeConversion2 *
    stateEstimator_B.sincos_o1_h[0] + stateEstimator_B.sincos_o1[2] *
    stateEstimator_B.sincos_o1[0];

  /* Fcn: '<S16>/Fcn32' */
  stateEstimator_B.Reshape9to3x3columnmajor[5] = y *
    stateEstimator_B.sincos_o1_h[0] - stateEstimator_B.sincos_o1_h[2] *
    stateEstimator_B.sincos_o1[0];

  /* Fcn: '<S16>/Fcn13' */
  stateEstimator_B.Reshape9to3x3columnmajor[6] = -stateEstimator_B.sincos_o1_h[1];

  /* Fcn: '<S16>/Fcn23' */
  stateEstimator_B.Reshape9to3x3columnmajor[7] = stateEstimator_B.sincos_o1_h[2]
    * stateEstimator_B.sincos_o1[1];

  /* Fcn: '<S16>/Fcn33' */
  stateEstimator_B.Reshape9to3x3columnmajor[8] = stateEstimator_B.sincos_o1[2] *
    stateEstimator_B.sincos_o1[1];
  for (j = 0; j < 3; j++) {
    /* Trigonometry: '<S73>/sincos' */
    stateEstimator_B.sincos_o1[j] = (real32_T)sin
      (stateEstimator_DW.Memory_PreviousInput[j]);
    stateEstimator_B.sincos_o1_h[j] = (real32_T)cos
      (stateEstimator_DW.Memory_PreviousInput[j]);

    /* Sum: '<S3>/Sum' incorporates:
     *  Constant: '<S3>/gravity'
     *  DiscreteFir: '<S5>/FIR_IMUaccel'
     *  Math: '<S3>/Math Function'
     *  Product: '<S3>/Product'
     */
    stateEstimator_B.Sum[j] = ((stateEstimator_B.Reshape9to3x3columnmajor[3 * j
      + 1] * stateEstimator_B.FIR_IMUaccel[1] +
      stateEstimator_B.Reshape9to3x3columnmajor[3 * j] *
      stateEstimator_B.FIR_IMUaccel[0]) +
      stateEstimator_B.Reshape9to3x3columnmajor[3 * j + 2] *
      rtb_DataTypeConversion1_o_idx_1) + rtCP_gravity_Value_k[j];
  }

  /* Fcn: '<S73>/Fcn11' */
  stateEstimator_B.Reshape9to3x3columnmajor_g[0] = stateEstimator_B.sincos_o1_h
    [1] * stateEstimator_B.sincos_o1_h[0];

  /* Fcn: '<S73>/Fcn21' incorporates:
   *  Fcn: '<S73>/Fcn22'
   */
  rtb_DataTypeConversion1_o_idx_1 = stateEstimator_B.sincos_o1[2] *
    stateEstimator_B.sincos_o1[1];
  stateEstimator_B.Reshape9to3x3columnmajor_g[1] =
    rtb_DataTypeConversion1_o_idx_1 * stateEstimator_B.sincos_o1_h[0] -
    stateEstimator_B.sincos_o1_h[2] * stateEstimator_B.sincos_o1[0];

  /* Fcn: '<S73>/Fcn31' incorporates:
   *  Fcn: '<S73>/Fcn32'
   */
  rtb_DataTypeConversion2 = stateEstimator_B.sincos_o1_h[2] *
    stateEstimator_B.sincos_o1[1];
  stateEstimator_B.Reshape9to3x3columnmajor_g[2] = rtb_DataTypeConversion2 *
    stateEstimator_B.sincos_o1_h[0] + stateEstimator_B.sincos_o1[2] *
    stateEstimator_B.sincos_o1[0];

  /* Fcn: '<S73>/Fcn12' */
  stateEstimator_B.Reshape9to3x3columnmajor_g[3] = stateEstimator_B.sincos_o1_h
    [1] * stateEstimator_B.sincos_o1[0];

  /* Fcn: '<S73>/Fcn22' */
  stateEstimator_B.Reshape9to3x3columnmajor_g[4] =
    rtb_DataTypeConversion1_o_idx_1 * stateEstimator_B.sincos_o1[0] +
    stateEstimator_B.sincos_o1_h[2] * stateEstimator_B.sincos_o1_h[0];

  /* Fcn: '<S73>/Fcn32' */
  stateEstimator_B.Reshape9to3x3columnmajor_g[5] = rtb_DataTypeConversion2 *
    stateEstimator_B.sincos_o1[0] - stateEstimator_B.sincos_o1[2] *
    stateEstimator_B.sincos_o1_h[0];

  /* Fcn: '<S73>/Fcn13' */
  stateEstimator_B.Reshape9to3x3columnmajor_g[6] = -stateEstimator_B.sincos_o1[1];

  /* Fcn: '<S73>/Fcn23' */
  stateEstimator_B.Reshape9to3x3columnmajor_g[7] = stateEstimator_B.sincos_o1[2]
    * stateEstimator_B.sincos_o1_h[1];

  /* Fcn: '<S73>/Fcn33' */
  stateEstimator_B.Reshape9to3x3columnmajor_g[8] = stateEstimator_B.sincos_o1_h
    [2] * stateEstimator_B.sincos_o1_h[1];

  /* Gain: '<S3>/invertzaxisGain' */
  stateEstimator_B.invertzaxisGain = (real_T)(-1.0F) *
    rtu_sensordata_datin->altitude_sonar;

  /* DiscreteFilter: '<S15>/pressureFilter_IIR' incorporates:
   *  DataTypeConversion: '<S5>/Data Type Conversion'
   *  Gain: '<S3>/prsToAltGain'
   *  Sum: '<S5>/Sum2'
   */
  stateEstimator_DW.pressureFilter_IIR_tmp = ((((rtu_sensordata_datin->prs -
    rtu_sensorCalibration_datin[6]) * 0.0832137167F - (-2.93717074F) *
    stateEstimator_DW.pressureFilter_IIR_states[0]) - 2.87629962F *
    stateEstimator_DW.pressureFilter_IIR_states[1]) - (-0.939098954F) *
    stateEstimator_DW.pressureFilter_IIR_states[2]) / 1.0F;

  /* DiscreteFilter: '<S15>/soonarFilter_IIR' */
  stateEstimator_DW.soonarFilter_IIR_tmp = (((stateEstimator_B.invertzaxisGain -
    (-2.9371707284498907) * stateEstimator_DW.soonarFilter_IIR_states[0]) -
    2.8762997234793319 * stateEstimator_DW.soonarFilter_IIR_states[1]) -
    (-0.939098940325283) * stateEstimator_DW.soonarFilter_IIR_states[2]) / 1.0;

  /* Saturate: '<S15>/SaturationSonar' */
  if ((stateEstimator_B.invertzaxisGain >= (-0.44)) || rtIsNaN(-0.44)) {
    stateEstimator_B.Add1 = (-0.44);
  } else {
    stateEstimator_B.Add1 = stateEstimator_B.invertzaxisGain;
  }

  /* End of Saturate: '<S15>/SaturationSonar' */

  /* Logic: '<S15>/nicemeasurementor newupdateneeded' incorporates:
   *  Abs: '<S15>/Absestdiff'
   *  Abs: '<S15>/Absestdiff1'
   *  Abs: '<S15>/Absestdiff2'
   *  Constant: '<S62>/Constant'
   *  Constant: '<S63>/Constant'
   *  Constant: '<S64>/Constant'
   *  Constant: '<S65>/Constant'
   *  Delay: '<S3>/Delay2'
   *  DiscreteFilter: '<S15>/pressureFilter_IIR'
   *  DiscreteFilter: '<S15>/soonarFilter_IIR'
   *  Logic: '<S15>/findingoutliers'
   *  Logic: '<S15>/newupdateneeded'
   *  RelationalOperator: '<S62>/Compare'
   *  RelationalOperator: '<S63>/Compare'
   *  RelationalOperator: '<S64>/Compare'
   *  RelationalOperator: '<S65>/Compare'
   *  Sum: '<S15>/Add'
   *  Sum: '<S15>/Add1'
   *  Sum: '<S15>/Add2'
   */
  stateEstimator_B.nicemeasurementornewupdateneeded = (((fabs
    (stateEstimator_DW.Delay2_DSTATE - stateEstimator_B.Add1) <= 0.3) &&
    (stateEstimator_B.invertzaxisGain < (-0.44))) || ((fabs((((3.75683794E-6F *
    stateEstimator_DW.pressureFilter_IIR_tmp + 1.12705138E-5F *
    stateEstimator_DW.pressureFilter_IIR_states[0]) + 1.12705138E-5F *
    stateEstimator_DW.pressureFilter_IIR_states[1]) + 3.75683794E-6F *
    stateEstimator_DW.pressureFilter_IIR_states[2]) -
    stateEstimator_DW.Delay2_DSTATE) >= 0.8) && (fabs((((3.7568380197861018E-6 *
    stateEstimator_DW.soonarFilter_IIR_tmp + 1.1270514059358305E-5 *
    stateEstimator_DW.soonarFilter_IIR_states[0]) + 1.1270514059358305E-5 *
    stateEstimator_DW.soonarFilter_IIR_states[1]) + 3.7568380197861018E-6 *
    stateEstimator_DW.soonarFilter_IIR_states[2]) -
    stateEstimator_DW.Delay2_DSTATE) >= 0.4)));

  /* RelationalOperator: '<S17>/Compare' incorporates:
   *  Constant: '<S17>/Constant'
   *  Delay: '<S3>/Delay2'
   */
  stateEstimator_B.Compare = (stateEstimator_DW.Delay2_DSTATE > 0.0);

  /* Delay: '<S14>/MemoryX' incorporates:
   *  DataTypeConversion: '<S14>/DataTypeConversionReset'
   */
  if (stateEstimator_B.Compare) {
    stateEstimator_DW.icLoad = 1U;
  }

  if (stateEstimator_DW.icLoad != 0) {
    stateEstimator_DW.MemoryX_DSTATE[0] = stateEstimator_ConstB.ReshapeX0[0];
    stateEstimator_DW.MemoryX_DSTATE[1] = stateEstimator_ConstB.ReshapeX0[1];
  }

  /* Outputs for Atomic SubSystem: '<S14>/UseCurrentEstimator' */
  /* Outputs for Enabled SubSystem: '<S40>/Enabled Subsystem' incorporates:
   *  EnablePort: '<S61>/Enable'
   */
  /* DataTypeConversion: '<S14>/DataTypeConversionEnable' */
  if (stateEstimator_B.nicemeasurementornewupdateneeded) {
    if (!stateEstimator_DW.EnabledSubsystem_MODE) {
      stateEstimator_DW.EnabledSubsystem_MODE = true;
    }

    /* Sum: '<S61>/Add1' incorporates:
     *  Constant: '<S14>/C'
     *  Constant: '<S14>/D'
     *  Delay: '<S14>/MemoryX'
     *  Product: '<S61>/Product'
     *  Product: '<S61>/Product1'
     *  Reshape: '<S14>/Reshapeu'
     *  Reshape: '<S14>/Reshapey'
     */
    stateEstimator_B.Add1 = (stateEstimator_B.invertzaxisGain - (1.0 *
      stateEstimator_DW.MemoryX_DSTATE[0] + 0.0 *
      stateEstimator_DW.MemoryX_DSTATE[1])) - 0.0 * stateEstimator_B.Sum[2];

    /* Product: '<S61>/Product2' */
    stateEstimator_B.Product2[0] = stateEstimator_ConstB.Conversion[0] *
      stateEstimator_B.Add1;
    stateEstimator_B.Product2[1] = stateEstimator_ConstB.Conversion[1] *
      stateEstimator_B.Add1;
  } else {
    if (stateEstimator_DW.EnabledSubsystem_MODE) {
      /* Disable for Outport: '<S61>/deltax' */
      stateEstimator_B.Product2[0] = 0.0;
      stateEstimator_B.Product2[1] = 0.0;
      stateEstimator_DW.EnabledSubsystem_MODE = false;
    }
  }

  /* End of Outputs for SubSystem: '<S40>/Enabled Subsystem' */
  for (i = 0; i < 2; i++) {
    /* Sum: '<S40>/Add' incorporates:
     *  Delay: '<S14>/MemoryX'
     */
    stateEstimator_B.Add[i] = stateEstimator_B.Product2[i] +
      stateEstimator_DW.MemoryX_DSTATE[i];

    /* DiscreteFilter: '<S70>/IIRgyroz' */
    cff = i * 5;
    rtb_DataTypeConversion1_o_idx_1 = stateEstimator_B.inverseIMU_gain[i + 3];
    denIdx = 1;
    for (j = 0; j < 5; j++) {
      rtb_DataTypeConversion1_o_idx_1 -= stateEstimator_DW.IIRgyroz_states[cff +
        j] * rtCP_IIRgyroz_DenCoef[denIdx];
      denIdx++;
    }

    stateEstimator_DW.IIRgyroz_tmp[i] = rtb_DataTypeConversion1_o_idx_1 / 1.0F;
    rtb_DataTypeConversion1_o_idx_1 = 0.282124132F *
      stateEstimator_DW.IIRgyroz_tmp[i];
    denIdx = 1;
    for (j = 0; j < 5; j++) {
      rtb_DataTypeConversion1_o_idx_1 += stateEstimator_DW.IIRgyroz_states[cff +
        j] * rtCP_IIRgyroz_NumCoef[denIdx];
      denIdx++;
    }

    /* SampleTimeMath: '<S77>/TSamp' incorporates:
     *  DiscreteFilter: '<S70>/IIRgyroz'
     *
     * About '<S77>/TSamp':
     *  y = u * K where K = 1 / ( w * Ts )
     */
    rtb_DataTypeConversion1_o_idx_1 *= 200.0F;

    /* Sum: '<S77>/Diff' incorporates:
     *  UnitDelay: '<S77>/UD'
     */
    stateEstimator_DW.UD_DSTATE[i] = rtb_DataTypeConversion1_o_idx_1 -
      stateEstimator_DW.UD_DSTATE[i];

    /* SampleTimeMath: '<S77>/TSamp'
     *
     * About '<S77>/TSamp':
     *  y = u * K where K = 1 / ( w * Ts )
     */
    stateEstimator_B.TSamp[i] = rtb_DataTypeConversion1_o_idx_1;
  }

  /* End of Outputs for SubSystem: '<S14>/UseCurrentEstimator' */

  /* Gain: '<S67>/opticalFlowToVelocity_gain' */
  stateEstimator_B.opticalFlowToVelocity_gain[0] = 1.0F * rtu_opticalFlow_datin
    [0];
  stateEstimator_B.opticalFlowToVelocity_gain[1] = 1.0F * rtu_opticalFlow_datin
    [1];
  stateEstimator_B.opticalFlowToVelocity_gain[2] = 1.0F * rtu_opticalFlow_datin
    [2];

  /* Abs: '<S70>/Abs' incorporates:
   *  Abs: '<S70>/Abs4'
   */
  rtb_DataTypeConversion2 = (real32_T)fabs(stateEstimator_B.inverseIMU_gain[3]);

  /* Abs: '<S70>/Abs1' incorporates:
   *  Abs: '<S70>/Abs5'
   */
  y = (real32_T)fabs(stateEstimator_B.inverseIMU_gain[4]);

  /* Abs: '<S70>/Abs2' incorporates:
   *  Abs: '<S134>/Abs'
   */
  rtb_DataTypeConversion1_o_idx_1 = (real32_T)fabs
    (stateEstimator_DW.Memory_PreviousInput[1]);

  /* Abs: '<S70>/Abs3' incorporates:
   *  Abs: '<S134>/Abs1'
   */
  rtb_Sum_idx_2 = (real32_T)fabs(stateEstimator_DW.Memory_PreviousInput[2]);

  /* Logic: '<S70>/Logical Operator3' incorporates:
   *  Abs: '<S70>/Abs'
   *  Abs: '<S70>/Abs1'
   *  Abs: '<S70>/Abs2'
   *  Abs: '<S70>/Abs3'
   *  Abs: '<S70>/Abs6'
   *  Abs: '<S70>/Abs7'
   *  Abs: '<S70>/Abs8'
   *  Abs: '<S70>/Abs9'
   *  Constant: '<S78>/Constant'
   *  Constant: '<S79>/Constant'
   *  Constant: '<S80>/Constant'
   *  Constant: '<S81>/Constant'
   *  Constant: '<S82>/Constant'
   *  Constant: '<S83>/Constant'
   *  Constant: '<S84>/Constant'
   *  Constant: '<S85>/Constant'
   *  Constant: '<S86>/Constant'
   *  Constant: '<S87>/Constant'
   *  Constant: '<S88>/Constant'
   *  DataTypeConversion: '<S3>/Data Type Conversion'
   *  Delay: '<S67>/Delay'
   *  Logic: '<S70>/Logical Operator'
   *  Logic: '<S70>/Logical Operator1'
   *  Logic: '<S70>/Logical Operator2'
   *  RelationalOperator: '<S78>/Compare'
   *  RelationalOperator: '<S79>/Compare'
   *  RelationalOperator: '<S80>/Compare'
   *  RelationalOperator: '<S81>/Compare'
   *  RelationalOperator: '<S82>/Compare'
   *  RelationalOperator: '<S83>/Compare'
   *  RelationalOperator: '<S84>/Compare'
   *  RelationalOperator: '<S85>/Compare'
   *  RelationalOperator: '<S86>/Compare'
   *  RelationalOperator: '<S87>/Compare'
   *  RelationalOperator: '<S88>/Compare'
   *  Reshape: '<S14>/Reshapexhat'
   *  Sum: '<S70>/Add'
   */
  stateEstimator_B.LogicalOperator3 = ((((rtb_DataTypeConversion1_o_idx_1 <=
    0.6F) && (rtb_Sum_idx_2 <= 0.6F) && (rtb_DataTypeConversion2 <= 7.0F) && (y <=
    7.0F) && ((real32_T)fabs(stateEstimator_DW.UD_DSTATE[0]) <= 80.0F) &&
    ((real32_T)fabs(stateEstimator_DW.UD_DSTATE[1]) <= 80.0F)) ||
    ((rtb_DataTypeConversion2 <= 0.5F) && (y <= 0.5F))) && ((real32_T)fabs
    (stateEstimator_B.opticalFlowToVelocity_gain[0] -
     stateEstimator_DW.Delay_DSTATE[0]) <= 5.0F) && ((real32_T)fabs
    (stateEstimator_B.opticalFlowToVelocity_gain[1] -
     stateEstimator_DW.Delay_DSTATE[1]) <= 5.0F) && ((real32_T)
    stateEstimator_B.Add[0] <= (-0.4F)));

  /* DataTypeConversion: '<S71>/DataTypeConversionEnable' */
  rtb_DataTypeConversionEnable_i = stateEstimator_B.LogicalOperator3;

  /* Logic: '<S69>/Logical Operator' incorporates:
   *  Constant: '<S74>/Constant'
   *  Constant: '<S75>/Constant'
   *  RelationalOperator: '<S74>/Compare'
   *  RelationalOperator: '<S75>/Compare'
   */
  rtb_LogicalOperator_cg = ((stateEstimator_B.opticalFlowToVelocity_gain[0] !=
    0.0F) || (stateEstimator_B.opticalFlowToVelocity_gain[1] != 0.0F));

  /* RelationalOperator: '<S72>/Compare' incorporates:
   *  Constant: '<S72>/Constant'
   *  DataTypeConversion: '<S3>/Data Type Conversion'
   *  Reshape: '<S14>/Reshapexhat'
   */
  rtb_Compare_i = ((real32_T)stateEstimator_B.Add[0] <= (-0.4F));

  /* Sum: '<S69>/Add' incorporates:
   *  Constant: '<S69>/gravity'
   *  Gain: '<S69>/gainaccinput'
   *  Product: '<S69>/Product1'
   */
  for (j = 0; j < 3; j++) {
    stateEstimator_B.rtb_FIR_IMUaccel_c[j] = stateEstimator_B.FIR_IMUaccel[j] -
      ((stateEstimator_B.Reshape9to3x3columnmajor_g[j + 3] * 0.0 +
        stateEstimator_B.Reshape9to3x3columnmajor_g[j] * 0.0) +
       stateEstimator_B.Reshape9to3x3columnmajor_g[j + 6] * (-9.81));
  }

  /* End of Sum: '<S69>/Add' */

  /* Product: '<S69>/Product' incorporates:
   *  Gain: '<S69>/gainaccinput'
   */
  stateEstimator_B.Product[0] = (real32_T)(0.2 *
    stateEstimator_B.rtb_FIR_IMUaccel_c[0]) * (real32_T)rtb_LogicalOperator_cg *
    (real32_T)rtb_Compare_i;
  stateEstimator_B.Product[1] = (real32_T)(0.2 *
    stateEstimator_B.rtb_FIR_IMUaccel_c[1]) * (real32_T)rtb_LogicalOperator_cg *
    (real32_T)rtb_Compare_i;

  /* Reshape: '<S71>/Reshapeu' */
  stateEstimator_B.Reshapeu_h[0] = stateEstimator_B.Product[0];
  stateEstimator_B.Reshapeu_h[1] = stateEstimator_B.Product[1];

  /* Reshape: '<S71>/Reshapey' */
  stateEstimator_B.Reshapey_o[0] = stateEstimator_B.opticalFlowToVelocity_gain[0];
  stateEstimator_B.Reshapey_o[1] = stateEstimator_B.opticalFlowToVelocity_gain[1];

  /* Delay: '<S71>/MemoryX' */
  if (stateEstimator_DW.icLoad_i != 0) {
    stateEstimator_DW.MemoryX_DSTATE_d[0] = stateEstimator_ConstB.ReshapeX0_k[0];
    stateEstimator_DW.MemoryX_DSTATE_d[1] = stateEstimator_ConstB.ReshapeX0_k[1];
  }

  stateEstimator_B.MemoryX_m[0] = stateEstimator_DW.MemoryX_DSTATE_d[0];
  stateEstimator_B.MemoryX_m[1] = stateEstimator_DW.MemoryX_DSTATE_d[1];

  /* End of Delay: '<S71>/MemoryX' */

  /* Outputs for Atomic SubSystem: '<S71>/UseCurrentEstimator' */

  /* Constant: '<S71>/C' incorporates:
   *  Constant: '<S71>/D'
   */
  stateEstimator_UseCurrentEstimator(rtb_DataTypeConversionEnable_i,
    stateEstimator_ConstB.Conversion_p, stateEstimator_B.Reshapeu_h,
    stateEstimator_B.Reshapey_o, rtCP_C_Value_f, rtCP_D_Value_f,
    stateEstimator_B.MemoryX_m, &stateEstimator_B.UseCurrentEstimator_l,
    &stateEstimator_DW.UseCurrentEstimator_l);

  /* End of Outputs for SubSystem: '<S71>/UseCurrentEstimator' */

  /* Product: '<S3>/Product1' incorporates:
   *  DataTypeConversion: '<S3>/Data Type Conversion1'
   *  DataTypeConversion: '<S3>/Data Type Conversion2'
   *  Delay: '<S1>/Delay1'
   *  Reshape: '<S14>/Reshapexhat'
   *  SignalConversion: '<S3>/TmpSignal ConversionAtProduct1Inport2'
   */
  for (j = 0; j < 3; j++) {
    stateEstimator_B.rtb_FIR_IMUaccel_c[j] =
      stateEstimator_B.Reshape9to3x3columnmajor[j + 6] * stateEstimator_B.Add[1]
      + ((real_T)stateEstimator_B.Reshape9to3x3columnmajor[j + 3] *
         stateEstimator_DW.Delay1_DSTATE[1] + (real_T)
         stateEstimator_B.Reshape9to3x3columnmajor[j] *
         stateEstimator_DW.Delay1_DSTATE[0]);
  }

  /* Product: '<S68>/Product' incorporates:
   *  DataTypeConversion: '<S3>/Data Type Conversion2'
   *  Reshape: '<S71>/Reshapexhat'
   *  SignalConversion: '<S68>/TmpSignal ConversionAtProductInport2'
   */
  for (j = 0; j < 3; j++) {
    stateEstimator_B.Product_l[j] = 0.0F;
    stateEstimator_B.Product_l[j] += stateEstimator_B.VectorConcatenate[j] *
      stateEstimator_B.UseCurrentEstimator_l.Add[0];
    stateEstimator_B.Product_l[j] += stateEstimator_B.VectorConcatenate[j + 3] *
      stateEstimator_B.UseCurrentEstimator_l.Add[1];
    stateEstimator_B.Product_l[j] += stateEstimator_B.VectorConcatenate[j + 6] *
      (real32_T)stateEstimator_B.rtb_FIR_IMUaccel_c[2];
  }

  /* End of Product: '<S68>/Product' */

  /* Reshape: '<S133>/Reshapeu' */
  stateEstimator_B.Reshapeu_i[0] = stateEstimator_B.Product_l[0];
  stateEstimator_B.Reshapeu_i[1] = stateEstimator_B.Product_l[1];

  /* Sum: '<S134>/Add1' incorporates:
   *  Delay: '<S68>/Delay'
   */
  y = rtu_posVIS_datin[0] - stateEstimator_DW.Delay_DSTATE_l[0];
  rtb_VectorConcatenate_tmp = rtu_posVIS_datin[1] -
    stateEstimator_DW.Delay_DSTATE_l[1];

  /* Math: '<S134>/Math Function' */
  rtb_DataTypeConversion2 = (real32_T)floor(2.0F);
  if ((y < 0.0F) && (2.0F > rtb_DataTypeConversion2)) {
    y = -rt_powf_snf(-y, 2.0F);
  } else {
    y = rt_powf_snf(y, 2.0F);
  }

  if ((rtb_VectorConcatenate_tmp < 0.0F) && (2.0F > rtb_DataTypeConversion2)) {
    rtb_VectorConcatenate_tmp = -rt_powf_snf(-rtb_VectorConcatenate_tmp, 2.0F);
  } else {
    rtb_VectorConcatenate_tmp = rt_powf_snf(rtb_VectorConcatenate_tmp, 2.0F);
  }

  /* End of Math: '<S134>/Math Function' */

  /* Logic: '<S134>/Logical Operator3' incorporates:
   *  Constant: '<S180>/Constant'
   *  Constant: '<S181>/Constant'
   *  Constant: '<S182>/Constant'
   *  Constant: '<S183>/Constant'
   *  RelationalOperator: '<S180>/Compare'
   *  RelationalOperator: '<S181>/Compare'
   *  RelationalOperator: '<S182>/Compare'
   *  RelationalOperator: '<S183>/Compare'
   *  Sqrt: '<S134>/Sqrt'
   *  Sum: '<S134>/Sum of Elements'
   */
  stateEstimator_B.LogicalOperator3_o = ((rtu_posVIS_datin[0] != (-99.0F)) &&
    (rtb_DataTypeConversion1_o_idx_1 <= 0.18F) && (rtb_Sum_idx_2 <= 0.18F) &&
    ((real32_T)sqrt(y + rtb_VectorConcatenate_tmp) < 1.0F));

  /* DataTypeConversion: '<S133>/DataTypeConversionEnable' */
  rtb_DataTypeConversionEnable_h = stateEstimator_B.LogicalOperator3_o;

  /* Reshape: '<S133>/Reshapey' */
  stateEstimator_B.Reshapey_a[0] = rtu_posVIS_datin[0];
  stateEstimator_B.Reshapey_a[1] = rtu_posVIS_datin[1];

  /* Delay: '<S133>/MemoryX' */
  if (stateEstimator_DW.icLoad_g != 0) {
    stateEstimator_DW.MemoryX_DSTATE_dl[0] = stateEstimator_ConstB.ReshapeX0_b[0];
    stateEstimator_DW.MemoryX_DSTATE_dl[1] = stateEstimator_ConstB.ReshapeX0_b[1];
  }

  stateEstimator_B.MemoryX_f[0] = stateEstimator_DW.MemoryX_DSTATE_dl[0];
  stateEstimator_B.MemoryX_f[1] = stateEstimator_DW.MemoryX_DSTATE_dl[1];

  /* End of Delay: '<S133>/MemoryX' */

  /* Outputs for Atomic SubSystem: '<S133>/UseCurrentEstimator' */

  /* Constant: '<S133>/C' incorporates:
   *  Constant: '<S133>/D'
   */
  stateEstimator_UseCurrentEstimator(rtb_DataTypeConversionEnable_h,
    stateEstimator_ConstB.Conversion_g, stateEstimator_B.Reshapeu_i,
    stateEstimator_B.Reshapey_a, rtCP_C_Value_p, rtCP_D_Value_b,
    stateEstimator_B.MemoryX_f, &stateEstimator_B.UseCurrentEstimator_j,
    &stateEstimator_DW.UseCurrentEstimator_j);

  /* End of Outputs for SubSystem: '<S133>/UseCurrentEstimator' */

  /* Reshape: '<S133>/Reshapeyhat' incorporates:
   *  Constant: '<S133>/C'
   *  Constant: '<S133>/D'
   *  Product: '<S137>/Product'
   *  Product: '<S137>/Product1'
   *  Sum: '<S137>/Add1'
   */
  stateEstimator_B.Reshapeyhat_l[0] = (1.0F *
    stateEstimator_B.UseCurrentEstimator_j.Add[0] + 0.0F *
    stateEstimator_B.UseCurrentEstimator_j.Add[1]) + (0.0F *
    stateEstimator_B.Reshapeu_i[0] + 0.0F * stateEstimator_B.Reshapeu_i[1]);
  stateEstimator_B.Reshapeyhat_l[1] = (0.0F *
    stateEstimator_B.UseCurrentEstimator_j.Add[0] + 1.0F *
    stateEstimator_B.UseCurrentEstimator_j.Add[1]) + (0.0F *
    stateEstimator_B.Reshapeu_i[0] + 0.0F * stateEstimator_B.Reshapeu_i[1]);

  /* DiscreteIntegrator: '<S68>/SimplyIntegrateVelocity' */
  if ((*rtu_controlModePosVSOrient_flagin) &&
      (stateEstimator_DW.SimplyIntegrateVelocity_PrevResetState <= 0)) {
    stateEstimator_DW.SimplyIntegrateVelocity_DSTATE[0] = 0.0F;
    stateEstimator_DW.SimplyIntegrateVelocity_DSTATE[1] = 0.0F;
  }

  /* Switch: '<S68>/UseIPPosSwitch' incorporates:
   *  DiscreteIntegrator: '<S68>/SimplyIntegrateVelocity'
   *  Reshape: '<S133>/Reshapexhat'
   */
  if (*rtu_usePosVIS_flagin > 0.0F) {
    stateEstimator_B.SimplyIntegrateVelocity[0] =
      stateEstimator_B.UseCurrentEstimator_j.Add[0];
    stateEstimator_B.SimplyIntegrateVelocity[1] =
      stateEstimator_B.UseCurrentEstimator_j.Add[1];
  } else {
    stateEstimator_B.SimplyIntegrateVelocity[0] =
      stateEstimator_DW.SimplyIntegrateVelocity_DSTATE[0];
    stateEstimator_B.SimplyIntegrateVelocity[1] =
      stateEstimator_DW.SimplyIntegrateVelocity_DSTATE[1];
  }

  /* End of Switch: '<S68>/UseIPPosSwitch' */

  /* DataTypeConversion: '<S149>/Conversion' */
  stateEstimator_B.Conversion_p[0] = stateEstimator_B.MemoryX_f[0];
  stateEstimator_B.Conversion_p[1] = stateEstimator_B.MemoryX_f[1];

  /* Outputs for Enabled SubSystem: '<S153>/MeasurementUpdate' */

  /* Constant: '<S133>/C' incorporates:
   *  Constant: '<S133>/D'
   */
  stateEstimator_MeasurementUpdate(rtb_DataTypeConversionEnable_h,
    stateEstimator_ConstB.Conversion_a, stateEstimator_B.Reshapey_a,
    rtCP_C_Value_p, stateEstimator_B.Conversion_p, rtCP_D_Value_b,
    stateEstimator_B.Reshapeu_i, &stateEstimator_B.MeasurementUpdate_b,
    &stateEstimator_DW.MeasurementUpdate_b);

  /* End of Outputs for SubSystem: '<S153>/MeasurementUpdate' */

  /* Reshape: '<S71>/Reshapeyhat' incorporates:
   *  Constant: '<S71>/C'
   *  Constant: '<S71>/D'
   *  Product: '<S90>/Product'
   *  Product: '<S90>/Product1'
   *  Sum: '<S90>/Add1'
   */
  stateEstimator_B.Reshapeyhat_a[0] = (1.0F *
    stateEstimator_B.UseCurrentEstimator_l.Add[0] + 0.0F *
    stateEstimator_B.UseCurrentEstimator_l.Add[1]) + (0.0F *
    stateEstimator_B.Reshapeu_h[0] + 0.0F * stateEstimator_B.Reshapeu_h[1]);
  stateEstimator_B.Reshapeyhat_a[1] = (0.0F *
    stateEstimator_B.UseCurrentEstimator_l.Add[0] + 1.0F *
    stateEstimator_B.UseCurrentEstimator_l.Add[1]) + (0.0F *
    stateEstimator_B.Reshapeu_h[0] + 0.0F * stateEstimator_B.Reshapeu_h[1]);

  /* DataTypeConversion: '<S102>/Conversion' */
  stateEstimator_B.Conversion_j[0] = stateEstimator_B.MemoryX_m[0];
  stateEstimator_B.Conversion_j[1] = stateEstimator_B.MemoryX_m[1];

  /* Outputs for Enabled SubSystem: '<S106>/MeasurementUpdate' */

  /* Constant: '<S71>/C' incorporates:
   *  Constant: '<S71>/D'
   */
  stateEstimator_MeasurementUpdate(rtb_DataTypeConversionEnable_i,
    stateEstimator_ConstB.Conversion_n4, stateEstimator_B.Reshapey_o,
    rtCP_C_Value_f, stateEstimator_B.Conversion_j, rtCP_D_Value_f,
    stateEstimator_B.Reshapeu_h, &stateEstimator_B.MeasurementUpdate_e,
    &stateEstimator_DW.MeasurementUpdate_e);

  /* End of Outputs for SubSystem: '<S106>/MeasurementUpdate' */

  /* Outputs for Enabled SubSystem: '<S35>/MeasurementUpdate' incorporates:
   *  EnablePort: '<S60>/Enable'
   */
  /* DataTypeConversion: '<S14>/DataTypeConversionEnable' */
  if (stateEstimator_B.nicemeasurementornewupdateneeded) {
    if (!stateEstimator_DW.MeasurementUpdate_MODE) {
      stateEstimator_DW.MeasurementUpdate_MODE = true;
    }

    /* Sum: '<S60>/Sum' incorporates:
     *  Constant: '<S14>/C'
     *  Constant: '<S14>/D'
     *  Delay: '<S14>/MemoryX'
     *  Product: '<S60>/C[k]*xhat[k|k-1]'
     *  Product: '<S60>/D[k]*u[k]'
     *  Reshape: '<S14>/Reshapeu'
     *  Reshape: '<S14>/Reshapey'
     *  Sum: '<S60>/Add1'
     */
    stateEstimator_B.Add1 = stateEstimator_B.invertzaxisGain - ((1.0 *
      stateEstimator_DW.MemoryX_DSTATE[0] + 0.0 *
      stateEstimator_DW.MemoryX_DSTATE[1]) + 0.0 * stateEstimator_B.Sum[2]);

    /* Product: '<S60>/Product3' */
    stateEstimator_B.Product3[0] = stateEstimator_ConstB.Conversion_n[0] *
      stateEstimator_B.Add1;
    stateEstimator_B.Product3[1] = stateEstimator_ConstB.Conversion_n[1] *
      stateEstimator_B.Add1;
  } else {
    if (stateEstimator_DW.MeasurementUpdate_MODE) {
      /* Disable for Outport: '<S60>/L*(y[k]-yhat[k|k-1])' */
      stateEstimator_B.Product3[0] = 0.0;
      stateEstimator_B.Product3[1] = 0.0;
      stateEstimator_DW.MeasurementUpdate_MODE = false;
    }
  }

  /* End of Outputs for SubSystem: '<S35>/MeasurementUpdate' */

  /* BusCreator: '<Root>/BusConversion_InsertedFor_states_estimout_at_inport_0' incorporates:
   *  DataTypeConversion: '<S3>/Data Type Conversion'
   *  Reshape: '<S14>/Reshapexhat'
   *  Reshape: '<S71>/Reshapexhat'
   */
  rty_states_estimout->X = stateEstimator_B.SimplyIntegrateVelocity[0];
  rty_states_estimout->Y = stateEstimator_B.SimplyIntegrateVelocity[1];
  rty_states_estimout->Z = (real32_T)stateEstimator_B.Add[0];
  rty_states_estimout->yaw = stateEstimator_DW.Memory_PreviousInput[0];
  rty_states_estimout->pitch = stateEstimator_DW.Memory_PreviousInput[1];
  rty_states_estimout->roll = stateEstimator_DW.Memory_PreviousInput[2];
  rty_states_estimout->dx = stateEstimator_B.UseCurrentEstimator_l.Add[0];
  rty_states_estimout->dy = stateEstimator_B.UseCurrentEstimator_l.Add[1];

  /* Product: '<S3>/Product1' incorporates:
   *  DataTypeConversion: '<S3>/Data Type Conversion1'
   *  DataTypeConversion: '<S3>/Data Type Conversion2'
   *  Delay: '<S1>/Delay1'
   *  Reshape: '<S14>/Reshapexhat'
   *  SignalConversion: '<S3>/TmpSignal ConversionAtProduct1Inport2'
   */
  for (j = 0; j < 3; j++) {
    stateEstimator_B.rtb_FIR_IMUaccel_c[j] =
      stateEstimator_B.Reshape9to3x3columnmajor[j + 6] * stateEstimator_B.Add[1]
      + ((real_T)stateEstimator_B.Reshape9to3x3columnmajor[j + 3] *
         stateEstimator_DW.Delay1_DSTATE[1] + (real_T)
         stateEstimator_B.Reshape9to3x3columnmajor[j] *
         stateEstimator_DW.Delay1_DSTATE[0]);
  }

  /* BusCreator: '<Root>/BusConversion_InsertedFor_states_estimout_at_inport_0' incorporates:
   *  DataTypeConversion: '<S3>/Data Type Conversion2'
   *  DiscreteFilter: '<S5>/IIR_IMUgyro_r'
   *  SignalConversion: '<S2>/TmpSignal ConversionAtProductInport2'
   */
  rty_states_estimout->dz = (real32_T)stateEstimator_B.rtb_FIR_IMUaccel_c[2];
  rty_states_estimout->p = stateEstimator_B.inverseIMU_gain[3];
  rty_states_estimout->q = stateEstimator_B.inverseIMU_gain[4];
  rty_states_estimout->r = rtb_DataTypeConversion1_o_idx_0_tmp;

  /* Reshape: '<S14>/Reshapeyhat' incorporates:
   *  Constant: '<S14>/C'
   *  Constant: '<S14>/D'
   *  Product: '<S19>/Product'
   *  Product: '<S19>/Product1'
   *  Reshape: '<S14>/Reshapeu'
   *  Sum: '<S19>/Add1'
   */
  stateEstimator_B.Reshapeyhat = (1.0 * stateEstimator_B.Add[0] + 0.0 *
    stateEstimator_B.Add[1]) + 0.0 * stateEstimator_B.Sum[2];

  /* ManualSwitch: '<S14>/ManualSwitchPZ' incorporates:
   *  Constant: '<S14>/P0'
   */
  if (((uint8_T)1U) == 1) {
    stateEstimator_B.ManualSwitchPZ[0] = stateEstimator_ConstB.Conversion_c[0];
    stateEstimator_B.ManualSwitchPZ[1] = stateEstimator_ConstB.Conversion_c[1];
    stateEstimator_B.ManualSwitchPZ[2] = stateEstimator_ConstB.Conversion_c[2];
    stateEstimator_B.ManualSwitchPZ[3] = stateEstimator_ConstB.Conversion_c[3];
  } else {
    stateEstimator_B.ManualSwitchPZ[0] = 0.0026948589925819932;
    stateEstimator_B.ManualSwitchPZ[1] = 0.0071657120718248571;
    stateEstimator_B.ManualSwitchPZ[2] = 0.0071657120718248571;
    stateEstimator_B.ManualSwitchPZ[3] = 0.038107692935056586;
  }

  /* End of ManualSwitch: '<S14>/ManualSwitchPZ' */

  /* ManualSwitch: '<S71>/ManualSwitchPZ' incorporates:
   *  Constant: '<S71>/P0'
   */
  if (((uint8_T)1U) == 1) {
    stateEstimator_B.ManualSwitchPZ_m[0] = stateEstimator_ConstB.Conversion_i[0];
    stateEstimator_B.ManualSwitchPZ_m[1] = stateEstimator_ConstB.Conversion_i[1];
    stateEstimator_B.ManualSwitchPZ_m[2] = stateEstimator_ConstB.Conversion_i[2];
    stateEstimator_B.ManualSwitchPZ_m[3] = stateEstimator_ConstB.Conversion_i[3];
  } else {
    stateEstimator_B.ManualSwitchPZ_m[0] = 0.717328072F;
    stateEstimator_B.ManualSwitchPZ_m[1] = 0.0F;
    stateEstimator_B.ManualSwitchPZ_m[2] = 0.0F;
    stateEstimator_B.ManualSwitchPZ_m[3] = 0.717328072F;
  }

  /* End of ManualSwitch: '<S71>/ManualSwitchPZ' */

  /* ManualSwitch: '<S133>/ManualSwitchPZ' incorporates:
   *  Constant: '<S133>/P0'
   */
  if (((uint8_T)1U) == 1) {
    stateEstimator_B.ManualSwitchPZ_p[0] = stateEstimator_ConstB.Conversion_ao[0];
    stateEstimator_B.ManualSwitchPZ_p[1] = stateEstimator_ConstB.Conversion_ao[1];
    stateEstimator_B.ManualSwitchPZ_p[2] = stateEstimator_ConstB.Conversion_ao[2];
    stateEstimator_B.ManualSwitchPZ_p[3] = stateEstimator_ConstB.Conversion_ao[3];
  } else {
    stateEstimator_B.ManualSwitchPZ_p[0] = 0.00173705805F;
    stateEstimator_B.ManualSwitchPZ_p[1] = 0.0F;
    stateEstimator_B.ManualSwitchPZ_p[2] = 0.0F;
    stateEstimator_B.ManualSwitchPZ_p[3] = 0.00173705805F;
  }

  /* End of ManualSwitch: '<S133>/ManualSwitchPZ' */

  /* Update for DiscreteFilter: '<S5>/IIR_IMUgyro_r' */
  stateEstimator_DW.IIR_IMUgyro_r_states[4] =
    stateEstimator_DW.IIR_IMUgyro_r_states[3];
  stateEstimator_DW.IIR_IMUgyro_r_states[3] =
    stateEstimator_DW.IIR_IMUgyro_r_states[2];
  stateEstimator_DW.IIR_IMUgyro_r_states[2] =
    stateEstimator_DW.IIR_IMUgyro_r_states[1];
  stateEstimator_DW.IIR_IMUgyro_r_states[1] =
    stateEstimator_DW.IIR_IMUgyro_r_states[0];
  stateEstimator_DW.IIR_IMUgyro_r_states[0] =
    stateEstimator_DW.IIR_IMUgyro_r_tmp;

  /* Update for DiscreteFir: '<S5>/FIR_IMUaccel' */
  /* Update circular buffer index */
  stateEstimator_DW.FIR_IMUaccel_circBuf--;
  if (stateEstimator_DW.FIR_IMUaccel_circBuf < 0) {
    stateEstimator_DW.FIR_IMUaccel_circBuf = 4;
  }

  /* Update circular buffer */
  stateEstimator_DW.FIR_IMUaccel_states[stateEstimator_DW.FIR_IMUaccel_circBuf] =
    stateEstimator_B.inverseIMU_gain[0];
  stateEstimator_DW.FIR_IMUaccel_states[stateEstimator_DW.FIR_IMUaccel_circBuf +
    5] = stateEstimator_B.inverseIMU_gain[1];
  stateEstimator_DW.FIR_IMUaccel_states[stateEstimator_DW.FIR_IMUaccel_circBuf +
    10] = stateEstimator_B.inverseIMU_gain[2];

  /* End of Update for DiscreteFir: '<S5>/FIR_IMUaccel' */

  /* Update for Delay: '<S3>/Delay2' incorporates:
   *  Reshape: '<S14>/Reshapexhat'
   */
  stateEstimator_DW.Delay2_DSTATE = stateEstimator_B.Add[0];

  /* Update for DiscreteFilter: '<S15>/pressureFilter_IIR' */
  stateEstimator_DW.pressureFilter_IIR_states[2] =
    stateEstimator_DW.pressureFilter_IIR_states[1];
  stateEstimator_DW.pressureFilter_IIR_states[1] =
    stateEstimator_DW.pressureFilter_IIR_states[0];
  stateEstimator_DW.pressureFilter_IIR_states[0] =
    stateEstimator_DW.pressureFilter_IIR_tmp;

  /* Update for DiscreteFilter: '<S15>/soonarFilter_IIR' */
  stateEstimator_DW.soonarFilter_IIR_states[2] =
    stateEstimator_DW.soonarFilter_IIR_states[1];
  stateEstimator_DW.soonarFilter_IIR_states[1] =
    stateEstimator_DW.soonarFilter_IIR_states[0];
  stateEstimator_DW.soonarFilter_IIR_states[0] =
    stateEstimator_DW.soonarFilter_IIR_tmp;

  /* Update for Delay: '<S14>/MemoryX' */
  stateEstimator_DW.icLoad = 0U;

  /* Product: '<S35>/A[k]*xhat[k|k-1]' incorporates:
   *  Constant: '<S14>/A'
   *  Delay: '<S14>/MemoryX'
   *  Sum: '<S35>/Add'
   */
  stateEstimator_B.Add1 = 0.0 * stateEstimator_DW.MemoryX_DSTATE[0] + 1.0 *
    stateEstimator_DW.MemoryX_DSTATE[1];

  /* Update for Delay: '<S14>/MemoryX' incorporates:
   *  Constant: '<S14>/A'
   *  Constant: '<S14>/B'
   *  Product: '<S35>/A[k]*xhat[k|k-1]'
   *  Product: '<S35>/B[k]*u[k]'
   *  Reshape: '<S14>/Reshapeu'
   *  Sum: '<S35>/Add'
   */
  stateEstimator_DW.MemoryX_DSTATE[0] = ((1.0 *
    stateEstimator_DW.MemoryX_DSTATE[0] + 0.005 *
    stateEstimator_DW.MemoryX_DSTATE[1]) + 0.0 * stateEstimator_B.Sum[2]) +
    stateEstimator_B.Product3[0];
  stateEstimator_DW.MemoryX_DSTATE[1] = (0.005 * stateEstimator_B.Sum[2] +
    stateEstimator_B.Add1) + stateEstimator_B.Product3[1];

  /* Update for Delay: '<S1>/Delay1' incorporates:
   *  Reshape: '<S71>/Reshapexhat'
   */
  stateEstimator_DW.Delay1_DSTATE[0] =
    stateEstimator_B.UseCurrentEstimator_l.Add[0];
  stateEstimator_DW.Delay1_DSTATE[1] =
    stateEstimator_B.UseCurrentEstimator_l.Add[1];
  for (j = 0; j < 2; j++) {
    /* Update for DiscreteFilter: '<S70>/IIRgyroz' */
    cff = j * 5;
    stateEstimator_DW.IIRgyroz_states[cff + 4] =
      stateEstimator_DW.IIRgyroz_states[cff + 3];
    stateEstimator_DW.IIRgyroz_states[cff + 3] =
      stateEstimator_DW.IIRgyroz_states[cff + 2];
    stateEstimator_DW.IIRgyroz_states[cff + 2] =
      stateEstimator_DW.IIRgyroz_states[cff + 1];
    stateEstimator_DW.IIRgyroz_states[cff + 1] =
      stateEstimator_DW.IIRgyroz_states[cff];
    stateEstimator_DW.IIRgyroz_states[cff] = stateEstimator_DW.IIRgyroz_tmp[j];

    /* Update for UnitDelay: '<S77>/UD' */
    stateEstimator_DW.UD_DSTATE[j] = stateEstimator_B.TSamp[j];
  }

  /* Update for Delay: '<S67>/Delay' incorporates:
   *  Reshape: '<S71>/Reshapexhat'
   */
  stateEstimator_DW.Delay_DSTATE[0] =
    stateEstimator_B.UseCurrentEstimator_l.Add[0];
  stateEstimator_DW.Delay_DSTATE[1] =
    stateEstimator_B.UseCurrentEstimator_l.Add[1];

  /* Update for Delay: '<S71>/MemoryX' incorporates:
   *  Constant: '<S71>/A'
   *  Constant: '<S71>/B'
   *  Product: '<S106>/A[k]*xhat[k|k-1]'
   *  Product: '<S106>/B[k]*u[k]'
   *  Sum: '<S106>/Add'
   */
  stateEstimator_DW.icLoad_i = 0U;
  stateEstimator_DW.MemoryX_DSTATE_d[0] = ((0.005F *
    stateEstimator_B.Reshapeu_h[0] + 0.0F * stateEstimator_B.Reshapeu_h[1]) +
    (1.0F * stateEstimator_B.Conversion_j[0] + 0.0F *
     stateEstimator_B.Conversion_j[1])) +
    stateEstimator_B.MeasurementUpdate_e.Product3[0];
  stateEstimator_DW.MemoryX_DSTATE_d[1] = ((0.0F * stateEstimator_B.Reshapeu_h[0]
    + 0.005F * stateEstimator_B.Reshapeu_h[1]) + (0.0F *
    stateEstimator_B.Conversion_j[0] + 1.0F * stateEstimator_B.Conversion_j[1]))
    + stateEstimator_B.MeasurementUpdate_e.Product3[1];

  /* Update for Delay: '<S68>/Delay' incorporates:
   *  Reshape: '<S133>/Reshapexhat'
   */
  stateEstimator_DW.Delay_DSTATE_l[0] =
    stateEstimator_B.UseCurrentEstimator_j.Add[0];
  stateEstimator_DW.Delay_DSTATE_l[1] =
    stateEstimator_B.UseCurrentEstimator_j.Add[1];

  /* Update for Delay: '<S133>/MemoryX' incorporates:
   *  Constant: '<S133>/A'
   *  Constant: '<S133>/B'
   *  Product: '<S153>/A[k]*xhat[k|k-1]'
   *  Product: '<S153>/B[k]*u[k]'
   *  Sum: '<S153>/Add'
   */
  stateEstimator_DW.icLoad_g = 0U;
  stateEstimator_DW.MemoryX_DSTATE_dl[0] = ((0.005F *
    stateEstimator_B.Reshapeu_i[0] + 0.0F * stateEstimator_B.Reshapeu_i[1]) +
    (1.0F * stateEstimator_B.Conversion_p[0] + 0.0F *
     stateEstimator_B.Conversion_p[1])) +
    stateEstimator_B.MeasurementUpdate_b.Product3[0];
  stateEstimator_DW.MemoryX_DSTATE_dl[1] = ((0.0F * stateEstimator_B.Reshapeu_i
    [0] + 0.005F * stateEstimator_B.Reshapeu_i[1]) + (0.0F *
    stateEstimator_B.Conversion_p[0] + 1.0F * stateEstimator_B.Conversion_p[1]))
    + stateEstimator_B.MeasurementUpdate_b.Product3[1];

  /* Update for DiscreteIntegrator: '<S68>/SimplyIntegrateVelocity' */
  stateEstimator_DW.SimplyIntegrateVelocity_DSTATE[0] += 0.005F *
    stateEstimator_B.Product_l[0];
  stateEstimator_DW.SimplyIntegrateVelocity_DSTATE[1] += 0.005F *
    stateEstimator_B.Product_l[1];
  stateEstimator_DW.SimplyIntegrateVelocity_PrevResetState = (int8_T)
    *rtu_controlModePosVSOrient_flagin;
}

/* Model initialize function */
void stateEstimator_initialize(const char_T **rt_errorStatus)
{
  RT_MODEL_stateEstimator_T *const stateEstimator_M =
    &(stateEstimator_MdlrefDW.rtm);

  /* Registration code */

  /* initialize non-finites */
  rt_InitInfAndNaN(sizeof(real_T));

  /* initialize error status */
  rtmSetErrorStatusPointer(stateEstimator_M, rt_errorStatus);

  /* block I/O */
  (void) memset(((void *) &stateEstimator_B), 0,
                sizeof(B_stateEstimator_c_T));

  /* states (dwork) */
  (void) memset((void *)&stateEstimator_DW, 0,
                sizeof(DW_stateEstimator_f_T));
}
