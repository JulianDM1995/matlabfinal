/*
 * C:\Users\Julian\Desktop\MATLAB FINAL\asbQuadcopterCompetition_Unicauca\work\slprj\grt\_sharedutils\sqrt_GxpnL5DU.h
 *
 * Sponsored License - for use in support of a program or activity
 * sponsored by MathWorks.  Not for government, commercial or other
 * non-sponsored organizational use.
 *
 * Code generation for model "flightControlSystem".
 *
 * Model version              : 1.860
 * Simulink Coder version : 8.14 (R2018a) 06-Feb-2018
 * C source code generated on : Wed Oct 17 00:32:42 2018
 * Created for block: flightControlSystem
 */

#ifndef SHARE_sqrt_GxpnL5DU
#define SHARE_sqrt_GxpnL5DU
#include "rtwtypes.h"

extern void sqrt_GxpnL5DU(real_T x_data[], int32_T *x_size);

#endif
