/*
 * C:\Users\Julian\Desktop\MATLAB FINAL\asbQuadcopterCompetition_Unicauca\work\slprj\grt\_sharedutils\any_JducOVS3.h
 *
 * Sponsored License - for use in support of a program or activity
 * sponsored by MathWorks.  Not for government, commercial or other
 * non-sponsored organizational use.
 *
 * Code generation for model "flightControlSystem".
 *
 * Model version              : 1.860
 * Simulink Coder version : 8.14 (R2018a) 06-Feb-2018
 * C source code generated on : Wed Oct 17 00:32:42 2018
 * Created for block: flightControlSystem
 */

#ifndef SHARE_any_JducOVS3
#define SHARE_any_JducOVS3
#include "rtwtypes.h"

extern boolean_T any_JducOVS3(const real_T x[200]);

#endif
