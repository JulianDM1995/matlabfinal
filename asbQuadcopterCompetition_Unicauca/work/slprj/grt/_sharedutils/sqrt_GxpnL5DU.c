/*
 * C:\Users\Julian\Desktop\MATLAB FINAL\asbQuadcopterCompetition_Unicauca\work\slprj\grt\_sharedutils\sqrt_GxpnL5DU.c
 *
 * Sponsored License - for use in support of a program or activity
 * sponsored by MathWorks.  Not for government, commercial or other
 * non-sponsored organizational use.
 *
 * Code generation for model "flightControlSystem".
 *
 * Model version              : 1.860
 * Simulink Coder version : 8.14 (R2018a) 06-Feb-2018
 * C source code generated on : Wed Oct 17 00:32:42 2018
 * Created for block: flightControlSystem
 */

#include "rtwtypes.h"
#include <math.h>
#include "sqrt_GxpnL5DU.h"

/* Function for MATLAB Function: '<S2>/MATLAB Function2' */
void sqrt_GxpnL5DU(real_T x_data[], int32_T *x_size)
{
  int32_T k;
  for (k = 0; k < *x_size; k++) {
    x_data[k] = sqrt(x_data[k]);
  }
}
