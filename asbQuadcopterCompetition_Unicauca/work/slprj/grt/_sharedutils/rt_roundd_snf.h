/*
 * C:\Users\Julian\Desktop\MATLAB FINAL\asbQuadcopterCompetition_Unicauca\work\slprj\grt\_sharedutils\rt_roundd_snf.h
 *
 * Sponsored License - for use in support of a program or activity
 * sponsored by MathWorks.  Not for government, commercial or other
 * non-sponsored organizational use.
 *
 * Code generation for model "flightControlSystem".
 *
 * Model version              : 1.860
 * Simulink Coder version : 8.14 (R2018a) 06-Feb-2018
 * C source code generated on : Wed Oct 17 00:32:42 2018
 * Created for block: flightControlSystem
 */

#ifndef SHARE_rt_roundd_snf
#define SHARE_rt_roundd_snf
#include "rtwtypes.h"

extern real_T rt_roundd_snf(real_T u);

#endif
