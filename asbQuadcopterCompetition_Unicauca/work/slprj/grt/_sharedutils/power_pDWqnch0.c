/*
 * C:\Users\Julian\Desktop\MATLAB FINAL\asbQuadcopterCompetition_Unicauca\work\slprj\grt\_sharedutils\power_pDWqnch0.c
 *
 * Sponsored License - for use in support of a program or activity
 * sponsored by MathWorks.  Not for government, commercial or other
 * non-sponsored organizational use.
 *
 * Code generation for model "flightControlSystem".
 *
 * Model version              : 1.860
 * Simulink Coder version : 8.14 (R2018a) 06-Feb-2018
 * C source code generated on : Wed Oct 17 00:32:42 2018
 * Created for block: flightControlSystem
 */

#include "rtwtypes.h"
#include "power_pDWqnch0.h"

/* Function for MATLAB Function: '<S2>/MATLAB Function1' */
void power_pDWqnch0(const real_T a[256], real_T y[256])
{
  int32_T k;
  for (k = 0; k < 256; k++) {
    y[k] = a[k] * a[k];
  }
}
