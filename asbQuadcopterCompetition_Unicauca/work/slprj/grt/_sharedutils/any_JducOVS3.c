/*
 * C:\Users\Julian\Desktop\MATLAB FINAL\asbQuadcopterCompetition_Unicauca\work\slprj\grt\_sharedutils\any_JducOVS3.c
 *
 * Sponsored License - for use in support of a program or activity
 * sponsored by MathWorks.  Not for government, commercial or other
 * non-sponsored organizational use.
 *
 * Code generation for model "flightControlSystem".
 *
 * Model version              : 1.860
 * Simulink Coder version : 8.14 (R2018a) 06-Feb-2018
 * C source code generated on : Wed Oct 17 00:32:42 2018
 * Created for block: flightControlSystem
 */

#include "rtwtypes.h"
#include "rt_nonfinite.h"
#include "any_JducOVS3.h"

/* Function for MATLAB Function: '<S2>/MATLAB Function2' */
boolean_T any_JducOVS3(const real_T x[200])
{
  boolean_T y;
  int32_T k;
  boolean_T exitg1;
  y = false;
  k = 0;
  exitg1 = false;
  while ((!exitg1) && (k < 200)) {
    if ((x[k] == 0.0) || rtIsNaN(x[k])) {
      k++;
    } else {
      y = true;
      exitg1 = true;
    }
  }

  return y;
}
