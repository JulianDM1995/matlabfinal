/*
 * flightControlSystem.h
 *
 * Sponsored License - for use in support of a program or activity
 * sponsored by MathWorks.  Not for government, commercial or other
 * non-sponsored organizational use.
 *
 * Code generation for model "flightControlSystem".
 *
 * Model version              : 1.861
 * Simulink Coder version : 8.14 (R2018a) 06-Feb-2018
 * C source code generated on : Wed Oct 17 09:55:45 2018
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: ARM Compatible->ARM 9
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_flightControlSystem_h_
#define RTW_HEADER_flightControlSystem_h_
#include <stddef.h>
#include <math.h>
#include <string.h>
#include <float.h>
#ifndef flightControlSystem_COMMON_INCLUDES_
# define flightControlSystem_COMMON_INCLUDES_
#include "rtwtypes.h"
#include "rtw_continuous.h"
#include "rtw_solver.h"
#include "rt_logging.h"
#endif                                 /* flightControlSystem_COMMON_INCLUDES_ */

#include "flightControlSystem_types.h"

/* Shared type includes */
#include "multiword_types.h"

/* Child system includes */
#define stateEstimator_MDLREF_HIDE_CHILD_
#include "stateEstimator.h"
#include "imageConversion.h"
#define flightController_MDLREF_HIDE_CHILD_
#include "flightController.h"
#include "rt_nonfinite.h"
#include "rtGetInf.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetFinalTime
# define rtmGetFinalTime(rtm)          ((rtm)->Timing.tFinal)
#endif

#ifndef rtmGetRTWLogInfo
# define rtmGetRTWLogInfo(rtm)         ((rtm)->rtwLogInfo)
#endif

#ifndef rtmCounterLimit
# define rtmCounterLimit(rtm, idx)     ((rtm)->Timing.TaskCounters.cLimit[(idx)])
#endif

#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

#ifndef rtmGetErrorStatusPointer
# define rtmGetErrorStatusPointer(rtm) ((const char_T **)(&((rtm)->errorStatus)))
#endif

#ifndef rtmStepTask
# define rtmStepTask(rtm, idx)         ((rtm)->Timing.TaskCounters.TID[(idx)] == 0)
#endif

#ifndef rtmGetStopRequested
# define rtmGetStopRequested(rtm)      ((rtm)->Timing.stopRequestedFlag)
#endif

#ifndef rtmSetStopRequested
# define rtmSetStopRequested(rtm, val) ((rtm)->Timing.stopRequestedFlag = (val))
#endif

#ifndef rtmGetStopRequestedPtr
# define rtmGetStopRequestedPtr(rtm)   (&((rtm)->Timing.stopRequestedFlag))
#endif

#ifndef rtmGetT
# define rtmGetT(rtm)                  ((rtm)->Timing.taskTime0)
#endif

#ifndef rtmGetTFinal
# define rtmGetTFinal(rtm)             ((rtm)->Timing.tFinal)
#endif

#ifndef rtmGetTPtr
# define rtmGetTPtr(rtm)               (&(rtm)->Timing.taskTime0)
#endif

#ifndef rtmTaskCounter
# define rtmTaskCounter(rtm, idx)      ((rtm)->Timing.TaskCounters.TID[(idx)])
#endif

/* Block signals for system '<S1>/Logging' */
typedef struct {
  real_T DataTypeConversion4[12];      /* '<S4>/Data Type Conversion4' */
  real_T DataTypeConversion12[8];      /* '<S4>/Data Type Conversion12' */
  real_T DataTypeConversion1_i[8];     /* '<S4>/Data Type Conversion1' */
  real_T DataTypeConversion5[4];       /* '<S4>/Data Type Conversion5' */
  real_T DataTypeConversion2[4];       /* '<S4>/Data Type Conversion2' */
  real_T DataTypeConversion6[3];       /* '<S4>/Data Type Conversion6' */
  real_T DataTypeConversion8[3];       /* '<S4>/Data Type Conversion8' */
  real_T DataTypeConversion10[3];      /* '<S4>/Data Type Conversion10' */
  real_T DataTypeConversion3;          /* '<S4>/Data Type Conversion3' */
  real_T DataTypeConversion7;          /* '<S4>/Data Type Conversion7' */
  real_T DataTypeConversion9;          /* '<S4>/Data Type Conversion9' */
  real_T DataTypeConversion11;         /* '<S4>/Data Type Conversion11' */
  real_T DataTypeConversion_e;         /* '<S4>/Data Type Conversion' */
  real_T DataTypeConversion13;         /* '<S4>/Data Type Conversion13' */
  real_T DataTypeConversion14;         /* '<S4>/Data Type Conversion14' */
} B_Logging_flightControlSystem_T;

/* Block states (default storage) for system '<S1>/Logging' */
typedef struct {
  struct {
    void *LoggedData;
  } ToWorkspace2_PWORK;                /* '<S4>/To Workspace2' */

  struct {
    void *LoggedData;
  } ToWorkspace1_PWORK;                /* '<S4>/To Workspace1' */

  struct {
    void *LoggedData;
  } ToWorkspace_PWORK;                 /* '<S4>/To Workspace' */
} DW_Logging_flightControlSystem_T;

/* Block signals for system '<Root>/Flight Control System' */
typedef struct {
  statesEstim_t estimator;             /* '<S1>/estimator' */
  sensordata_t BusConversion_InsertedFor_estimator_at_inport_1_BusCreator1;
  real32_T controller_o2[8];           /* '<S1>/controller' */
  CommandBus ControlModeUpdate;        /* '<S1>/Control Mode Update' */
  CommandBus BusAssignment;            /* '<S5>/Bus  Assignment' */
  real_T vXY;
  real_T dT;
  real_T dZ;
  real_T rtb_dE_idx_0;
  real_T rtb_dE_idx_1;
  real32_T X__tmp;
  B_Logging_flightControlSystem_T Logging;/* '<S1>/Logging' */
} B_FlightControlSystem_flightControlSystem_T;

/* Block states (default storage) for system '<Root>/Flight Control System' */
typedef struct {
  real_T X_;                           /* '<S5>/MATLAB Function' */
  real_T Y_;                           /* '<S5>/MATLAB Function' */
  real_T Z_;                           /* '<S5>/MATLAB Function' */
  real_T T_;                           /* '<S5>/MATLAB Function' */
  DW_Logging_flightControlSystem_T Logging;/* '<S1>/Logging' */
} DW_FlightControlSystem_flightControlSystem_T;

/* Block signals (default storage) */
typedef struct {
  uint8_T ColorSpaceConversion_o1[19200];/* '<S2>/Color Space  Conversion' */
  uint8_T ColorSpaceConversion_o2[19200];/* '<S2>/Color Space  Conversion' */
  uint8_T ColorSpaceConversion_o3[19200];/* '<S2>/Color Space  Conversion' */
  uint8_T ImageConversionfromY1UY2VtoYUV_o2[19200];/* '<S2>/Image Conversion from Y1UY2V to YUV' */
  uint8_T ImageConversionfromY1UY2VtoYUV_o3[19200];/* '<S2>/Image Conversion from Y1UY2V to YUV' */
  uint8_T ImageConversionfromY1UY2VtoYUV_o1[19200];/* '<S2>/Image Conversion from Y1UY2V to YUV' */
  uint8_T B[19200];                    /* '<S2>/MATLAB Function' */
  uint8_T G[19200];                    /* '<S2>/MATLAB Function' */
  uint8_T R[19200];                    /* '<S2>/MATLAB Function' */
  boolean_T Al[19200];                 /* '<S2>/MATLAB Function3' */
  real_T dv0[256];
  real_T EM_data[200];
  real_T ET_data[200];
  real_T tmp_data[200];
  real_T tmp_data_m[200];
  real_T z1_data[200];
  real_T AVef[200];
  real_T BVef[200];
  real_T CVef[200];
  real_T DVef[200];
  int16_T ET_[400];
  int16_T ET_data_c[400];
  int32_T ii_data[200];
  boolean_T c_x_data[200];
  real_T A[16];
  real_T dv1[10];
  real_T varargin_1[4];
  real_T A_k[2];
  real_T B_c[2];
  real_T C[2];
  real_T D[2];
  real_T TAB;
  real_T TBC;
  real_T TCD;
  real_T TDA;
  real_T d0;
  real_T d1;
  real_T d2;
  real_T d3;
  real_T d4;
  real_T d5;
  real_T rtb_Sum1_b;
  real_T m;
  real_T Am;
  real_T AM;
  real_T Bm;
  real_T BM;
  real_T Cm;
  real_T CM;
  real_T Dm;
  real_T DM;
  real_T AR;
  real_T BR;
  real_T CR;
  real_T DR;
  real_T AC;
  real_T x;
  real_T PrD_idx_0;
  real_T Bp_idx_1;
  real_T Cp_idx_0;
  real_T Dp_idx_1;
  real_T scale;
  real_T RateTransition;               /* '<Root>/Rate Transition' */
  int32_T i;
  int32_T Y;
  uint32_T rConst;
  uint32_T gConst;
  uint32_T bConst;
  uint32_T cc1;
  uint32_T pos;
  uint32_T neg;
  uint32_T yDiff;
  int16_T Pr[2];
  B_FlightControlSystem_flightControlSystem_T FlightControlSystem;/* '<Root>/Flight Control System' */
} B_flightControlSystem_T;

/* Block states (default storage) for system '<Root>' */
typedef struct {
  volatile real_T RateTransition_Buffer[2];/* '<Root>/Rate Transition' */
  real_T x[256];                       /* '<S2>/MATLAB Function1' */
  real_T ra[256];                      /* '<S2>/MATLAB Function1' */
  real_T gv[256];                      /* '<S2>/MATLAB Function1' */
  real_T bv[256];                      /* '<S2>/MATLAB Function1' */
  volatile int8_T RateTransition_ActiveBufIdx;/* '<Root>/Rate Transition' */
  uint8_T ColorSpaceConversion_DWORK1[19200];/* '<S2>/Color Space  Conversion' */
  boolean_T x_not_empty;               /* '<S2>/MATLAB Function1' */
  MdlrefDW_imageConversion_T ImageConversionfromY1UY2VtoYUV_InstanceData;/* '<S2>/Image Conversion from Y1UY2V to YUV' */
  DW_FlightControlSystem_flightControlSystem_T FlightControlSystem;/* '<Root>/Flight Control System' */
} DW_flightControlSystem_T;

/* External inputs (root inport signals with default storage) */
typedef struct {
  uint8_T Y1UY2V[38400];               /* '<Root>/Image Data' */
} ExtU_flightControlSystem_T;

/* External outputs (root outports fed by signals with default storage) */
typedef struct {
  real32_T Actuators[4];               /* '<Root>/Actuators' */
  uint8_T Flag;                        /* '<Root>/Flag' */
} ExtY_flightControlSystem_T;

/* Real-time Model Data Structure */
struct tag_RTM_flightControlSystem_T {
  const char_T *errorStatus;
  RTWLogInfo *rtwLogInfo;

  /*
   * Timing:
   * The following substructure contains information regarding
   * the timing information for the model.
   */
  struct {
    time_T taskTime0;
    uint32_T clockTick0;
    uint32_T clockTickH0;
    time_T stepSize0;
    struct {
      uint32_T TID[2];
      uint32_T cLimit[2];
    } TaskCounters;

    time_T tFinal;
    boolean_T stopRequestedFlag;
  } Timing;
};

/* Block signals (default storage) */
extern B_flightControlSystem_T flightControlSystem_B;

/* Block states (default storage) */
extern DW_flightControlSystem_T flightControlSystem_DW;

/* External inputs (root inport signals with default storage) */
extern ExtU_flightControlSystem_T flightControlSystem_U;

/* External outputs (root outports fed by signals with default storage) */
extern ExtY_flightControlSystem_T flightControlSystem_Y;

/*
 * Exported Global Signals
 *
 * Note: Exported global signals are block signals with an exported global
 * storage class designation.  Code generation will declare the memory for
 * these signals and export their symbols.
 *
 */
extern CommandBus cmd_inport;          /* '<Root>/AC cmd' */
extern SensorsBus sensor_inport;       /* '<Root>/Sensors' */
extern real32_T motors_outport[4];     /* '<S1>/controller' */
extern uint8_T flag_outport;           /* '<S3>/Merge' */

/* Model entry point functions */
extern void flightControlSystem_initialize(void);
extern void flightControlSystem_step(int_T tid);
extern void flightControlSystem_terminate(void);

/* Real-time Model object */
extern RT_MODEL_flightControlSystem_T *const flightControlSystem_M;

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'flightControlSystem'
 * '<S1>'   : 'flightControlSystem/Flight Control System'
 * '<S2>'   : 'flightControlSystem/Image Processing System'
 * '<S3>'   : 'flightControlSystem/Flight Control System/Crash Predictor Flags'
 * '<S4>'   : 'flightControlSystem/Flight Control System/Logging'
 * '<S5>'   : 'flightControlSystem/Flight Control System/Path Planning'
 * '<S6>'   : 'flightControlSystem/Flight Control System/sensordata_group'
 * '<S7>'   : 'flightControlSystem/Flight Control System/Crash Predictor Flags/Compare To Constant'
 * '<S8>'   : 'flightControlSystem/Flight Control System/Crash Predictor Flags/Compare To Constant1'
 * '<S9>'   : 'flightControlSystem/Flight Control System/Crash Predictor Flags/Compare To Constant2'
 * '<S10>'  : 'flightControlSystem/Flight Control System/Crash Predictor Flags/Compare To Constant3'
 * '<S11>'  : 'flightControlSystem/Flight Control System/Crash Predictor Flags/Compare To Constant4'
 * '<S12>'  : 'flightControlSystem/Flight Control System/Crash Predictor Flags/Compare To Constant5'
 * '<S13>'  : 'flightControlSystem/Flight Control System/Crash Predictor Flags/Geofencing error'
 * '<S14>'  : 'flightControlSystem/Flight Control System/Crash Predictor Flags/Normal condition'
 * '<S15>'  : 'flightControlSystem/Flight Control System/Crash Predictor Flags/estimator//Optical flow error'
 * '<S16>'  : 'flightControlSystem/Flight Control System/Path Planning/MATLAB Function'
 * '<S17>'  : 'flightControlSystem/Flight Control System/Path Planning/MATLAB Function2'
 * '<S18>'  : 'flightControlSystem/Image Processing System/Funcional 1.0'
 * '<S19>'  : 'flightControlSystem/Image Processing System/MATLAB Function'
 * '<S20>'  : 'flightControlSystem/Image Processing System/MATLAB Function1'
 * '<S21>'  : 'flightControlSystem/Image Processing System/MATLAB Function2'
 * '<S22>'  : 'flightControlSystem/Image Processing System/MATLAB Function3'
 * '<S23>'  : 'flightControlSystem/Image Processing System/Funcional 1.0/Layer 1'
 * '<S24>'  : 'flightControlSystem/Image Processing System/Funcional 1.0/Layer 2'
 * '<S25>'  : 'flightControlSystem/Image Processing System/Funcional 1.0/Process Input 1'
 * '<S26>'  : 'flightControlSystem/Image Processing System/Funcional 1.0/Process Output 1'
 * '<S27>'  : 'flightControlSystem/Image Processing System/Funcional 1.0/Layer 1/Delays 1'
 * '<S28>'  : 'flightControlSystem/Image Processing System/Funcional 1.0/Layer 1/IW{1,1}'
 * '<S29>'  : 'flightControlSystem/Image Processing System/Funcional 1.0/Layer 1/tansig'
 * '<S30>'  : 'flightControlSystem/Image Processing System/Funcional 1.0/Layer 1/IW{1,1}/dotprod1'
 * '<S31>'  : 'flightControlSystem/Image Processing System/Funcional 1.0/Layer 1/IW{1,1}/dotprod10'
 * '<S32>'  : 'flightControlSystem/Image Processing System/Funcional 1.0/Layer 1/IW{1,1}/dotprod2'
 * '<S33>'  : 'flightControlSystem/Image Processing System/Funcional 1.0/Layer 1/IW{1,1}/dotprod3'
 * '<S34>'  : 'flightControlSystem/Image Processing System/Funcional 1.0/Layer 1/IW{1,1}/dotprod4'
 * '<S35>'  : 'flightControlSystem/Image Processing System/Funcional 1.0/Layer 1/IW{1,1}/dotprod5'
 * '<S36>'  : 'flightControlSystem/Image Processing System/Funcional 1.0/Layer 1/IW{1,1}/dotprod6'
 * '<S37>'  : 'flightControlSystem/Image Processing System/Funcional 1.0/Layer 1/IW{1,1}/dotprod7'
 * '<S38>'  : 'flightControlSystem/Image Processing System/Funcional 1.0/Layer 1/IW{1,1}/dotprod8'
 * '<S39>'  : 'flightControlSystem/Image Processing System/Funcional 1.0/Layer 1/IW{1,1}/dotprod9'
 * '<S40>'  : 'flightControlSystem/Image Processing System/Funcional 1.0/Layer 2/Delays 1'
 * '<S41>'  : 'flightControlSystem/Image Processing System/Funcional 1.0/Layer 2/LW{2,1}'
 * '<S42>'  : 'flightControlSystem/Image Processing System/Funcional 1.0/Layer 2/purelin'
 * '<S43>'  : 'flightControlSystem/Image Processing System/Funcional 1.0/Layer 2/LW{2,1}/dotprod1'
 * '<S44>'  : 'flightControlSystem/Image Processing System/Funcional 1.0/Process Input 1/mapminmax'
 * '<S45>'  : 'flightControlSystem/Image Processing System/Funcional 1.0/Process Output 1/mapminmax_reverse'
 */
#endif                                 /* RTW_HEADER_flightControlSystem_h_ */
