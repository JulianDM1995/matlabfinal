/*
 * imageConversion_types.h
 *
 * Sponsored License - for use in support of a program or activity
 * sponsored by MathWorks.  Not for government, commercial or other
 * non-sponsored organizational use.
 *
 * Code generation for model "imageConversion".
 *
 * Model version              : 1.245
 * Simulink Coder version : 8.14 (R2018a) 06-Feb-2018
 * C source code generated on : Wed Oct 17 00:27:56 2018
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: ARM Compatible->ARM 9
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_imageConversion_types_h_
#define RTW_HEADER_imageConversion_types_h_

/* Forward declaration for rtModel */
typedef struct tag_RTM_imageConversion_T RT_MODEL_imageConversion_T;

#endif                                 /* RTW_HEADER_imageConversion_types_h_ */
