/*
 * Code generation for system model 'imageConversion'
 * For more details, see corresponding source file imageConversion.c
 *
 */

#ifndef RTW_HEADER_imageConversion_h_
#define RTW_HEADER_imageConversion_h_
#include <string.h>
#ifndef imageConversion_COMMON_INCLUDES_
# define imageConversion_COMMON_INCLUDES_
#include "rtwtypes.h"
#include "rtw_continuous.h"
#include "rtw_solver.h"
#endif                                 /* imageConversion_COMMON_INCLUDES_ */

#include "imageConversion_types.h"

/* Shared type includes */
#include "multiword_types.h"

/* Block signals for model 'imageConversion' */
typedef struct {
  uint8_T Reshape[19200];              /* '<Root>/Reshape' */
} B_imageConversion_c_T;

/* Real-time Model Data Structure */
struct tag_RTM_imageConversion_T {
  const char_T **errorStatus;
};

typedef struct {
  B_imageConversion_c_T rtb;
  RT_MODEL_imageConversion_T rtm;
} MdlrefDW_imageConversion_T;

/* Model reference registration function */
extern void imageConversion_initialize(const char_T **rt_errorStatus,
  RT_MODEL_imageConversion_T *const imageConversion_M);
extern void imageConversion(const uint8_T rtu_ImageData[38400], uint8_T rty_Y
  [19200], uint8_T rty_U[19200], uint8_T rty_V[19200], B_imageConversion_c_T
  *localB);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'imageConversion'
 * '<S1>'   : 'imageConversion/Cb'
 * '<S2>'   : 'imageConversion/Cr'
 * '<S3>'   : 'imageConversion/Y''
 */
#endif                                 /* RTW_HEADER_imageConversion_h_ */
