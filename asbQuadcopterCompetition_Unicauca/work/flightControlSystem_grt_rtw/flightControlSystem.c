/*
 * flightControlSystem.c
 *
 * Sponsored License - for use in support of a program or activity
 * sponsored by MathWorks.  Not for government, commercial or other
 * non-sponsored organizational use.
 *
 * Code generation for model "flightControlSystem".
 *
 * Model version              : 1.861
 * Simulink Coder version : 8.14 (R2018a) 06-Feb-2018
 * C source code generated on : Wed Oct 17 09:55:45 2018
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: ARM Compatible->ARM 9
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "flightControlSystem.h"
#include "flightControlSystem_private.h"
#include "any_JducOVS3.h"
#include "power_pDWqnch0.h"
#include "rt_roundd_snf.h"
#include "sqrt_GxpnL5DU.h"

/* Exported block signals */
CommandBus cmd_inport;                 /* '<Root>/AC cmd' */
SensorsBus sensor_inport;              /* '<Root>/Sensors' */
real32_T motors_outport[4];            /* '<S1>/controller' */
uint8_T flag_outport;                  /* '<S3>/Merge' */

/* Block signals (default storage) */
B_flightControlSystem_T flightControlSystem_B;

/* Block states (default storage) */
DW_flightControlSystem_T flightControlSystem_DW;

/* External inputs (root inport signals with default storage) */
ExtU_flightControlSystem_T flightControlSystem_U;

/* External outputs (root outports fed by signals with default storage) */
ExtY_flightControlSystem_T flightControlSystem_Y;

/* Real-time model */
RT_MODEL_flightControlSystem_T flightControlSystem_M_;
RT_MODEL_flightControlSystem_T *const flightControlSystem_M =
  &flightControlSystem_M_;

/* Forward declaration for local functions */
static void flightControlSystem_power_tHYmGLzj(const real_T a_data[], const
  int32_T *a_size, real_T y_data[], int32_T *y_size);
static void flightControlSystem_calcularPrAfuera_OofAsJLX(const boolean_T L
  [19200], int16_T E[2]);
static real_T flightControlSystem_norm_cPn2UTGY(const real_T x[2]);
static void flightControlSystem_calcularVectores_CEyKXHdf(const boolean_T L
  [19200], const int16_T Pr[2], real_T A[2], real_T B[2], real_T C[2], real_T D
  [2]);

/* Start for atomic system: '<S1>/Logging' */
void flightControlSystem_Logging_Start(RT_MODEL_flightControlSystem_T * const
  flightControlSystem_M, DW_Logging_flightControlSystem_T *localDW)
{
  /* Start for ToWorkspace: '<S4>/To Workspace2' */
  {
    static int_T rt_ToWksWidths[] = { 12 };

    static int_T rt_ToWksNumDimensions[] = { 1 };

    static int_T rt_ToWksDimensions[] = { 12 };

    static boolean_T rt_ToWksIsVarDims[] = { 0 };

    static void *rt_ToWksCurrSigDims[] = { (NULL) };

    static int_T rt_ToWksCurrSigDimsSize[] = { 4 };

    static BuiltInDTypeId rt_ToWksDataTypeIds[] = { SS_DOUBLE };

    static int_T rt_ToWksComplexSignals[] = { 0 };

    static int_T rt_ToWksFrameData[] = { 0 };

    static RTWPreprocessingFcnPtr rt_ToWksLoggingPreprocessingFcnPtrs[] = {
      (NULL)
    };

    static const char_T *rt_ToWksLabels[] = { "" };

    static RTWLogSignalInfo rt_ToWksSignalInfo = {
      1,
      rt_ToWksWidths,
      rt_ToWksNumDimensions,
      rt_ToWksDimensions,
      rt_ToWksIsVarDims,
      rt_ToWksCurrSigDims,
      rt_ToWksCurrSigDimsSize,
      rt_ToWksDataTypeIds,
      rt_ToWksComplexSignals,
      rt_ToWksFrameData,
      rt_ToWksLoggingPreprocessingFcnPtrs,

      { rt_ToWksLabels },
      (NULL),
      (NULL),
      (NULL),

      { (NULL) },

      { (NULL) },
      (NULL),
      (NULL)
    };

    static const char_T rt_ToWksBlockName[] =
      "flightControlSystem/Flight Control System/Logging/To Workspace2";
    localDW->ToWorkspace2_PWORK.LoggedData = rt_CreateStructLogVar(
      flightControlSystem_M->rtwLogInfo,
      0.0,
      rtmGetTFinal(flightControlSystem_M),
      flightControlSystem_M->Timing.stepSize0,
      (&rtmGetErrorStatus(flightControlSystem_M)),
      "estim",
      1,
      0,
      1,
      0.005,
      &rt_ToWksSignalInfo,
      rt_ToWksBlockName);
    if (localDW->ToWorkspace2_PWORK.LoggedData == (NULL))
      return;
  }

  /* Start for ToWorkspace: '<S4>/To Workspace1' */
  {
    static int_T rt_ToWksWidths[] = { 8 };

    static int_T rt_ToWksNumDimensions[] = { 1 };

    static int_T rt_ToWksDimensions[] = { 8 };

    static boolean_T rt_ToWksIsVarDims[] = { 0 };

    static void *rt_ToWksCurrSigDims[] = { (NULL) };

    static int_T rt_ToWksCurrSigDimsSize[] = { 4 };

    static BuiltInDTypeId rt_ToWksDataTypeIds[] = { SS_DOUBLE };

    static int_T rt_ToWksComplexSignals[] = { 0 };

    static int_T rt_ToWksFrameData[] = { 0 };

    static RTWPreprocessingFcnPtr rt_ToWksLoggingPreprocessingFcnPtrs[] = {
      (NULL)
    };

    static const char_T *rt_ToWksLabels[] = { "" };

    static RTWLogSignalInfo rt_ToWksSignalInfo = {
      1,
      rt_ToWksWidths,
      rt_ToWksNumDimensions,
      rt_ToWksDimensions,
      rt_ToWksIsVarDims,
      rt_ToWksCurrSigDims,
      rt_ToWksCurrSigDimsSize,
      rt_ToWksDataTypeIds,
      rt_ToWksComplexSignals,
      rt_ToWksFrameData,
      rt_ToWksLoggingPreprocessingFcnPtrs,

      { rt_ToWksLabels },
      (NULL),
      (NULL),
      (NULL),

      { (NULL) },

      { (NULL) },
      (NULL),
      (NULL)
    };

    static const char_T rt_ToWksBlockName[] =
      "flightControlSystem/Flight Control System/Logging/To Workspace1";
    localDW->ToWorkspace1_PWORK.LoggedData = rt_CreateStructLogVar(
      flightControlSystem_M->rtwLogInfo,
      0.0,
      rtmGetTFinal(flightControlSystem_M),
      flightControlSystem_M->Timing.stepSize0,
      (&rtmGetErrorStatus(flightControlSystem_M)),
      "posref",
      1,
      0,
      1,
      0.005,
      &rt_ToWksSignalInfo,
      rt_ToWksBlockName);
    if (localDW->ToWorkspace1_PWORK.LoggedData == (NULL))
      return;
  }

  /* Start for ToWorkspace: '<S4>/To Workspace' */
  {
    static int_T rt_ToWksWidths[] = { 4 };

    static int_T rt_ToWksNumDimensions[] = { 1 };

    static int_T rt_ToWksDimensions[] = { 4 };

    static boolean_T rt_ToWksIsVarDims[] = { 0 };

    static void *rt_ToWksCurrSigDims[] = { (NULL) };

    static int_T rt_ToWksCurrSigDimsSize[] = { 4 };

    static BuiltInDTypeId rt_ToWksDataTypeIds[] = { SS_DOUBLE };

    static int_T rt_ToWksComplexSignals[] = { 0 };

    static int_T rt_ToWksFrameData[] = { 0 };

    static RTWPreprocessingFcnPtr rt_ToWksLoggingPreprocessingFcnPtrs[] = {
      (NULL)
    };

    static const char_T *rt_ToWksLabels[] = { "" };

    static RTWLogSignalInfo rt_ToWksSignalInfo = {
      1,
      rt_ToWksWidths,
      rt_ToWksNumDimensions,
      rt_ToWksDimensions,
      rt_ToWksIsVarDims,
      rt_ToWksCurrSigDims,
      rt_ToWksCurrSigDimsSize,
      rt_ToWksDataTypeIds,
      rt_ToWksComplexSignals,
      rt_ToWksFrameData,
      rt_ToWksLoggingPreprocessingFcnPtrs,

      { rt_ToWksLabels },
      (NULL),
      (NULL),
      (NULL),

      { (NULL) },

      { (NULL) },
      (NULL),
      (NULL)
    };

    static const char_T rt_ToWksBlockName[] =
      "flightControlSystem/Flight Control System/Logging/To Workspace";
    localDW->ToWorkspace_PWORK.LoggedData = rt_CreateStructLogVar(
      flightControlSystem_M->rtwLogInfo,
      0.0,
      rtmGetTFinal(flightControlSystem_M),
      flightControlSystem_M->Timing.stepSize0,
      (&rtmGetErrorStatus(flightControlSystem_M)),
      "motor",
      1,
      0,
      1,
      0.005,
      &rt_ToWksSignalInfo,
      rt_ToWksBlockName);
    if (localDW->ToWorkspace_PWORK.LoggedData == (NULL))
      return;
  }
}

/* Output and update for atomic system: '<S1>/Logging' */
void flightControlSystem_Logging(RT_MODEL_flightControlSystem_T * const
  flightControlSystem_M, const real32_T rtu_motorCmds[4], const real32_T
  rtu_posRef[8], const statesEstim_t *rtu_states_estim, const CommandBus
  *rtu_ReferenceValueServerBus, const SensorsBus *rtu_Sensors, real32_T
  rtu_sensordata_datin, real32_T rtu_sensordata_datin_d, real32_T
  rtu_sensordata_datin_e, real32_T rtu_sensordata_datin_c, real32_T
  rtu_sensordata_datin_cg, real32_T rtu_sensordata_datin_i, real32_T
  rtu_sensordata_datin_h, real32_T rtu_sensordata_datin_b, real32_T
  rtu_sensordata_datin_dz, uint32_T rtu_sensordata_datin_n,
  B_Logging_flightControlSystem_T *localB, DW_Logging_flightControlSystem_T
  *localDW)
{
  /* local block i/o variables */
  real_T rtb_DataTypeConversion15;
  real_T rtb_DataTypeConversion16;
  real_T rtb_DataTypeConversion17;
  real_T rtb_DataTypeConversion18;
  real_T rtb_DataTypeConversion19;
  real_T rtb_DataTypeConversion20;
  real_T rtb_DataTypeConversion21;
  int32_T i;

  /* DataTypeConversion: '<S4>/Data Type Conversion4' */
  localB->DataTypeConversion4[0] = rtu_states_estim->X;
  localB->DataTypeConversion4[1] = rtu_states_estim->Y;
  localB->DataTypeConversion4[2] = rtu_states_estim->Z;
  localB->DataTypeConversion4[3] = rtu_states_estim->yaw;
  localB->DataTypeConversion4[4] = rtu_states_estim->pitch;
  localB->DataTypeConversion4[5] = rtu_states_estim->roll;
  localB->DataTypeConversion4[6] = rtu_states_estim->dx;
  localB->DataTypeConversion4[7] = rtu_states_estim->dy;
  localB->DataTypeConversion4[8] = rtu_states_estim->dz;
  localB->DataTypeConversion4[9] = rtu_states_estim->p;
  localB->DataTypeConversion4[10] = rtu_states_estim->q;
  localB->DataTypeConversion4[11] = rtu_states_estim->r;

  /* ToWorkspace: '<S4>/To Workspace2' */
  {
    double locTime = flightControlSystem_M->Timing.taskTime0;
    ;
    rt_UpdateStructLogVar((StructLogVar *)localDW->ToWorkspace2_PWORK.LoggedData,
                          &locTime, &localB->DataTypeConversion4[0]);
  }

  /* DataTypeConversion: '<S4>/Data Type Conversion12' */
  for (i = 0; i < 8; i++) {
    localB->DataTypeConversion12[i] = rtu_posRef[i];
  }

  /* End of DataTypeConversion: '<S4>/Data Type Conversion12' */

  /* ToWorkspace: '<S4>/To Workspace1' */
  {
    double locTime = flightControlSystem_M->Timing.taskTime0;
    ;
    rt_UpdateStructLogVar((StructLogVar *)localDW->ToWorkspace1_PWORK.LoggedData,
                          &locTime, &localB->DataTypeConversion12[0]);
  }

  /* DataTypeConversion: '<S4>/Data Type Conversion1' */
  for (i = 0; i < 8; i++) {
    localB->DataTypeConversion1_i[i] = rtu_Sensors->SensorCalibration[i];
  }

  /* End of DataTypeConversion: '<S4>/Data Type Conversion1' */

  /* DataTypeConversion: '<S4>/Data Type Conversion5' */
  localB->DataTypeConversion5[0] = rtu_motorCmds[0];
  localB->DataTypeConversion5[1] = rtu_motorCmds[1];
  localB->DataTypeConversion5[2] = rtu_motorCmds[2];
  localB->DataTypeConversion5[3] = rtu_motorCmds[3];

  /* ToWorkspace: '<S4>/To Workspace' */
  {
    double locTime = flightControlSystem_M->Timing.taskTime0;
    ;
    rt_UpdateStructLogVar((StructLogVar *)localDW->ToWorkspace_PWORK.LoggedData,
                          &locTime, &localB->DataTypeConversion5[0]);
  }

  /* DataTypeConversion: '<S4>/Data Type Conversion2' */
  localB->DataTypeConversion2[0] = rtu_Sensors->VisionSensors.posVIS_data[0];
  localB->DataTypeConversion2[1] = rtu_Sensors->VisionSensors.posVIS_data[1];
  localB->DataTypeConversion2[2] = rtu_Sensors->VisionSensors.posVIS_data[2];
  localB->DataTypeConversion2[3] = rtu_Sensors->VisionSensors.posVIS_data[3];

  /* DataTypeConversion: '<S4>/Data Type Conversion6' */
  localB->DataTypeConversion6[0] = rtu_ReferenceValueServerBus->pos_ref[0];
  localB->DataTypeConversion6[1] = rtu_ReferenceValueServerBus->pos_ref[1];
  localB->DataTypeConversion6[2] = rtu_ReferenceValueServerBus->pos_ref[2];

  /* DataTypeConversion: '<S4>/Data Type Conversion8' */
  localB->DataTypeConversion8[0] = rtu_ReferenceValueServerBus->orient_ref[0];
  localB->DataTypeConversion8[1] = rtu_ReferenceValueServerBus->orient_ref[1];
  localB->DataTypeConversion8[2] = rtu_ReferenceValueServerBus->orient_ref[2];

  /* DataTypeConversion: '<S4>/Data Type Conversion10' */
  localB->DataTypeConversion10[0] = rtu_Sensors->VisionSensors.opticalFlow_data
    [0];
  localB->DataTypeConversion10[1] = rtu_Sensors->VisionSensors.opticalFlow_data
    [1];
  localB->DataTypeConversion10[2] = rtu_Sensors->VisionSensors.opticalFlow_data
    [2];

  /* DataTypeConversion: '<S4>/Data Type Conversion3' */
  localB->DataTypeConversion3 =
    rtu_ReferenceValueServerBus->controlModePosVSOrient;

  /* DataTypeConversion: '<S4>/Data Type Conversion7' */
  localB->DataTypeConversion7 = rtu_ReferenceValueServerBus->takeoff_flag;

  /* DataTypeConversion: '<S4>/Data Type Conversion9' */
  localB->DataTypeConversion9 = rtu_ReferenceValueServerBus->live_time_ticks;

  /* DataTypeConversion: '<S4>/Data Type Conversion11' */
  localB->DataTypeConversion11 = rtu_Sensors->VisionSensors.usePosVIS_flag;

  /* DataTypeConversion: '<S4>/Data Type Conversion' */
  localB->DataTypeConversion_e = rtu_sensordata_datin;

  /* DataTypeConversion: '<S4>/Data Type Conversion13' */
  localB->DataTypeConversion13 = rtu_sensordata_datin_d;

  /* DataTypeConversion: '<S4>/Data Type Conversion14' */
  localB->DataTypeConversion14 = rtu_sensordata_datin_e;

  /* DataTypeConversion: '<S4>/Data Type Conversion15' */
  rtb_DataTypeConversion15 = rtu_sensordata_datin_c;

  /* DataTypeConversion: '<S4>/Data Type Conversion16' */
  rtb_DataTypeConversion16 = rtu_sensordata_datin_cg;

  /* DataTypeConversion: '<S4>/Data Type Conversion17' */
  rtb_DataTypeConversion17 = rtu_sensordata_datin_i;

  /* DataTypeConversion: '<S4>/Data Type Conversion18' */
  rtb_DataTypeConversion18 = rtu_sensordata_datin_h;

  /* DataTypeConversion: '<S4>/Data Type Conversion19' */
  rtb_DataTypeConversion19 = rtu_sensordata_datin_b;

  /* DataTypeConversion: '<S4>/Data Type Conversion20' */
  rtb_DataTypeConversion20 = rtu_sensordata_datin_dz;

  /* DataTypeConversion: '<S4>/Data Type Conversion21' */
  rtb_DataTypeConversion21 = rtu_sensordata_datin_n;
}

/* System initialize for atomic system: '<Root>/Flight Control System' */
void flightControlSystem_FlightControlSystem_Init
  (DW_FlightControlSystem_flightControlSystem_T *localDW)
{
  /* SystemInitialize for ModelReference: '<S1>/estimator' incorporates:
   *  Constant: '<S1>/controlModePosVsOrient'
   */
  stateEstimator_Init();

  /* SystemInitialize for MATLAB Function: '<S5>/MATLAB Function' */
  localDW->X_ = 0.0;
  localDW->Y_ = 0.0;
  localDW->Z_ = 0.0;
  localDW->T_ = 0.0;

  /* SystemInitialize for ModelReference: '<S1>/controller' */
  flightController_Init();

  /* SystemInitialize for Merge: '<S3>/Merge' */
  flag_outport = ((uint8_T)0U);
}

/* Start for atomic system: '<Root>/Flight Control System' */
void flightControlSystem_FlightControlSystem_Start
  (RT_MODEL_flightControlSystem_T * const flightControlSystem_M,
   DW_FlightControlSystem_flightControlSystem_T *localDW)
{
  /* Start for Atomic SubSystem: '<S1>/Logging' */
  flightControlSystem_Logging_Start(flightControlSystem_M, &localDW->Logging);

  /* End of Start for SubSystem: '<S1>/Logging' */
}

/* Output and update for atomic system: '<Root>/Flight Control System' */
void flightControlSystem_FlightControlSystem(RT_MODEL_flightControlSystem_T *
  const flightControlSystem_M, const CommandBus *rtu_ReferenceValueServerCmds,
  const SensorsBus *rtu_Sensors, real_T rtu_VisionbasedData,
  B_FlightControlSystem_flightControlSystem_T *localB,
  DW_FlightControlSystem_flightControlSystem_T *localDW)
{
  /* BusCreator: '<S1>/BusConversion_InsertedFor_estimator_at_inport_1' */
  localB->BusConversion_InsertedFor_estimator_at_inport_1_BusCreator1.ddx =
    rtu_Sensors->HALSensors.HAL_acc_SI.x;
  localB->BusConversion_InsertedFor_estimator_at_inport_1_BusCreator1.ddy =
    rtu_Sensors->HALSensors.HAL_acc_SI.y;
  localB->BusConversion_InsertedFor_estimator_at_inport_1_BusCreator1.ddz =
    rtu_Sensors->HALSensors.HAL_acc_SI.z;
  localB->BusConversion_InsertedFor_estimator_at_inport_1_BusCreator1.p =
    rtu_Sensors->HALSensors.HAL_gyro_SI.x;
  localB->BusConversion_InsertedFor_estimator_at_inport_1_BusCreator1.q =
    rtu_Sensors->HALSensors.HAL_gyro_SI.y;
  localB->BusConversion_InsertedFor_estimator_at_inport_1_BusCreator1.r =
    rtu_Sensors->HALSensors.HAL_gyro_SI.z;
  localB->BusConversion_InsertedFor_estimator_at_inport_1_BusCreator1.altitude_sonar
    = rtu_Sensors->HALSensors.HAL_ultrasound_SI.altitude;
  localB->BusConversion_InsertedFor_estimator_at_inport_1_BusCreator1.prs =
    rtu_Sensors->HALSensors.HAL_pressure_SI.pressure;
  localB->BusConversion_InsertedFor_estimator_at_inport_1_BusCreator1.vbat_V =
    rtu_Sensors->HALSensors.HAL_vbat_SI.vbat_V;
  localB->BusConversion_InsertedFor_estimator_at_inport_1_BusCreator1.vbat_percentage
    = rtu_Sensors->HALSensors.HAL_vbat_SI.vbat_percentage;

  /* ModelReference: '<S1>/estimator' incorporates:
   *  Constant: '<S1>/controlModePosVsOrient'
   */
  stateEstimator(&rtCP_controlModePosVsOrient_Value,
                 &localB->BusConversion_InsertedFor_estimator_at_inport_1_BusCreator1,
                 &rtu_Sensors->VisionSensors.usePosVIS_flag,
                 &rtu_Sensors->VisionSensors.opticalFlow_data[0],
                 &rtu_Sensors->VisionSensors.posVIS_data[0],
                 &rtu_Sensors->SensorCalibration[0], &localB->estimator);

  /* BusAssignment: '<S1>/Control Mode Update' incorporates:
   *  Constant: '<S1>/controlModePosVsOrient'
   */
  localB->ControlModeUpdate = *rtu_ReferenceValueServerCmds;
  localB->ControlModeUpdate.controlModePosVSOrient = true;

  /* MATLAB Function: '<S5>/MATLAB Function2' incorporates:
   *  Constant: '<S5>/F3'
   *  Constant: '<S5>/F4'
   *  Constant: '<S5>/F5'
   */
  localB->vXY = 1.7 / 1000.0;
  switch ((int32_T)rtu_VisionbasedData) {
   case 1:
    localB->rtb_dE_idx_1 = 0.0;
    localB->dT = -1.0;
    localB->dZ = 0.0;
    break;

   case 2:
    localB->rtb_dE_idx_1 = 1.0;
    localB->dT = 0.0;
    localB->dZ = 0.0;
    break;

   case 3:
    localB->rtb_dE_idx_1 = 0.0;
    localB->dT = 1.0;
    localB->dZ = 0.0;
    break;

   case 5:
    localB->rtb_dE_idx_1 = -1.0;
    localB->dT = 0.0;
    localB->dZ = 0.0;
    break;

   case 4:
    localB->rtb_dE_idx_1 = 0.1;
    localB->dT = 0.0;
    localB->dZ = 1.0;
    break;

   default:
    localB->rtb_dE_idx_1 = 0.0;
    localB->dT = 0.0;
    localB->dZ = 0.0;
    break;
  }

  localB->rtb_dE_idx_0 = 0.0 * localB->vXY;
  localB->vXY *= localB->rtb_dE_idx_1;
  localB->dT *= 1.5 / 1000.0;
  localB->dZ *= 3.0 / 1000.0;

  /* MATLAB Function: '<S5>/MATLAB Function' incorporates:
   *  MATLAB Function: '<S5>/MATLAB Function2'
   */
  if (localB->ControlModeUpdate.live_time_ticks < 600U) {
    localDW->X_ = 0.0;
    localDW->Y_ = 0.0;
    localDW->Z_ = -1.5;
    localDW->T_ = 0.0;
  } else {
    localB->rtb_dE_idx_1 = (real32_T)cos(localB->estimator.yaw);
    localB->X__tmp = (real32_T)sin(localB->estimator.yaw);
    localDW->X_ += localB->rtb_dE_idx_0 * -localB->X__tmp + localB->vXY *
      localB->rtb_dE_idx_1;
    localDW->Y_ += localB->rtb_dE_idx_0 * localB->rtb_dE_idx_1 + localB->vXY *
      localB->X__tmp;
    localDW->Z_ += localB->dZ;
    localDW->T_ += localB->dT;
  }

  /* BusAssignment: '<S5>/Bus  Assignment' incorporates:
   *  Constant: '<S5>/F1'
   *  Constant: '<S5>/F2'
   *  DataTypeConversion: '<S5>/Data Type Conversion'
   *  DataTypeConversion: '<S5>/Data Type Conversion1'
   *  MATLAB Function: '<S5>/MATLAB Function'
   */
  localB->BusAssignment = localB->ControlModeUpdate;
  localB->BusAssignment.pos_ref[0] = (real32_T)localDW->X_;
  localB->BusAssignment.pos_ref[1] = (real32_T)localDW->Y_;
  localB->BusAssignment.pos_ref[2] = (real32_T)localDW->Z_;
  localB->BusAssignment.orient_ref[0] = (real32_T)localDW->T_;
  localB->BusAssignment.orient_ref[1] = (real32_T)0.0;
  localB->BusAssignment.orient_ref[2] = (real32_T)0.0;

  /* ModelReference: '<S1>/controller' */
  flightController(&localB->BusAssignment, &localB->estimator, &motors_outport[0],
                   &localB->controller_o2[0]);

  /* Outputs for Atomic SubSystem: '<S1>/Logging' */
  flightControlSystem_Logging(flightControlSystem_M, motors_outport,
    localB->controller_o2, &localB->estimator, &localB->BusAssignment,
    rtu_Sensors, rtu_Sensors->HALSensors.HAL_acc_SI.x,
    rtu_Sensors->HALSensors.HAL_acc_SI.y, rtu_Sensors->HALSensors.HAL_acc_SI.z,
    rtu_Sensors->HALSensors.HAL_gyro_SI.x, rtu_Sensors->HALSensors.HAL_gyro_SI.y,
    rtu_Sensors->HALSensors.HAL_gyro_SI.z,
    rtu_Sensors->HALSensors.HAL_ultrasound_SI.altitude,
    rtu_Sensors->HALSensors.HAL_pressure_SI.pressure,
    rtu_Sensors->HALSensors.HAL_vbat_SI.vbat_V,
    rtu_Sensors->HALSensors.HAL_vbat_SI.vbat_percentage, &localB->Logging,
    &localDW->Logging);

  /* End of Outputs for SubSystem: '<S1>/Logging' */

  /* If: '<S3>/If' incorporates:
   *  Abs: '<S3>/Abs'
   *  Abs: '<S3>/Abs1'
   *  Abs: '<S3>/Abs2'
   *  Abs: '<S3>/Abs3'
   *  Abs: '<S3>/Abs4'
   *  Abs: '<S3>/Abs5'
   *  Constant: '<S10>/Constant'
   *  Constant: '<S11>/Constant'
   *  Constant: '<S12>/Constant'
   *  Constant: '<S7>/Constant'
   *  Constant: '<S8>/Constant'
   *  Constant: '<S9>/Constant'
   *  Gain: '<S3>/Gain'
   *  Gain: '<S3>/Gain1'
   *  Logic: '<S3>/Logical Operator'
   *  Logic: '<S3>/Logical Operator1'
   *  Logic: '<S3>/Logical Operator2'
   *  Logic: '<S3>/Logical Operator3'
   *  RelationalOperator: '<S10>/Compare'
   *  RelationalOperator: '<S11>/Compare'
   *  RelationalOperator: '<S12>/Compare'
   *  RelationalOperator: '<S7>/Compare'
   *  RelationalOperator: '<S8>/Compare'
   *  RelationalOperator: '<S9>/Compare'
   *  Sum: '<S3>/Subtract'
   *  Sum: '<S3>/Subtract1'
   */
  if (((real32_T)fabs(localB->estimator.X) > 10.0F) || ((real32_T)fabs
       (localB->estimator.Y) > 10.0F)) {
    /* Outputs for IfAction SubSystem: '<S3>/Geofencing error' incorporates:
     *  ActionPort: '<S13>/Action Port'
     */
    /* SignalConversion: '<S13>/OutportBufferForOut1' incorporates:
     *  Constant: '<S13>/Constant'
     */
    flag_outport = ((uint8_T)1U);

    /* End of Outputs for SubSystem: '<S3>/Geofencing error' */
  } else if (((((real32_T)fabs(rtu_Sensors->VisionSensors.opticalFlow_data[0]) >
                0.01F) && ((real32_T)fabs(1.0F *
      rtu_Sensors->VisionSensors.opticalFlow_data[0] - localB->estimator.dx) >
                6.0F)) || (((real32_T)fabs(1.0F *
      rtu_Sensors->VisionSensors.opticalFlow_data[1] - localB->estimator.dy) >
                6.0F) && ((real32_T)fabs
                          (rtu_Sensors->VisionSensors.opticalFlow_data[1]) >
                          0.01F))) > 0) {
    /* Outputs for IfAction SubSystem: '<S3>/estimator//Optical flow error' incorporates:
     *  ActionPort: '<S15>/Action Port'
     */
    /* SignalConversion: '<S15>/OutportBufferForOut1' incorporates:
     *  Constant: '<S15>/Constant'
     */
    flag_outport = ((uint8_T)99U);

    /* End of Outputs for SubSystem: '<S3>/estimator//Optical flow error' */
  } else {
    /* Outputs for IfAction SubSystem: '<S3>/Normal condition' incorporates:
     *  ActionPort: '<S14>/Action Port'
     */
    /* SignalConversion: '<S14>/OutportBufferForOut1' incorporates:
     *  Constant: '<S14>/Constant'
     */
    flag_outport = ((uint8_T)0U);

    /* End of Outputs for SubSystem: '<S3>/Normal condition' */
  }

  /* End of If: '<S3>/If' */
}

/* Function for MATLAB Function: '<S2>/MATLAB Function2' */
static void flightControlSystem_power_tHYmGLzj(const real_T a_data[], const
  int32_T *a_size, real_T y_data[], int32_T *y_size)
{
  int32_T loop_ub;
  uint8_T a_idx_0;
  a_idx_0 = (uint8_T)*a_size;
  if (0 <= a_idx_0 - 1) {
    memcpy(&flightControlSystem_B.z1_data[0], &y_data[0], a_idx_0 * sizeof
           (real_T));
  }

  for (loop_ub = 0; loop_ub < a_idx_0; loop_ub++) {
    flightControlSystem_B.z1_data[loop_ub] = a_data[loop_ub] * a_data[loop_ub];
  }

  *y_size = (uint8_T)*a_size;
  if (0 <= a_idx_0 - 1) {
    memcpy(&y_data[0], &flightControlSystem_B.z1_data[0], a_idx_0 * sizeof
           (real_T));
  }
}

/* Function for MATLAB Function: '<S2>/MATLAB Function2' */
static void flightControlSystem_calcularPrAfuera_OofAsJLX(const boolean_T L
  [19200], int16_T E[2])
{
  int32_T Rad;
  int32_T encontrar;
  int16_T y;
  int32_T tmp;
  int32_T tmp_0;
  int32_T tmp_size;
  int8_T A_idx_0;
  int8_T A_idx_1;
  int8_T B_idx_0;
  uint8_T C_idx_1;
  uint8_T D_idx_0;
  uint8_T D_idx_1;
  boolean_T exitg1;
  Rad = 1;
  encontrar = 0;
  memset(&flightControlSystem_B.ET_[0], 0, 400U * sizeof(int16_T));
  flightControlSystem_B.m = 1.0;
  while ((encontrar == 0) && (Rad < 160)) {
    if (1 < 60 - Rad) {
      A_idx_0 = (int8_T)(60 - Rad);
    } else {
      A_idx_0 = 1;
    }

    if (1 < 80 - Rad) {
      A_idx_1 = (int8_T)(80 - Rad);
    } else {
      A_idx_1 = 1;
    }

    if (120 > 60 + Rad) {
      B_idx_0 = (int8_T)(60 + Rad);
    } else {
      B_idx_0 = 120;
    }

    if (160 > 80 + Rad) {
      C_idx_1 = (uint8_T)(80 + Rad);
    } else {
      C_idx_1 = 160U;
    }

    if (120 > 60 + Rad) {
      D_idx_0 = (uint8_T)(60 + Rad);
    } else {
      D_idx_0 = 120U;
    }

    if (160 > 80 + Rad) {
      D_idx_1 = (uint8_T)(80 + Rad);
    } else {
      D_idx_1 = 160U;
    }

    for (y = A_idx_0; y <= B_idx_0; y++) {
      if (L[((A_idx_1 - 1) * 120 + y) - 1]) {
        encontrar = (int32_T)flightControlSystem_B.m - 1;
        flightControlSystem_B.ET_[encontrar] = y;
        flightControlSystem_B.ET_[200 + encontrar] = A_idx_1;
        flightControlSystem_B.m++;
        encontrar = 1;
      }
    }

    for (y = A_idx_1; y <= C_idx_1; y++) {
      if (L[((y - 1) * 120 + A_idx_0) - 1]) {
        encontrar = (int32_T)flightControlSystem_B.m - 1;
        flightControlSystem_B.ET_[encontrar] = (uint8_T)A_idx_0;
        flightControlSystem_B.ET_[200 + encontrar] = (uint8_T)y;
        flightControlSystem_B.m++;
        encontrar = 1;
      }
    }

    if (1 < 60 - Rad) {
      y = (uint8_T)(60 - Rad);
    } else {
      y = 1;
    }

    while (y <= D_idx_0) {
      if (L[((C_idx_1 - 1) * 120 + y) - 1]) {
        encontrar = (int32_T)flightControlSystem_B.m - 1;
        flightControlSystem_B.ET_[encontrar] = y;
        flightControlSystem_B.ET_[200 + encontrar] = C_idx_1;
        flightControlSystem_B.m++;
        encontrar = 1;
      }

      y++;
    }

    if (1 < 80 - Rad) {
      y = (int8_T)(80 - Rad);
    } else {
      y = 1;
    }

    while (y <= D_idx_1) {
      if (L[((y - 1) * 120 + B_idx_0) - 1]) {
        encontrar = (int32_T)flightControlSystem_B.m - 1;
        flightControlSystem_B.ET_[encontrar] = (uint8_T)B_idx_0;
        flightControlSystem_B.ET_[200 + encontrar] = (uint8_T)y;
        flightControlSystem_B.m++;
        encontrar = 1;
      }

      y++;
    }

    Rad++;
  }

  if (flightControlSystem_B.m > 1.0) {
    Rad = (int32_T)(flightControlSystem_B.m - 1.0);
    if (0 <= Rad - 1) {
      memcpy(&flightControlSystem_B.ET_data_c[0], &flightControlSystem_B.ET_[0],
             Rad * sizeof(int16_T));
    }

    for (encontrar = 0; encontrar < Rad; encontrar++) {
      flightControlSystem_B.ET_data_c[encontrar + Rad] =
        flightControlSystem_B.ET_[encontrar + 200];
    }

    tmp = (int32_T)(flightControlSystem_B.m - 1.0);
    tmp_0 = (int32_T)(flightControlSystem_B.m - 1.0);
    for (encontrar = 0; encontrar < tmp; encontrar++) {
      flightControlSystem_B.ET_data[encontrar] = (real_T)
        flightControlSystem_B.ET_data_c[encontrar] - 60.0;
    }

    flightControlSystem_power_tHYmGLzj(flightControlSystem_B.ET_data, &tmp,
      flightControlSystem_B.tmp_data, &tmp_size);
    for (encontrar = 0; encontrar < tmp_0; encontrar++) {
      flightControlSystem_B.ET_data[encontrar] = (real_T)
        flightControlSystem_B.ET_data_c[encontrar + Rad] - 80.0;
    }

    flightControlSystem_power_tHYmGLzj(flightControlSystem_B.ET_data, &tmp_0,
      flightControlSystem_B.tmp_data_m, &tmp);
    for (encontrar = 0; encontrar < tmp_size; encontrar++) {
      flightControlSystem_B.EM_data[encontrar] =
        flightControlSystem_B.tmp_data[encontrar] +
        flightControlSystem_B.tmp_data_m[encontrar];
    }

    sqrt_GxpnL5DU(flightControlSystem_B.EM_data, &tmp_size);
    if (tmp_size <= 2) {
      if (tmp_size == 1) {
        flightControlSystem_B.m = flightControlSystem_B.EM_data[0];
      } else if ((flightControlSystem_B.EM_data[0] >
                  flightControlSystem_B.EM_data[1]) || (rtIsNaN
                  (flightControlSystem_B.EM_data[0]) && (!rtIsNaN
                   (flightControlSystem_B.EM_data[1])))) {
        flightControlSystem_B.m = flightControlSystem_B.EM_data[1];
      } else {
        flightControlSystem_B.m = flightControlSystem_B.EM_data[0];
      }
    } else {
      if (!rtIsNaN(flightControlSystem_B.EM_data[0])) {
        encontrar = 1;
      } else {
        encontrar = 0;
        tmp = 2;
        exitg1 = false;
        while ((!exitg1) && (tmp <= tmp_size)) {
          if (!rtIsNaN(flightControlSystem_B.EM_data[tmp - 1])) {
            encontrar = tmp;
            exitg1 = true;
          } else {
            tmp++;
          }
        }
      }

      if (encontrar == 0) {
        flightControlSystem_B.m = flightControlSystem_B.EM_data[0];
      } else {
        flightControlSystem_B.m = flightControlSystem_B.EM_data[encontrar - 1];
        while (encontrar + 1 <= tmp_size) {
          if (flightControlSystem_B.m > flightControlSystem_B.EM_data[encontrar])
          {
            flightControlSystem_B.m = flightControlSystem_B.EM_data[encontrar];
          }

          encontrar++;
        }
      }
    }

    for (encontrar = 0; encontrar < tmp_size; encontrar++) {
      flightControlSystem_B.c_x_data[encontrar] =
        (flightControlSystem_B.EM_data[encontrar] == flightControlSystem_B.m);
    }

    encontrar = 0;
    tmp_0 = 1;
    exitg1 = false;
    while ((!exitg1) && (tmp_0 <= tmp_size)) {
      if (flightControlSystem_B.c_x_data[tmp_0 - 1]) {
        encontrar++;
        flightControlSystem_B.ii_data[encontrar - 1] = tmp_0;
        if (encontrar >= tmp_size) {
          exitg1 = true;
        } else {
          tmp_0++;
        }
      } else {
        tmp_0++;
      }
    }

    E[0] = flightControlSystem_B.ET_data_c[flightControlSystem_B.ii_data[0] - 1];
    E[1] = flightControlSystem_B.ET_data_c[(flightControlSystem_B.ii_data[0] +
      Rad) - 1];
  } else {
    E[0] = 60;
    E[1] = 80;
  }
}

/* Function for MATLAB Function: '<S2>/MATLAB Function2' */
static real_T flightControlSystem_norm_cPn2UTGY(const real_T x[2])
{
  real_T y;
  real_T absxk;
  real_T t;
  flightControlSystem_B.scale = 3.3121686421112381E-170;
  absxk = fabs(x[0]);
  if (absxk > 3.3121686421112381E-170) {
    y = 1.0;
    flightControlSystem_B.scale = absxk;
  } else {
    t = absxk / 3.3121686421112381E-170;
    y = t * t;
  }

  absxk = fabs(x[1]);
  if (absxk > flightControlSystem_B.scale) {
    t = flightControlSystem_B.scale / absxk;
    y = y * t * t + 1.0;
    flightControlSystem_B.scale = absxk;
  } else {
    t = absxk / flightControlSystem_B.scale;
    y += t * t;
  }

  return flightControlSystem_B.scale * sqrt(y);
}

/* Function for MATLAB Function: '<S2>/MATLAB Function2' */
static void flightControlSystem_calcularVectores_CEyKXHdf(const boolean_T L
  [19200], const int16_T Pr[2], real_T A[2], real_T B[2], real_T C[2], real_T D
  [2])
{
  boolean_T AK;
  boolean_T BK;
  boolean_T CK;
  boolean_T DK;
  int32_T b;
  int32_T b_y;
  int32_T PrD_idx_1;
  int32_T Bp_idx_0;
  int32_T Cp_idx_1;
  int32_T Dp_idx_0;
  boolean_T exitg1;
  if (1.0 > (real_T)Pr[0] - 1.0) {
    flightControlSystem_B.Am = 1.0;
  } else {
    flightControlSystem_B.Am = (real_T)Pr[0] - 1.0;
  }

  if (120.0 < (real_T)Pr[0] + 1.0) {
    flightControlSystem_B.AM = 120.0;
  } else {
    flightControlSystem_B.AM = (real_T)Pr[0] + 1.0;
  }

  if (1.0 > (real_T)Pr[1] - 1.0) {
    flightControlSystem_B.Bm = 1.0;
  } else {
    flightControlSystem_B.Bm = (real_T)Pr[1] - 1.0;
  }

  if (160.0 < (real_T)Pr[1] + 1.0) {
    flightControlSystem_B.BM = 160.0;
  } else {
    flightControlSystem_B.BM = (real_T)Pr[1] + 1.0;
  }

  if (1.0 > (real_T)Pr[0] - 1.0) {
    flightControlSystem_B.Cm = 1.0;
  } else {
    flightControlSystem_B.Cm = (real_T)Pr[0] - 1.0;
  }

  if (120.0 < (real_T)Pr[0] + 1.0) {
    flightControlSystem_B.CM = 120.0;
  } else {
    flightControlSystem_B.CM = (real_T)Pr[0] + 1.0;
  }

  if (1.0 > (real_T)Pr[1] - 1.0) {
    flightControlSystem_B.Dm = 1.0;
  } else {
    flightControlSystem_B.Dm = (real_T)Pr[1] - 1.0;
  }

  if (160.0 < (real_T)Pr[1] + 1.0) {
    flightControlSystem_B.DM = 160.0;
  } else {
    flightControlSystem_B.DM = (real_T)Pr[1] + 1.0;
  }

  if (1.0 > (real_T)Pr[1] - 1.0) {
    flightControlSystem_B.AR = 1.0;
  } else {
    flightControlSystem_B.AR = (real_T)Pr[1] - 1.0;
  }

  if (1.0 > (real_T)Pr[0] - 1.0) {
    flightControlSystem_B.BR = 1.0;
  } else {
    flightControlSystem_B.BR = (real_T)Pr[0] - 1.0;
  }

  if (160.0 < (real_T)Pr[1] + 1.0) {
    flightControlSystem_B.CR = 160.0;
  } else {
    flightControlSystem_B.CR = (real_T)Pr[1] + 1.0;
  }

  if (120.0 < (real_T)Pr[0] + 1.0) {
    flightControlSystem_B.DR = 120.0;
  } else {
    flightControlSystem_B.DR = (real_T)Pr[0] + 1.0;
  }

  AK = true;
  BK = true;
  CK = true;
  DK = true;
  memset(&flightControlSystem_B.AVef[0], 0, 200U * sizeof(real_T));
  memset(&flightControlSystem_B.BVef[0], 0, 200U * sizeof(real_T));
  memset(&flightControlSystem_B.CVef[0], 0, 200U * sizeof(real_T));
  memset(&flightControlSystem_B.DVef[0], 0, 200U * sizeof(real_T));
  flightControlSystem_B.PrD_idx_0 = (flightControlSystem_B.Am +
    flightControlSystem_B.AM) / 2.0;
  PrD_idx_1 = (int32_T)flightControlSystem_B.AR;
  Bp_idx_0 = (int32_T)flightControlSystem_B.BR;
  flightControlSystem_B.Bp_idx_1 = (flightControlSystem_B.Bm +
    flightControlSystem_B.BM) / 2.0;
  flightControlSystem_B.Cp_idx_0 = (flightControlSystem_B.Cm +
    flightControlSystem_B.CM) / 2.0;
  Cp_idx_1 = (int32_T)flightControlSystem_B.CR;
  Dp_idx_0 = (int32_T)flightControlSystem_B.DR;
  flightControlSystem_B.Dp_idx_1 = (flightControlSystem_B.Dm +
    flightControlSystem_B.DM) / 2.0;
  while (AK || BK || CK || DK) {
    if (AK) {
      flightControlSystem_B.AC = 1.0;
      b = (int32_T)((1.0 - flightControlSystem_B.Am) + flightControlSystem_B.AM);
      for (b_y = 0; b_y < b; b_y++) {
        flightControlSystem_B.x = flightControlSystem_B.Am + (real_T)b_y;
        if (L[(((int32_T)flightControlSystem_B.AR - 1) * 120 + (int32_T)
               flightControlSystem_B.x) - 1]) {
          flightControlSystem_B.AVef[(int32_T)flightControlSystem_B.AC - 1] =
            flightControlSystem_B.x;
          flightControlSystem_B.AC++;
        }
      }

      if ((flightControlSystem_B.AC > 1.0) && (flightControlSystem_B.AR != 1.0))
      {
        b = (int32_T)(flightControlSystem_B.AC - 1.0);
        if ((int32_T)(flightControlSystem_B.AC - 1.0) <= 2) {
          if ((int32_T)(flightControlSystem_B.AC - 1.0) == 1) {
            flightControlSystem_B.Am = flightControlSystem_B.AVef[0];
          } else if (flightControlSystem_B.AVef[0] > flightControlSystem_B.AVef
                     [1]) {
            flightControlSystem_B.Am = flightControlSystem_B.AVef[1];
          } else {
            flightControlSystem_B.Am = flightControlSystem_B.AVef[0];
          }
        } else {
          flightControlSystem_B.Am = flightControlSystem_B.AVef[0];
          for (b_y = 1; b_y < b; b_y++) {
            if (flightControlSystem_B.Am > flightControlSystem_B.AVef[b_y]) {
              flightControlSystem_B.Am = flightControlSystem_B.AVef[b_y];
            }
          }
        }

        if (flightControlSystem_B.Am - 1.0 > 1.0) {
          flightControlSystem_B.Am--;
        } else {
          flightControlSystem_B.Am = 1.0;
        }

        b = (int32_T)(flightControlSystem_B.AC - 1.0);
        if ((int32_T)(flightControlSystem_B.AC - 1.0) <= 2) {
          if ((int32_T)(flightControlSystem_B.AC - 1.0) == 1) {
            flightControlSystem_B.AM = flightControlSystem_B.AVef[0];
          } else if (flightControlSystem_B.AVef[0] < flightControlSystem_B.AVef
                     [1]) {
            flightControlSystem_B.AM = flightControlSystem_B.AVef[1];
          } else {
            flightControlSystem_B.AM = flightControlSystem_B.AVef[0];
          }
        } else {
          flightControlSystem_B.AM = flightControlSystem_B.AVef[0];
          for (b_y = 1; b_y < b; b_y++) {
            if (flightControlSystem_B.AM < flightControlSystem_B.AVef[b_y]) {
              flightControlSystem_B.AM = flightControlSystem_B.AVef[b_y];
            }
          }
        }

        if (flightControlSystem_B.AM + 1.0 < 120.0) {
          flightControlSystem_B.AM++;
        } else {
          flightControlSystem_B.AM = 120.0;
        }

        flightControlSystem_B.AR--;
        AK = !(flightControlSystem_B.AR == 1.0);
      } else {
        flightControlSystem_B.Am++;
        flightControlSystem_B.AM--;
        if (!any_JducOVS3(flightControlSystem_B.AVef)) {
          flightControlSystem_B.AR++;
        }

        AK = false;
      }
    }

    flightControlSystem_B.AC = 1.0;
    if (BK) {
      b = (int32_T)((1.0 - flightControlSystem_B.Bm) + flightControlSystem_B.BM);
      for (b_y = 0; b_y < b; b_y++) {
        flightControlSystem_B.x = flightControlSystem_B.Bm + (real_T)b_y;
        if (L[(((int32_T)flightControlSystem_B.x - 1) * 120 + (int32_T)
               flightControlSystem_B.BR) - 1]) {
          flightControlSystem_B.BVef[(int32_T)flightControlSystem_B.AC - 1] =
            flightControlSystem_B.x;
          flightControlSystem_B.AC++;
        }
      }

      if ((flightControlSystem_B.AC > 1.0) && (flightControlSystem_B.BR != 1.0))
      {
        b = (int32_T)(flightControlSystem_B.AC - 1.0);
        if ((int32_T)(flightControlSystem_B.AC - 1.0) <= 2) {
          if ((int32_T)(flightControlSystem_B.AC - 1.0) == 1) {
            flightControlSystem_B.Bm = flightControlSystem_B.BVef[0];
          } else if (flightControlSystem_B.BVef[0] > flightControlSystem_B.BVef
                     [1]) {
            flightControlSystem_B.Bm = flightControlSystem_B.BVef[1];
          } else {
            flightControlSystem_B.Bm = flightControlSystem_B.BVef[0];
          }
        } else {
          flightControlSystem_B.Bm = flightControlSystem_B.BVef[0];
          for (b_y = 1; b_y < b; b_y++) {
            if (flightControlSystem_B.Bm > flightControlSystem_B.BVef[b_y]) {
              flightControlSystem_B.Bm = flightControlSystem_B.BVef[b_y];
            }
          }
        }

        if (1.0 > flightControlSystem_B.Bm - 1.0) {
          flightControlSystem_B.Bm = 1.0;
        } else {
          flightControlSystem_B.Bm--;
        }

        b = (int32_T)(flightControlSystem_B.AC - 1.0);
        if ((int32_T)(flightControlSystem_B.AC - 1.0) <= 2) {
          if ((int32_T)(flightControlSystem_B.AC - 1.0) == 1) {
            flightControlSystem_B.BM = flightControlSystem_B.BVef[0];
          } else if (flightControlSystem_B.BVef[0] < flightControlSystem_B.BVef
                     [1]) {
            flightControlSystem_B.BM = flightControlSystem_B.BVef[1];
          } else {
            flightControlSystem_B.BM = flightControlSystem_B.BVef[0];
          }
        } else {
          flightControlSystem_B.BM = flightControlSystem_B.BVef[0];
          for (b_y = 1; b_y < b; b_y++) {
            if (flightControlSystem_B.BM < flightControlSystem_B.BVef[b_y]) {
              flightControlSystem_B.BM = flightControlSystem_B.BVef[b_y];
            }
          }
        }

        if (flightControlSystem_B.BM + 1.0 < 160.0) {
          flightControlSystem_B.BM++;
        } else {
          flightControlSystem_B.BM = 160.0;
        }

        flightControlSystem_B.BR--;
      } else {
        flightControlSystem_B.Bm++;
        flightControlSystem_B.BM--;
        if (!any_JducOVS3(flightControlSystem_B.BVef)) {
          flightControlSystem_B.BR++;
        }

        BK = false;
      }
    }

    flightControlSystem_B.AC = 1.0;
    if (CK) {
      b = (int32_T)((1.0 - flightControlSystem_B.Cm) + flightControlSystem_B.CM);
      for (b_y = 0; b_y < b; b_y++) {
        flightControlSystem_B.x = flightControlSystem_B.Cm + (real_T)b_y;
        if (L[(((int32_T)flightControlSystem_B.CR - 1) * 120 + (int32_T)
               flightControlSystem_B.x) - 1]) {
          flightControlSystem_B.CVef[(int32_T)flightControlSystem_B.AC - 1] =
            flightControlSystem_B.x;
          flightControlSystem_B.AC++;
        }
      }

      if ((flightControlSystem_B.AC > 1.0) && (flightControlSystem_B.CR != 160.0))
      {
        b = (int32_T)(flightControlSystem_B.AC - 1.0);
        if ((int32_T)(flightControlSystem_B.AC - 1.0) <= 2) {
          if ((int32_T)(flightControlSystem_B.AC - 1.0) == 1) {
            flightControlSystem_B.Cm = flightControlSystem_B.CVef[0];
          } else if (flightControlSystem_B.CVef[0] > flightControlSystem_B.CVef
                     [1]) {
            flightControlSystem_B.Cm = flightControlSystem_B.CVef[1];
          } else {
            flightControlSystem_B.Cm = flightControlSystem_B.CVef[0];
          }
        } else {
          flightControlSystem_B.Cm = flightControlSystem_B.CVef[0];
          for (b_y = 1; b_y < b; b_y++) {
            if (flightControlSystem_B.Cm > flightControlSystem_B.CVef[b_y]) {
              flightControlSystem_B.Cm = flightControlSystem_B.CVef[b_y];
            }
          }
        }

        if (flightControlSystem_B.Cm - 1.0 > 1.0) {
          flightControlSystem_B.Cm--;
        } else {
          flightControlSystem_B.Cm = 1.0;
        }

        b = (int32_T)(flightControlSystem_B.AC - 1.0);
        if ((int32_T)(flightControlSystem_B.AC - 1.0) <= 2) {
          if ((int32_T)(flightControlSystem_B.AC - 1.0) == 1) {
            flightControlSystem_B.CM = flightControlSystem_B.CVef[0];
          } else if (flightControlSystem_B.CVef[0] < flightControlSystem_B.CVef
                     [1]) {
            flightControlSystem_B.CM = flightControlSystem_B.CVef[1];
          } else {
            flightControlSystem_B.CM = flightControlSystem_B.CVef[0];
          }
        } else {
          flightControlSystem_B.CM = flightControlSystem_B.CVef[0];
          for (b_y = 1; b_y < b; b_y++) {
            if (flightControlSystem_B.CM < flightControlSystem_B.CVef[b_y]) {
              flightControlSystem_B.CM = flightControlSystem_B.CVef[b_y];
            }
          }
        }

        if (flightControlSystem_B.CM + 1.0 < 120.0) {
          flightControlSystem_B.CM++;
        } else {
          flightControlSystem_B.CM = 120.0;
        }

        flightControlSystem_B.CR++;
      } else {
        flightControlSystem_B.Cm++;
        flightControlSystem_B.CM--;
        if (!any_JducOVS3(flightControlSystem_B.CVef)) {
          flightControlSystem_B.CR--;
        }

        CK = false;
      }
    }

    flightControlSystem_B.AC = 1.0;
    if (DK) {
      b = (int32_T)((1.0 - flightControlSystem_B.Dm) + flightControlSystem_B.DM);
      for (b_y = 0; b_y < b; b_y++) {
        flightControlSystem_B.x = flightControlSystem_B.Dm + (real_T)b_y;
        if (L[(((int32_T)flightControlSystem_B.x - 1) * 120 + (int32_T)
               flightControlSystem_B.DR) - 1]) {
          flightControlSystem_B.DVef[(int32_T)flightControlSystem_B.AC - 1] =
            flightControlSystem_B.x;
          flightControlSystem_B.AC++;
        }
      }

      if ((flightControlSystem_B.AC > 1.0) && (flightControlSystem_B.DR != 120.0))
      {
        b = (int32_T)(flightControlSystem_B.AC - 1.0);
        if ((int32_T)(flightControlSystem_B.AC - 1.0) <= 2) {
          if ((int32_T)(flightControlSystem_B.AC - 1.0) == 1) {
            flightControlSystem_B.Dm = flightControlSystem_B.DVef[0];
          } else if (flightControlSystem_B.DVef[0] > flightControlSystem_B.DVef
                     [1]) {
            flightControlSystem_B.Dm = flightControlSystem_B.DVef[1];
          } else {
            flightControlSystem_B.Dm = flightControlSystem_B.DVef[0];
          }
        } else {
          flightControlSystem_B.Dm = flightControlSystem_B.DVef[0];
          for (b_y = 1; b_y < b; b_y++) {
            if (flightControlSystem_B.Dm > flightControlSystem_B.DVef[b_y]) {
              flightControlSystem_B.Dm = flightControlSystem_B.DVef[b_y];
            }
          }
        }

        if (1.0 > flightControlSystem_B.Dm - 1.0) {
          flightControlSystem_B.Dm = 1.0;
        } else {
          flightControlSystem_B.Dm--;
        }

        b = (int32_T)(flightControlSystem_B.AC - 1.0);
        if ((int32_T)(flightControlSystem_B.AC - 1.0) <= 2) {
          if ((int32_T)(flightControlSystem_B.AC - 1.0) == 1) {
            flightControlSystem_B.DM = flightControlSystem_B.DVef[0];
          } else if (flightControlSystem_B.DVef[0] < flightControlSystem_B.DVef
                     [1]) {
            flightControlSystem_B.DM = flightControlSystem_B.DVef[1];
          } else {
            flightControlSystem_B.DM = flightControlSystem_B.DVef[0];
          }
        } else {
          flightControlSystem_B.DM = flightControlSystem_B.DVef[0];
          for (b_y = 1; b_y < b; b_y++) {
            if (flightControlSystem_B.DM < flightControlSystem_B.DVef[b_y]) {
              flightControlSystem_B.DM = flightControlSystem_B.DVef[b_y];
            }
          }
        }

        if (flightControlSystem_B.DM + 1.0 < 160.0) {
          flightControlSystem_B.DM++;
        } else {
          flightControlSystem_B.DM = 160.0;
        }

        if (1.0 > flightControlSystem_B.DR + 1.0) {
          flightControlSystem_B.DR = 1.0;
        } else {
          flightControlSystem_B.DR++;
        }
      } else {
        flightControlSystem_B.Dm++;
        flightControlSystem_B.DM--;
        if (!any_JducOVS3(flightControlSystem_B.DVef)) {
          flightControlSystem_B.DR--;
        }

        DK = false;
      }
    }
  }

  A[0] = flightControlSystem_B.AR - (real_T)PrD_idx_1;
  A[1] = -((flightControlSystem_B.Am + flightControlSystem_B.AM) / 2.0 -
           flightControlSystem_B.PrD_idx_0);
  B[0] = (flightControlSystem_B.Bm + flightControlSystem_B.BM) / 2.0 -
    flightControlSystem_B.Bp_idx_1;
  B[1] = -(flightControlSystem_B.BR - (real_T)Bp_idx_0);
  C[0] = flightControlSystem_B.CR - (real_T)Cp_idx_1;
  C[1] = -((flightControlSystem_B.Cm + flightControlSystem_B.CM) / 2.0 -
           flightControlSystem_B.Cp_idx_0);
  D[0] = (flightControlSystem_B.Dm + flightControlSystem_B.DM) / 2.0 -
    flightControlSystem_B.Dp_idx_1;
  D[1] = -(flightControlSystem_B.DR - (real_T)Dp_idx_0);
  flightControlSystem_B.AR = flightControlSystem_norm_cPn2UTGY(A);
  flightControlSystem_B.varargin_1[0] = flightControlSystem_norm_cPn2UTGY(A);
  flightControlSystem_B.varargin_1[1] = flightControlSystem_norm_cPn2UTGY(B);
  flightControlSystem_B.varargin_1[2] = flightControlSystem_norm_cPn2UTGY(C);
  flightControlSystem_B.varargin_1[3] = flightControlSystem_norm_cPn2UTGY(D);
  if (!rtIsNaN(flightControlSystem_B.AR)) {
    PrD_idx_1 = 1;
  } else {
    PrD_idx_1 = 0;
    Bp_idx_0 = 2;
    exitg1 = false;
    while ((!exitg1) && (Bp_idx_0 < 5)) {
      if (!rtIsNaN(flightControlSystem_B.varargin_1[Bp_idx_0 - 1])) {
        PrD_idx_1 = Bp_idx_0;
        exitg1 = true;
      } else {
        Bp_idx_0++;
      }
    }
  }

  if (PrD_idx_1 != 0) {
    flightControlSystem_B.AR = flightControlSystem_B.varargin_1[PrD_idx_1 - 1];
    while (PrD_idx_1 + 1 < 5) {
      if (flightControlSystem_B.AR < flightControlSystem_B.varargin_1[PrD_idx_1])
      {
        flightControlSystem_B.AR = flightControlSystem_B.varargin_1[PrD_idx_1];
      }

      PrD_idx_1++;
    }
  }

  flightControlSystem_B.AR = fabs(flightControlSystem_B.AR);
  A[0] /= flightControlSystem_B.AR;
  B[0] /= flightControlSystem_B.AR;
  C[0] /= flightControlSystem_B.AR;
  D[0] /= flightControlSystem_B.AR;
  A[1] /= flightControlSystem_B.AR;
  B[1] /= flightControlSystem_B.AR;
  C[1] /= flightControlSystem_B.AR;
  D[1] /= flightControlSystem_B.AR;
}

/* Model step function for TID0 */
void flightControlSystem_step0(void)   /* Sample time: [0.005s, 0.0s] */
{
  /* RateTransition: '<Root>/Rate Transition' */
  flightControlSystem_B.RateTransition =
    flightControlSystem_DW.RateTransition_Buffer[flightControlSystem_DW.RateTransition_ActiveBufIdx];

  /* Outputs for Atomic SubSystem: '<Root>/Flight Control System' */

  /* Inport: '<Root>/AC cmd' incorporates:
   *  Inport: '<Root>/Sensors'
   */
  flightControlSystem_FlightControlSystem(flightControlSystem_M, &cmd_inport,
    &sensor_inport, flightControlSystem_B.RateTransition,
    &flightControlSystem_B.FlightControlSystem,
    &flightControlSystem_DW.FlightControlSystem);

  /* End of Outputs for SubSystem: '<Root>/Flight Control System' */

  /* Outport: '<Root>/Actuators' */
  flightControlSystem_Y.Actuators[0] = motors_outport[0];
  flightControlSystem_Y.Actuators[1] = motors_outport[1];
  flightControlSystem_Y.Actuators[2] = motors_outport[2];
  flightControlSystem_Y.Actuators[3] = motors_outport[3];

  /* Outport: '<Root>/Flag' */
  flightControlSystem_Y.Flag = flag_outport;

  /* Matfile logging */
  rt_UpdateTXYLogVars(flightControlSystem_M->rtwLogInfo,
                      (&flightControlSystem_M->Timing.taskTime0));

  /* signal main to stop simulation */
  {                                    /* Sample time: [0.005s, 0.0s] */
    if ((rtmGetTFinal(flightControlSystem_M)!=-1) &&
        !((rtmGetTFinal(flightControlSystem_M)-
           flightControlSystem_M->Timing.taskTime0) >
          flightControlSystem_M->Timing.taskTime0 * (DBL_EPSILON))) {
      rtmSetErrorStatus(flightControlSystem_M, "Simulation finished");
    }
  }

  /* Update absolute time */
  /* The "clockTick0" counts the number of times the code of this task has
   * been executed. The absolute time is the multiplication of "clockTick0"
   * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
   * overflow during the application lifespan selected.
   * Timer of this task consists of two 32 bit unsigned integers.
   * The two integers represent the low bits Timing.clockTick0 and the high bits
   * Timing.clockTickH0. When the low bit overflows to 0, the high bits increment.
   */
  if (!(++flightControlSystem_M->Timing.clockTick0)) {
    ++flightControlSystem_M->Timing.clockTickH0;
  }

  flightControlSystem_M->Timing.taskTime0 =
    flightControlSystem_M->Timing.clockTick0 *
    flightControlSystem_M->Timing.stepSize0 +
    flightControlSystem_M->Timing.clockTickH0 *
    flightControlSystem_M->Timing.stepSize0 * 4294967296.0;
}

/* Model step function for TID1 */
void flightControlSystem_step1(void)   /* Sample time: [0.2s, 0.0s] */
{
  int32_T k;
  uint8_T rtb_R_0;

  /* Outputs for Atomic SubSystem: '<Root>/Image Processing System' */
  /* ModelReference: '<S2>/Image Conversion from Y1UY2V to YUV' incorporates:
   *  Inport: '<Root>/Image Data'
   */
  imageConversion(&flightControlSystem_U.Y1UY2V[0],
                  &flightControlSystem_B.ImageConversionfromY1UY2VtoYUV_o1[0],
                  &flightControlSystem_B.ImageConversionfromY1UY2VtoYUV_o2[0],
                  &flightControlSystem_B.ImageConversionfromY1UY2VtoYUV_o3[0],
                  &(flightControlSystem_DW.ImageConversionfromY1UY2VtoYUV_InstanceData.rtb));

  /* S-Function (svipcolorconv): '<S2>/Color Space  Conversion' */
  /* Precompute constants */
  flightControlSystem_B.rConst = ((uint16_T)26149U) * 128U;
  flightControlSystem_B.gConst = (((uint16_T)6419U) * 128U + ((uint16_T)13320U) *
    128U) + 8192U;
  flightControlSystem_B.bConst = ((uint16_T)33050U) * 128U;
  for (flightControlSystem_B.i = 0; flightControlSystem_B.i < 19200;
       flightControlSystem_B.i++) {
    /* Convert YcbCr to RGB; apply coefficients and offsets */
    /* derived from the ITU BT.601-5 recommendation; all of the */
    /* coefficients and offsets are scaled (by 2^14) such that */
    /* the conversion can be done using integer arithmetic; this */
    /* routine relies on the user supplying the data in proper */
    /* ranges: Y [16..235], Cb & Cr [16..240] */
    /* Note that all of the operations are grouped in such a way */
    /* that the arithmetic can be done using unsigned integers */
    /* Given that Y is in the proper range, yDiff should */
    /* always be positive */
    flightControlSystem_B.yDiff =
      flightControlSystem_B.ImageConversionfromY1UY2VtoYUV_o1[flightControlSystem_B.i]
      - 16U;

    /* Red; 8192 in this equations is 0.5*2^14 or 2^13; adding 0.5  */
    /* before truncation will result in rounding */
    flightControlSystem_B.pos = (flightControlSystem_B.yDiff * ((uint16_T)19077U)
      + (uint32_T)
      flightControlSystem_B.ImageConversionfromY1UY2VtoYUV_o3[flightControlSystem_B.i]
      * ((uint16_T)26149U)) + 8192U;
    if (flightControlSystem_B.pos > flightControlSystem_B.rConst) {
      flightControlSystem_B.pos -= flightControlSystem_B.rConst;
    } else {
      flightControlSystem_B.pos = 0U;
    }

    flightControlSystem_B.cc1 = flightControlSystem_B.pos >> 14;

    /* limit to avoid wrapping */
    if (flightControlSystem_B.cc1 > 255U) {
      flightControlSystem_B.cc1 = 255U;
    }

    /* Compute green channel */
    flightControlSystem_B.pos = flightControlSystem_B.yDiff * ((uint16_T)19077U)
      + flightControlSystem_B.gConst;
    flightControlSystem_B.neg = (uint32_T)
      flightControlSystem_B.ImageConversionfromY1UY2VtoYUV_o2[flightControlSystem_B.i]
      * ((uint16_T)6419U) + (uint32_T)
      flightControlSystem_B.ImageConversionfromY1UY2VtoYUV_o3[flightControlSystem_B.i]
      * ((uint16_T)13320U);

    /* scale back */
    if (flightControlSystem_B.pos > flightControlSystem_B.neg) {
      flightControlSystem_B.pos -= flightControlSystem_B.neg;
    } else {
      flightControlSystem_B.pos = 0U;
    }

    flightControlSystem_B.neg = flightControlSystem_B.pos >> 14;
    if (flightControlSystem_B.neg > 255U) {
      flightControlSystem_B.neg = 255U;
    }

    /* Compute blue channel */
    flightControlSystem_B.pos = (flightControlSystem_B.yDiff * ((uint16_T)19077U)
      + (uint32_T)
      flightControlSystem_B.ImageConversionfromY1UY2VtoYUV_o2[flightControlSystem_B.i]
      * ((uint16_T)33050U)) + 8192U;

    /* scale back */
    if (flightControlSystem_B.pos > flightControlSystem_B.bConst) {
      flightControlSystem_B.pos -= flightControlSystem_B.bConst;
    } else {
      flightControlSystem_B.pos = 0U;
    }

    flightControlSystem_B.yDiff = flightControlSystem_B.pos >> 14;
    if (flightControlSystem_B.yDiff > 255U) {
      flightControlSystem_B.yDiff = 255U;
    }

    /* assign back the results */
    flightControlSystem_B.ColorSpaceConversion_o1[flightControlSystem_B.i] =
      (uint8_T)flightControlSystem_B.cc1;
    flightControlSystem_B.ColorSpaceConversion_o2[flightControlSystem_B.i] =
      (uint8_T)flightControlSystem_B.neg;
    flightControlSystem_B.ColorSpaceConversion_o3[flightControlSystem_B.i] =
      (uint8_T)flightControlSystem_B.yDiff;
  }

  /* End of S-Function (svipcolorconv): '<S2>/Color Space  Conversion' */

  /* MATLAB Function: '<S2>/MATLAB Function1' incorporates:
   *  Constant: '<S2>/r1'
   *  Constant: '<S2>/r2'
   *  Constant: '<S2>/r3'
   *  Constant: '<S2>/r4'
   *  Constant: '<S2>/r5'
   *  Constant: '<S2>/r6'
   *  Constant: '<S2>/r7'
   *  Constant: '<S2>/r8'
   *  Constant: '<S2>/r9'
   */
  if (!flightControlSystem_DW.x_not_empty) {
    flightControlSystem_DW.x_not_empty = true;
    for (k = 0; k < 256; k++) {
      flightControlSystem_B.dv0[k] = flightControlSystem_DW.x[k] - 255.0;
    }

    power_pDWqnch0(flightControlSystem_B.dv0, flightControlSystem_DW.ra);
    for (k = 0; k < 256; k++) {
      flightControlSystem_DW.ra[k] = -(flightControlSystem_DW.ra[k] / 1000.0);
      flightControlSystem_DW.ra[k] = exp(flightControlSystem_DW.ra[k]);
      flightControlSystem_DW.ra[k] *= 255.0;
      flightControlSystem_B.dv0[k] = flightControlSystem_DW.x[k] - 0.0;
    }

    power_pDWqnch0(flightControlSystem_B.dv0, flightControlSystem_DW.gv);
    for (k = 0; k < 256; k++) {
      flightControlSystem_DW.gv[k] = -(flightControlSystem_DW.gv[k] / 1000.0);
      flightControlSystem_DW.gv[k] = exp(flightControlSystem_DW.gv[k]);
      flightControlSystem_DW.gv[k] *= 0.0;
      flightControlSystem_B.TAB = flightControlSystem_DW.x[k] - 0.0;
      flightControlSystem_DW.bv[k] = flightControlSystem_B.TAB *
        flightControlSystem_B.TAB;
      flightControlSystem_DW.bv[k] = -(flightControlSystem_DW.bv[k] / 1000.0);
      flightControlSystem_DW.bv[k] = exp(flightControlSystem_DW.bv[k]);
      flightControlSystem_DW.bv[k] *= 0.0;
    }
  }

  /* MATLAB Function: '<S2>/MATLAB Function' incorporates:
   *  MATLAB Function: '<S2>/MATLAB Function1'
   */
  memcpy(&flightControlSystem_B.R[0],
         &flightControlSystem_B.ColorSpaceConversion_o1[0], 19200U * sizeof
         (uint8_T));
  memcpy(&flightControlSystem_B.G[0],
         &flightControlSystem_B.ColorSpaceConversion_o2[0], 19200U * sizeof
         (uint8_T));
  memcpy(&flightControlSystem_B.B[0],
         &flightControlSystem_B.ColorSpaceConversion_o3[0], 19200U * sizeof
         (uint8_T));
  for (flightControlSystem_B.i = 0; flightControlSystem_B.i < 120;
       flightControlSystem_B.i++) {
    for (flightControlSystem_B.Y = 0; flightControlSystem_B.Y < 160;
         flightControlSystem_B.Y++) {
      k = 120 * flightControlSystem_B.Y + flightControlSystem_B.i;
      if (flightControlSystem_B.R[k] < 1) {
        rtb_R_0 = 1U;
      } else {
        rtb_R_0 = flightControlSystem_B.R[120 * flightControlSystem_B.Y +
          flightControlSystem_B.i];
      }

      flightControlSystem_B.TAB = rt_roundd_snf
        (flightControlSystem_DW.ra[rtb_R_0 - 1]);
      if (flightControlSystem_B.TAB < 256.0) {
        if (flightControlSystem_B.TAB >= 0.0) {
          flightControlSystem_B.R[flightControlSystem_B.i + 120 *
            flightControlSystem_B.Y] = (uint8_T)flightControlSystem_B.TAB;
        } else {
          flightControlSystem_B.R[flightControlSystem_B.i + 120 *
            flightControlSystem_B.Y] = 0U;
        }
      } else {
        flightControlSystem_B.R[flightControlSystem_B.i + 120 *
          flightControlSystem_B.Y] = MAX_uint8_T;
      }

      if (flightControlSystem_B.G[k] < 1) {
        rtb_R_0 = 1U;
      } else {
        rtb_R_0 = flightControlSystem_B.G[120 * flightControlSystem_B.Y +
          flightControlSystem_B.i];
      }

      flightControlSystem_B.TAB = rt_roundd_snf
        (flightControlSystem_DW.gv[rtb_R_0 - 1]);
      if (flightControlSystem_B.TAB < 256.0) {
        if (flightControlSystem_B.TAB >= 0.0) {
          flightControlSystem_B.G[flightControlSystem_B.i + 120 *
            flightControlSystem_B.Y] = (uint8_T)flightControlSystem_B.TAB;
        } else {
          flightControlSystem_B.G[flightControlSystem_B.i + 120 *
            flightControlSystem_B.Y] = 0U;
        }
      } else {
        flightControlSystem_B.G[flightControlSystem_B.i + 120 *
          flightControlSystem_B.Y] = MAX_uint8_T;
      }

      if (flightControlSystem_B.B[k] < 1) {
        rtb_R_0 = 1U;
      } else {
        rtb_R_0 = flightControlSystem_B.B[120 * flightControlSystem_B.Y +
          flightControlSystem_B.i];
      }

      flightControlSystem_B.TAB = rt_roundd_snf
        (flightControlSystem_DW.bv[rtb_R_0 - 1]);
      if (flightControlSystem_B.TAB < 256.0) {
        if (flightControlSystem_B.TAB >= 0.0) {
          flightControlSystem_B.B[flightControlSystem_B.i + 120 *
            flightControlSystem_B.Y] = (uint8_T)flightControlSystem_B.TAB;
        } else {
          flightControlSystem_B.B[flightControlSystem_B.i + 120 *
            flightControlSystem_B.Y] = 0U;
        }
      } else {
        flightControlSystem_B.B[flightControlSystem_B.i + 120 *
          flightControlSystem_B.Y] = MAX_uint8_T;
      }
    }
  }

  /* End of MATLAB Function: '<S2>/MATLAB Function' */

  /* MATLAB Function: '<S2>/MATLAB Function3' incorporates:
   *  Constant: '<S2>/Constant2'
   */
  for (flightControlSystem_B.i = 0; flightControlSystem_B.i < 19200;
       flightControlSystem_B.i++) {
    k = (int32_T)rt_roundd_snf((real_T)
      (flightControlSystem_B.R[flightControlSystem_B.i] +
       flightControlSystem_B.G[flightControlSystem_B.i]) + (real_T)
      flightControlSystem_B.B[flightControlSystem_B.i] / 3.0);
    if (k < 256) {
      rtb_R_0 = (uint8_T)k;
    } else {
      rtb_R_0 = MAX_uint8_T;
    }

    flightControlSystem_B.Al[flightControlSystem_B.i] = (rtb_R_0 > 127.0);
  }

  /* End of MATLAB Function: '<S2>/MATLAB Function3' */

  /* MATLAB Function: '<S2>/MATLAB Function2' incorporates:
   *  Bias: '<S44>/Subtract min x'
   */
  flightControlSystem_B.Pr[0] = 60;
  flightControlSystem_B.Pr[1] = 80;
  if (!flightControlSystem_B.Al[9539]) {
    flightControlSystem_calcularPrAfuera_OofAsJLX(flightControlSystem_B.Al,
      flightControlSystem_B.Pr);
  }

  flightControlSystem_calcularVectores_CEyKXHdf(flightControlSystem_B.Al,
    flightControlSystem_B.Pr, flightControlSystem_B.A_k,
    flightControlSystem_B.B_c, flightControlSystem_B.C, flightControlSystem_B.D);
  flightControlSystem_B.TAB = acos((flightControlSystem_B.A_k[0] *
    flightControlSystem_B.B_c[0] + flightControlSystem_B.A_k[1] *
    flightControlSystem_B.B_c[1]) / (flightControlSystem_norm_cPn2UTGY
    (flightControlSystem_B.A_k) * flightControlSystem_norm_cPn2UTGY
    (flightControlSystem_B.B_c)));
  if (rtIsNaN(flightControlSystem_B.TAB)) {
    flightControlSystem_B.TAB = 0.0;
  }

  flightControlSystem_B.TBC = acos((flightControlSystem_B.B_c[0] *
    flightControlSystem_B.C[0] + flightControlSystem_B.B_c[1] *
    flightControlSystem_B.C[1]) / (flightControlSystem_norm_cPn2UTGY
    (flightControlSystem_B.B_c) * flightControlSystem_norm_cPn2UTGY
    (flightControlSystem_B.C)));
  if (rtIsNaN(flightControlSystem_B.TBC)) {
    flightControlSystem_B.TBC = 0.0;
  }

  flightControlSystem_B.TCD = acos((flightControlSystem_B.C[0] *
    flightControlSystem_B.D[0] + flightControlSystem_B.C[1] *
    flightControlSystem_B.D[1]) / (flightControlSystem_norm_cPn2UTGY
    (flightControlSystem_B.C) * flightControlSystem_norm_cPn2UTGY
    (flightControlSystem_B.D)));
  if (rtIsNaN(flightControlSystem_B.TCD)) {
    flightControlSystem_B.TCD = 0.0;
  }

  flightControlSystem_B.TDA = acos((flightControlSystem_B.D[0] *
    flightControlSystem_B.A_k[0] + flightControlSystem_B.D[1] *
    flightControlSystem_B.A_k[1]) / (flightControlSystem_norm_cPn2UTGY
    (flightControlSystem_B.D) * flightControlSystem_norm_cPn2UTGY
    (flightControlSystem_B.A_k)));
  if (rtIsNaN(flightControlSystem_B.TDA)) {
    flightControlSystem_B.TDA = 0.0;
  }

  flightControlSystem_B.A[0] = flightControlSystem_B.A_k[0];
  flightControlSystem_B.A[2] = flightControlSystem_B.B_c[0];
  flightControlSystem_B.A[4] = flightControlSystem_B.C[0];
  flightControlSystem_B.A[6] = flightControlSystem_B.D[0];
  flightControlSystem_B.A[1] = flightControlSystem_B.A_k[1];
  flightControlSystem_B.A[3] = flightControlSystem_B.B_c[1];
  flightControlSystem_B.A[5] = flightControlSystem_B.C[1];
  flightControlSystem_B.A[7] = flightControlSystem_B.D[1];
  flightControlSystem_B.A[8] = flightControlSystem_norm_cPn2UTGY
    (flightControlSystem_B.A_k);
  flightControlSystem_B.A[9] = flightControlSystem_norm_cPn2UTGY
    (flightControlSystem_B.B_c);
  flightControlSystem_B.A[10] = flightControlSystem_norm_cPn2UTGY
    (flightControlSystem_B.C);
  flightControlSystem_B.A[11] = flightControlSystem_norm_cPn2UTGY
    (flightControlSystem_B.D);
  flightControlSystem_B.A[12] = flightControlSystem_B.TAB;
  flightControlSystem_B.A[13] = flightControlSystem_B.TBC;
  flightControlSystem_B.A[14] = flightControlSystem_B.TCD;
  flightControlSystem_B.A[15] = flightControlSystem_B.TDA;

  /* End of MATLAB Function: '<S2>/MATLAB Function2' */

  /* DotProduct: '<S30>/Dot Product' */
  flightControlSystem_B.TAB = 0.0;

  /* DotProduct: '<S32>/Dot Product' */
  flightControlSystem_B.TBC = 0.0;

  /* DotProduct: '<S33>/Dot Product' */
  flightControlSystem_B.TCD = 0.0;

  /* DotProduct: '<S34>/Dot Product' */
  flightControlSystem_B.TDA = 0.0;

  /* DotProduct: '<S35>/Dot Product' */
  flightControlSystem_B.d0 = 0.0;

  /* DotProduct: '<S36>/Dot Product' */
  flightControlSystem_B.d1 = 0.0;

  /* DotProduct: '<S37>/Dot Product' */
  flightControlSystem_B.d2 = 0.0;

  /* DotProduct: '<S38>/Dot Product' */
  flightControlSystem_B.d3 = 0.0;

  /* DotProduct: '<S39>/Dot Product' */
  flightControlSystem_B.d4 = 0.0;

  /* DotProduct: '<S31>/Dot Product' */
  flightControlSystem_B.d5 = 0.0;
  for (k = 0; k < 16; k++) {
    /* Bias: '<S44>/Add min y' incorporates:
     *  Bias: '<S44>/Subtract min x'
     *  Gain: '<S44>/range y // range x'
     */
    flightControlSystem_B.rtb_Sum1_b = (flightControlSystem_B.A[k] +
      rtCP_Subtractminx_Bias[k]) * rtCP_rangeyrangex_Gain[k] + (-1.0);

    /* DotProduct: '<S30>/Dot Product' incorporates:
     *  Constant: '<S28>/IW{1,1}(1,:)''
     */
    flightControlSystem_B.TAB += rtCP_IW111_Value[k] *
      flightControlSystem_B.rtb_Sum1_b;

    /* DotProduct: '<S32>/Dot Product' incorporates:
     *  Constant: '<S28>/IW{1,1}(2,:)''
     */
    flightControlSystem_B.TBC += rtCP_IW112_Value[k] *
      flightControlSystem_B.rtb_Sum1_b;

    /* DotProduct: '<S33>/Dot Product' incorporates:
     *  Constant: '<S28>/IW{1,1}(3,:)''
     */
    flightControlSystem_B.TCD += rtCP_IW113_Value[k] *
      flightControlSystem_B.rtb_Sum1_b;

    /* DotProduct: '<S34>/Dot Product' incorporates:
     *  Constant: '<S28>/IW{1,1}(4,:)''
     */
    flightControlSystem_B.TDA += rtCP_IW114_Value[k] *
      flightControlSystem_B.rtb_Sum1_b;

    /* DotProduct: '<S35>/Dot Product' incorporates:
     *  Constant: '<S28>/IW{1,1}(5,:)''
     */
    flightControlSystem_B.d0 += rtCP_IW115_Value[k] *
      flightControlSystem_B.rtb_Sum1_b;

    /* DotProduct: '<S36>/Dot Product' incorporates:
     *  Constant: '<S28>/IW{1,1}(6,:)''
     */
    flightControlSystem_B.d1 += rtCP_IW116_Value[k] *
      flightControlSystem_B.rtb_Sum1_b;

    /* DotProduct: '<S37>/Dot Product' incorporates:
     *  Constant: '<S28>/IW{1,1}(7,:)''
     */
    flightControlSystem_B.d2 += rtCP_IW117_Value[k] *
      flightControlSystem_B.rtb_Sum1_b;

    /* DotProduct: '<S38>/Dot Product' incorporates:
     *  Constant: '<S28>/IW{1,1}(8,:)''
     */
    flightControlSystem_B.d3 += rtCP_IW118_Value[k] *
      flightControlSystem_B.rtb_Sum1_b;

    /* DotProduct: '<S39>/Dot Product' incorporates:
     *  Constant: '<S28>/IW{1,1}(9,:)''
     */
    flightControlSystem_B.d4 += rtCP_IW119_Value[k] *
      flightControlSystem_B.rtb_Sum1_b;

    /* DotProduct: '<S31>/Dot Product' incorporates:
     *  Constant: '<S28>/IW{1,1}(10,:)''
     */
    flightControlSystem_B.d5 += rtCP_IW1110_Value[k] *
      flightControlSystem_B.rtb_Sum1_b;
  }

  /* Sum: '<S23>/netsum' incorporates:
   *  DotProduct: '<S30>/Dot Product'
   *  DotProduct: '<S31>/Dot Product'
   *  DotProduct: '<S32>/Dot Product'
   *  DotProduct: '<S33>/Dot Product'
   *  DotProduct: '<S34>/Dot Product'
   *  DotProduct: '<S35>/Dot Product'
   *  DotProduct: '<S36>/Dot Product'
   *  DotProduct: '<S37>/Dot Product'
   *  DotProduct: '<S38>/Dot Product'
   *  DotProduct: '<S39>/Dot Product'
   */
  flightControlSystem_B.dv1[0] = flightControlSystem_B.TAB;
  flightControlSystem_B.dv1[1] = flightControlSystem_B.TBC;
  flightControlSystem_B.dv1[2] = flightControlSystem_B.TCD;
  flightControlSystem_B.dv1[3] = flightControlSystem_B.TDA;
  flightControlSystem_B.dv1[4] = flightControlSystem_B.d0;
  flightControlSystem_B.dv1[5] = flightControlSystem_B.d1;
  flightControlSystem_B.dv1[6] = flightControlSystem_B.d2;
  flightControlSystem_B.dv1[7] = flightControlSystem_B.d3;
  flightControlSystem_B.dv1[8] = flightControlSystem_B.d4;
  flightControlSystem_B.dv1[9] = flightControlSystem_B.d5;

  /* DotProduct: '<S43>/Dot Product' */
  flightControlSystem_B.TAB = 0.0;
  for (k = 0; k < 10; k++) {
    /* DotProduct: '<S43>/Dot Product' incorporates:
     *  Constant: '<S23>/b{1}'
     *  Constant: '<S29>/one'
     *  Constant: '<S29>/one1'
     *  Constant: '<S41>/IW{2,1}(1,:)''
     *  Gain: '<S29>/Gain'
     *  Gain: '<S29>/Gain1'
     *  Math: '<S29>/Exp'
     *  Math: '<S29>/Reciprocal'
     *  Sum: '<S23>/netsum'
     *  Sum: '<S29>/Sum'
     *  Sum: '<S29>/Sum1'
     *
     * About '<S29>/Exp':
     *  Operator: exp
     *
     * About '<S29>/Reciprocal':
     *  Operator: reciprocal
     */
    flightControlSystem_B.TAB += (1.0 / (exp((flightControlSystem_B.dv1[k] +
      rtCP_b1_Value[k]) * (-2.0)) + 1.0) * 2.0 - 1.0) * rtCP_IW211_Value[k];
  }

  /* Rounding: '<S2>/Round' incorporates:
   *  Bias: '<S45>/Add min x'
   *  Bias: '<S45>/Subtract min y'
   *  Constant: '<S24>/b{2}'
   *  DotProduct: '<S43>/Dot Product'
   *  Gain: '<S45>/Divide by range y'
   *  Sum: '<S24>/netsum'
   */
  flightControlSystem_B.TAB = rt_roundd_snf(((flightControlSystem_B.TAB +
    (-0.31710610588694338)) + 1.0) * 1.0 + 1.0);

  /* End of Outputs for SubSystem: '<Root>/Image Processing System' */

  /* Update for RateTransition: '<Root>/Rate Transition' */
  flightControlSystem_DW.RateTransition_Buffer[flightControlSystem_DW.RateTransition_ActiveBufIdx
    == 0] = flightControlSystem_B.TAB;
  flightControlSystem_DW.RateTransition_ActiveBufIdx = (int8_T)
    (flightControlSystem_DW.RateTransition_ActiveBufIdx == 0);
}

/* Model step wrapper function for compatibility with a static main program */
void flightControlSystem_step(int_T tid)
{
  switch (tid) {
   case 0 :
    flightControlSystem_step0();
    break;

   case 1 :
    flightControlSystem_step1();
    break;

   default :
    break;
  }
}

/* Model initialize function */
void flightControlSystem_initialize(void)
{
  /* Registration code */

  /* initialize non-finites */
  rt_InitInfAndNaN(sizeof(real_T));

  /* initialize real-time model */
  (void) memset((void *)flightControlSystem_M, 0,
                sizeof(RT_MODEL_flightControlSystem_T));
  (flightControlSystem_M)->Timing.TaskCounters.cLimit[0] = 1;
  (flightControlSystem_M)->Timing.TaskCounters.cLimit[1] = 40;
  rtmSetTFinal(flightControlSystem_M, 100.0);
  flightControlSystem_M->Timing.stepSize0 = 0.005;

  /* Setup for data logging */
  {
    static RTWLogInfo rt_DataLoggingInfo;
    rt_DataLoggingInfo.loggingInterval = NULL;
    flightControlSystem_M->rtwLogInfo = &rt_DataLoggingInfo;
  }

  /* Setup for data logging */
  {
    rtliSetLogXSignalInfo(flightControlSystem_M->rtwLogInfo, (NULL));
    rtliSetLogXSignalPtrs(flightControlSystem_M->rtwLogInfo, (NULL));
    rtliSetLogT(flightControlSystem_M->rtwLogInfo, "tout");
    rtliSetLogX(flightControlSystem_M->rtwLogInfo, "");
    rtliSetLogXFinal(flightControlSystem_M->rtwLogInfo, "");
    rtliSetLogVarNameModifier(flightControlSystem_M->rtwLogInfo, "rt_");
    rtliSetLogFormat(flightControlSystem_M->rtwLogInfo, 2);
    rtliSetLogMaxRows(flightControlSystem_M->rtwLogInfo, 1000);
    rtliSetLogDecimation(flightControlSystem_M->rtwLogInfo, 1);

    /*
     * Set pointers to the data and signal info for each output
     */
    {
      static void * rt_LoggedOutputSignalPtrs[] = {
        &flightControlSystem_Y.Actuators[0],
        &flightControlSystem_Y.Flag
      };

      rtliSetLogYSignalPtrs(flightControlSystem_M->rtwLogInfo,
                            ((LogSignalPtrsType)rt_LoggedOutputSignalPtrs));
    }

    {
      static int_T rt_LoggedOutputWidths[] = {
        4,
        1
      };

      static int_T rt_LoggedOutputNumDimensions[] = {
        1,
        1
      };

      static int_T rt_LoggedOutputDimensions[] = {
        4,
        1
      };

      static boolean_T rt_LoggedOutputIsVarDims[] = {
        0,
        0
      };

      static void* rt_LoggedCurrentSignalDimensions[] = {
        (NULL),
        (NULL)
      };

      static int_T rt_LoggedCurrentSignalDimensionsSize[] = {
        4,
        4
      };

      static BuiltInDTypeId rt_LoggedOutputDataTypeIds[] = {
        SS_SINGLE,
        SS_UINT8
      };

      static int_T rt_LoggedOutputComplexSignals[] = {
        0,
        0
      };

      static RTWPreprocessingFcnPtr rt_LoggingPreprocessingFcnPtrs[] = {
        (NULL),
        (NULL)
      };

      static const char_T *rt_LoggedOutputLabels[] = {
        "motors",
        "flag" };

      static const char_T *rt_LoggedOutputBlockNames[] = {
        "flightControlSystem/Actuators",
        "flightControlSystem/Flag" };

      static RTWLogDataTypeConvert rt_RTWLogDataTypeConvert[] = {
        { 0, SS_SINGLE, SS_SINGLE, 0, 0, 0, 1.0, 0, 0.0 },

        { 0, SS_UINT8, SS_UINT8, 0, 0, 0, 1.0, 0, 0.0 }
      };

      static RTWLogSignalInfo rt_LoggedOutputSignalInfo[] = {
        {
          2,
          rt_LoggedOutputWidths,
          rt_LoggedOutputNumDimensions,
          rt_LoggedOutputDimensions,
          rt_LoggedOutputIsVarDims,
          rt_LoggedCurrentSignalDimensions,
          rt_LoggedCurrentSignalDimensionsSize,
          rt_LoggedOutputDataTypeIds,
          rt_LoggedOutputComplexSignals,
          (NULL),
          rt_LoggingPreprocessingFcnPtrs,

          { rt_LoggedOutputLabels },
          (NULL),
          (NULL),
          (NULL),

          { rt_LoggedOutputBlockNames },

          { (NULL) },
          (NULL),
          rt_RTWLogDataTypeConvert
        }
      };

      rtliSetLogYSignalInfo(flightControlSystem_M->rtwLogInfo,
                            rt_LoggedOutputSignalInfo);

      /* set currSigDims field */
      rt_LoggedCurrentSignalDimensions[0] = &rt_LoggedOutputWidths[0];
      rt_LoggedCurrentSignalDimensions[1] = &rt_LoggedOutputWidths[1];
    }

    rtliSetLogY(flightControlSystem_M->rtwLogInfo, "yout");
  }

  /* block I/O */
  (void) memset(((void *) &flightControlSystem_B), 0,
                sizeof(B_flightControlSystem_T));

  /* exported global signals */
  motors_outport[0] = 0.0F;
  motors_outport[1] = 0.0F;
  motors_outport[2] = 0.0F;
  motors_outport[3] = 0.0F;
  flag_outport = 0U;

  /* states (dwork) */
  (void) memset((void *)&flightControlSystem_DW, 0,
                sizeof(DW_flightControlSystem_T));

  /* external inputs */
  (void)memset((void *)&flightControlSystem_U, 0, sizeof
               (ExtU_flightControlSystem_T));
  (void)memset(&sensor_inport, 0, sizeof(SensorsBus));
  (void)memset(&cmd_inport, 0, sizeof(CommandBus));

  /* external outputs */
  (void) memset((void *)&flightControlSystem_Y, 0,
                sizeof(ExtY_flightControlSystem_T));

  /* Model Initialize fcn for ModelReference Block: '<S1>/controller' */
  flightController_initialize(rtmGetErrorStatusPointer(flightControlSystem_M));

  /* Model Initialize fcn for ModelReference Block: '<S1>/estimator' */
  stateEstimator_initialize(rtmGetErrorStatusPointer(flightControlSystem_M));

  /* Model Initialize fcn for ModelReference Block: '<S2>/Image Conversion from Y1UY2V to YUV' */
  imageConversion_initialize(rtmGetErrorStatusPointer(flightControlSystem_M),
    &(flightControlSystem_DW.ImageConversionfromY1UY2VtoYUV_InstanceData.rtm));

  /* Matfile logging */
  rt_StartDataLoggingWithStartTime(flightControlSystem_M->rtwLogInfo, 0.0,
    rtmGetTFinal(flightControlSystem_M), flightControlSystem_M->Timing.stepSize0,
    (&rtmGetErrorStatus(flightControlSystem_M)));

  /* Start for Atomic SubSystem: '<Root>/Flight Control System' */
  flightControlSystem_FlightControlSystem_Start(flightControlSystem_M,
    &flightControlSystem_DW.FlightControlSystem);

  /* End of Start for SubSystem: '<Root>/Flight Control System' */
  {
    int32_T i;

    /* InitializeConditions for RateTransition: '<Root>/Rate Transition' */
    flightControlSystem_DW.RateTransition_Buffer[0] = 0.0;

    /* SystemInitialize for Atomic SubSystem: '<Root>/Flight Control System' */

    /* SystemInitialize for Inport: '<Root>/Sensors' */
    flightControlSystem_FlightControlSystem_Init
      (&flightControlSystem_DW.FlightControlSystem);

    /* End of SystemInitialize for SubSystem: '<Root>/Flight Control System' */

    /* SystemInitialize for Atomic SubSystem: '<Root>/Image Processing System' */
    /* SystemInitialize for MATLAB Function: '<S2>/MATLAB Function1' */
    flightControlSystem_DW.x_not_empty = false;
    for (i = 0; i < 256; i++) {
      flightControlSystem_DW.x[i] = i;
    }

    /* End of SystemInitialize for MATLAB Function: '<S2>/MATLAB Function1' */
    /* End of SystemInitialize for SubSystem: '<Root>/Image Processing System' */
  }
}

/* Model terminate function */
void flightControlSystem_terminate(void)
{
  /* (no terminate code required) */
}
