function Datos  = algoritmoJDM_1(L)

DIM = DimImagen();
Pr = int16(DIM./2);

if(~L(Pr(1),Pr(2)))
    Pr = calcularPrAfuera_1(L,Pr,DIM);
end

[A,B,C,D] = calcularVectores_1(L,Pr,DIM);
TAB = angulo(A,B);
TBC = angulo(B,C);
TCD = angulo(C,D);
TDA = angulo(D,A);
nA = norm(A);
nB = norm(B);
nC = norm(C);
nD = norm(D);
Datos = [A B C D nA nB nC nD TAB TBC TCD TDA];

end

function T = angulo(u,v)
nu = norm(u);
nv = norm(v);

if(nu ~= 0 && nv ~= 0)
T = real(acos(dot(u,v)/(nu*nv)));
else
    T = 0;
end

if(isnan(T))
    T = 0;
end
end
